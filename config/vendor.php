<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Default Directories
    |--------------------------------------------------------------------------
    |
    | The default directory of the images
    |
 */

    'catelogues' => [
        'directory' => 'catelogues',
        'thumbnail' => ['width' => 200, 'height' => 200],
    ],
    'brand' => [
        'directory' => 'brand',
        'thumbnail' => ['width' => 200, 'height' => 200],
    ],
    'vender' => [
        'directory' => 'vender',
        'thumbnail' => ['width' => 423, 'height' => 317],
    ],
    'product' => [
        'directory' => 'product',
        'thumbnail' => ['width' => 423, 'height' => 317],
    ],
    'exam' => [
        'directory' => 'exam',
        'thumbnail' => ['width' => 423, 'height' => 317],
    ],
    'event' => [
        'directory' => 'event',
        'thumbnail' => ['width' => 423, 'height' => 317],
    ],
    'industryConnect' => [
        'directory' => 'industry-connect',
        'thumbnail' => ['width' => 423, 'height' => 317],
    ],
    'pagination' => [
        'limit' => 10,
    ],
];

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePricingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pricing', function (Blueprint $table) {
                    $table->increments('id');
                    $table->integer('creator_id');
                    $table->integer('updater_id');
                    $table->uuid('uuid')->unique();
                    $table->float('price_tax_inc');
                    $table->float('price_tax_excl');                    
                    $table->float('price_per_unit');
                    $table->longText('tax_rule');
                    $table->timestamp('created_at');
                    $table->timestamp('updated_at')->nullable();
                    $table->timestamp('deleted_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pricing');
    }
}

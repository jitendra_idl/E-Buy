<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('creator_id');
            $table->integer('updater_id');
            $table->uuid('uuid')->unique();
            $table->integer('catalogue_id');
            $table->integer('brand_id'); 
            $table->integer('order_id'); 
            $table->integer('pricing_id');            
            $table->integer('media_id');            
            $table->string('name');
            $table->longtext('description');            
            $table->integer('disscount');
            $table->integer('quantities');
            $table->integer('minimum_quantities');
            $table->longtext('product_code'); 
            $table->integer('product_weight');   
            $table->integer('unit');                                                                               
            $table->string('status');
            $table->timestamp('created_at');
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_details');
    }
}

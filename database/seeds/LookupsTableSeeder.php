<?php

use Illuminate\Database\Seeder;

class LookupsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        DB::table('lookups')->truncate();
        DB::table('lookups')->insert([
            [
                'creator_id' => 1,
                'updater_id' => 1,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
                'type_id'    => 1,
                'name'       => 'Order Placed',
            ],
            [
                'creator_id' => 1,
                'updater_id' => 1,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
                'type_id'    => 1,
                'name'       => 'Confirmed',
            ],
            [
                'creator_id' => 1,
                'updater_id' => 1,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
                'type_id'    => 1,
                'name'       => 'Delivery boy assigned',
            ],
            [
                'creator_id' => 1,
                'updater_id' => 1,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
                'type_id'    => 1,
                'name'       => 'Despatched',
            ],
            [
                'creator_id' => 1,
                'updater_id' => 1,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
                'type_id'    => 1,
                'name'       => 'Delivered',
            ],
            [
                'creator_id' => 1,
                'updater_id' => 1,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
                'type_id'    => 1,
                'name'       => 'Closed',
            ],
            [
                'creator_id' => 1,
                'updater_id' => 1,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
                'type_id'    => 1,
                'name'       => 'Canceled',
            ],
            [
                'creator_id' => 1,
                'updater_id' => 1,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
                'type_id'    => 1,
                'name'       => 'Pending',
            ],
        ]);
    }
}

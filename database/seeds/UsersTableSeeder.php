<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'username' => 'customer',
            'password' => bcrypt('customer'),
            'name'     => 'customer',
            'email'    => 'customer@gmail.com',
            'mobile'   => '12312312331',
            'role_id'  => 2,
        ]);
    }
}

@extends('layouts.app')

@section('content')
            <div class="main-order-section">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="main-msg-box">
                                <div class="container-fluid">
                                    <div class="row">
                                         <div class="bread-crum">
                                            <nav class="breadcrumb">
                                              <a class="breadcrumb-item" href="index.html">Home</a>
                                              <span class="breadcrumb-item active">Message</span>
                                            </nav>
                                        </div>
                                        <div class="main-masseges-section">    
                                            <div class="col-lg-3 col-sm-3 col-md-4 col-xs-12 float-left">
                                                <div class="row">
                                                    <div class="left-side-msg-profile">
                                                        <div class="msg-profile-hed">
                                                            <h5>Messaging</h5>
                                                        </div>
                                                        <div class="main-pro-content">
                                                            <div class="search-bar-pro">
                                                                <div class="search-bar">
                                                                    <input type="text" name="" placeholder="Search Message">
                                                                </div>
                                                            </div>
                                                            <div class="main-pro-section">
                                                                <div class="profile-row">
                                                                    <div class="pro-name-img">
                                                                        <div class="profile-pic status">
                                                                            <img src="http://www.attractivepartners.co.uk/wp-content/uploads/2017/06/profile.jpg">
                                                                        </div>
                                                                        <div class="persone-name">
                                                                            <h5>Name <span>2</span></h5>
                                                                            <p>Lorem Ipsum is simply dummy text of the printing </p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="right-time">
                                                                        <p>2 min</p>
                                                                    </div>
                                                                </div>
                                                                <div class="profile-row">
                                                                    <div class="pro-name-img">
                                                                        <div class="profile-pic status away">
                                                                            <img src="http://www.attractivepartners.co.uk/wp-content/uploads/2017/06/profile.jpg">
                                                                        </div>
                                                                        <div class="persone-name">
                                                                            <h5>Name</h5>
                                                                            <p>Lorem Ipsum is simply dummy text of the printing </p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="right-time">
                                                                        <p>2 min</p>
                                                                    </div>
                                                                </div>
                                                                <div class="profile-row">
                                                                    <div class="pro-name-img">
                                                                        <div class="profile-pic">
                                                                            <img src="http://www.attractivepartners.co.uk/wp-content/uploads/2017/06/profile.jpg">
                                                                        </div>
                                                                        <div class="persone-name">
                                                                            <h5>Name</h5>
                                                                            <p>Lorem Ipsum is simply dummy text of the printing </p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="right-time">
                                                                        <p>2 min</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-9 col-sm-9 col-md-8 col-xs-12 float-left">
                                                <div class="row">
                                                    <div class="chat-section">
                                                        <div class="chat-persone-name">
                                                            <div class="name-of-chat">
                                                                <h5>Harshit Rana</h5>
                                                            </div>
                                                        </div>
                                                        <div class="main-chat-blockes">
                                                            <div class="main-chat-room">
                                                                <div class="chat-all-block">
                                                                    <div class="chat-meassge-row chat-room-row-left">
                                                                        <div class="chat-block">
                                                                            <div class="chat-user-pic"> 
                                                                                <img src="http://www.attractivepartners.co.uk/wp-content/uploads/2017/06/profile.jpg">
                                                                            </div>
                                                                            <div class="chat-user-info">
                                                                                <div class="chat-msg-content">
                                                                                    <div class="chat-msg chat-msd-for-client">
                                                                                        <h5>Steven Finn</h5>
                                                                                        <p>How are you ?</p>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="chat-meassge-row">
                                                                        <div class="chat-block float-right">
                                                                            <div class="chat-user-pic"> 
                                                                                <img src="http://www.attractivepartners.co.uk/wp-content/uploads/2017/06/profile.jpg">
                                                                            </div>
                                                                            <div class="chat-user-info">
                                                                                <div class="chat-msg-content">
                                                                                    <div class="chat-msg chat-msd-for-client">
                                                                                        <h5>Harshit Rana</h5>
                                                                                        <p>How are you ?</p>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="chat-meassge-row chat-room-row-left">
                                                                        <div class="chat-block">
                                                                            <div class="chat-user-pic"> 
                                                                                <img src="http://www.attractivepartners.co.uk/wp-content/uploads/2017/06/profile.jpg">
                                                                            </div>
                                                                            <div class="chat-user-info">
                                                                                <div class="chat-msg-content">
                                                                                    <div class="chat-msg chat-msd-for-client">
                                                                                        <h5>Steven Finn</h5>
                                                                                        <img src="http://www.attractivepartners.co.uk/wp-content/uploads/2017/06/profile.jpg">
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="chat-text-area">
                                                                <textarea placeholder="Write a Message or attach a file"></textarea>
                                                                <div class="upload-msg-img">
                                                                    <div class="pull-left upload-control-file">
                                                                        <a href="#"><i class="fas fa-paperclip"></i>
                                                                            <input type="file">
                                                                        </a>
                                                                        
                                                                    </div>
                                                                    <button><i class="fas fa-angle-right"></i></button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
@endsection

      
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="js/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <!-- <script src="js/popper.js"></script> -->
    <script src="js/bootstrap.min.js"></script>
    <script src="js/material.min.js"></script>
    <script src="js/getmdl-select.js"></script>
    <script defer src="js/solid.js"></script>
    <script defer src="js/fontawesome.js"></script>
    <script src="js/canvasjs.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#sidebarCollapse').on('click', function () {
                $('#sidebar').toggleClass('active');
                $('.main-container').toggleClass('open-menu');
            });
        });
    </script>
    <script>
        $(document).ready(function() {
            $('#example').DataTable();
        } );
    </script>
  </body>
</html>
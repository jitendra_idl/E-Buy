
@extends('layouts.app')

@section('content')   

<div class="main-order-section">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="bread-crum">
                                <nav class="breadcrumb">
                                  <a class="breadcrumb-item" href="index.html">Home</a>
                                  <span class="breadcrumb-item active">Settings</span>
                                </nav>
                            </div>
                            <div class="catalouge-list">    
                                <div class="page-headeing">
                                    <h5>Settings</h5>
                                </div>
                                <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                                  <li class="nav-item">
                                    <a class="nav-link " id="pills-home-tab" data-toggle="pill" href="#w-app" role="tab" aria-controls="pills-home" aria-selected="true">Category</a>
                                  </li>
                                  <li class="nav-item">
                                    <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#out-dill" role="tab" aria-controls="pills-profile" aria-selected="false">Brand</a>
                                  </li>
                                  <li class="nav-item">
                                    <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#order-dille" role="tab" aria-controls="pills-profile" aria-selected="false">Tax</a>
                                  </li>
                                  <li class="nav-item">
                                    <a class="nav-link active" id="pills-my-team" data-toggle="pill" href="#my-team" role="tab" aria-controls="pills-profile" aria-selected="false">My Team</a>
                                  </li>
                                </ul>
                                <div class="placeorder-btn">
                                    <div class="order-btn">
                                        <a href="#exampleModalCenter" data-toggle="modal">Add Category</a>
                                    </div>
                                </div>
                                <div class="tab-content" id="pills-tabContent">
                                    <div class="tab-pane fade" id="w-app" role="tabpanel" aria-labelledby="pills-home-tab">
                                       <div class="main-category-list bg-white com-box-shodow rounded float-left">
                                           
                                           
                                            @foreach($catalogues as $catalogue)
                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 float-left">
                                                <div class="order-category-lilst-box">
                                                    <div class="order-detail com-box-shodow bg-white">
                                                        <div class="order-pro-img">
                                                            <img src="{{ asset('storage/'.$catalogue->media->path) }}">
                                                        </div>
                                                        <div class="order-pro-info">
                                                            <h5>{{ $catalogue->name }}</h5>
                                                            <p>{{ $catalogue->description }}</p>
                                                            <div class="add-edit-btn">
                                                            	<a href="#"><i class="fas fa-trash-alt"></i></a>
                                                            	<a href="#"><i class="fas fa-edit"></i></a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                        
                                       </div>
                                    </div>
                                    <div class="tab-pane fade" id="out-dill" role="tabpanel" aria-labelledby="pills-profile-tab">
                                    <div class="main-category-list bg-white com-box-shodow rounded float-left">
                                           
                                    @foreach($brands as $brand)                                       
                                            <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6 float-left m-b-30">
                                                <div class="main-vendor-box bg-white com-box-shodow rounded float-left">
                                                    <div class="vendor-img">
                                                        <div class="vendor-image">
                                                        <img src="{{ asset('storage/'.$brand->media->path) }}">
                                                           
                                                        </div>
                                                    </div>
                                                    <div class="vender-detail brands">
                                                        <a href="#" class="vender-name">{{ $brand->name }}</a>
                                                        <br/>
                                                        <div class="location-name settings-brnds">
                                                            <a href="#" class="edt-del"><i class="fas fa-trash-alt"></i></a>
                                                            <a href="#" class="edt-del"><i class="fas fa-edit"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                    @endforeach
                                            
                                         
                                          
                                            
                                       
                                          
                                           
                                       

                                       </div>
                                    </div>
                                    <div class="tab-pane fade" id="order-dille" role="tabpanel" aria-labelledby="pills-profile-tab">
                                       <div class="main-category-list bg-white com-box-shodow rounded float-left">
                                            <div class="income-tax-detail">
                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 float-left">
                                                    <div class="frm-group">
                                                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                                           <input class="mdl-textfield__input" type="text" id="sample3">
                                                           <label class="mdl-textfield__label" for="sample3">GST</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 float-left">
                                                    <div class="frm-group">
                                                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                                           <input class="mdl-textfield__input" type="text" id="sample3">
                                                           <label class="mdl-textfield__label" for="sample3">IST</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 float-left">
                                                    <div class="frm-group">
                                                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                                           <input class="mdl-textfield__input" type="text" id="sample3">
                                                           <label class="mdl-textfield__label" for="sample3">Normal Tax</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="invoice-btn tax-submit-btn">
	                                            <div class="btn-group float-right">
	                                                <a href="index.html">Submit</a>
	                                            </div>
	                                        </div>
                                       </div>
                                    </div>
                                     <div class="tab-pane fade show active" id="my-team" role="tabpanel" aria-labelledby="pills-my-team">
                                       <div class="main-category-list bg-white com-box-shodow rounded float-left">
                                            <div class="location-state">
                                            	<div class="location-state-dropdown">
                                            		<div class="frm-group" id="weight-1">
                                                         <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select branch-select">
                                                            <input type="text" value="" class="mdl-textfield__input" id="sample7" readonly>
                                                            <input type="hidden" value="" name="sample7">
                                                            <i class="mdl-icon-toggle__label material-icons"></i>
                                                            <label for="sample7" class="mdl-textfield__label">Select state</label>
                                                            <ul for="sample7" class="mdl-menu mdl-menu--bottom-left mdl-js-menu">
                                                                <li class="mdl-menu__item select-state" data-val="100gm">Mumbai</li>
                                                                <li class="mdl-menu__item select-state" data-val="200gm">Bangalore</li>
                                                                <li class="mdl-menu__item select-state" data-val="300gm">Chennai</li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                            	</div>
                                            	<div id="stat-1" class="team-member-box ">
                                            		
                                            		<div class="team-member-row">
                                            			<div class="tem-member-box">
                                            				<a href="#">
	                                            				<div class="team-box-main com-box-shodow">
		                                            				<div class="team-member-img">
		                                            					<img src="images/profile-img.png">
		                                            					<!-- <div class="tem-poisiotn">
		                                            						<h5>CEO</h5>
		                                            					</div> -->
		                                            				</div>
		                                            			</div>
		                                            				<div class="team-info CEO-tab">
		                                        						<div class="team-content">
		                                        							<h5>-CEO</h5>
		                                        							<p>Amit Parmar</p>
																		</div>
		                                        					</div>
		                                        				</a>
		                                        				</div>
		                                        			
                                            			</div>
                                            		
                                            		<div class="team-member-row">
                                            			<div class="tem-member-box">
                                            				<a href="#">
	                                            				<div class="team-member-img">
	                                            					<img src="images/profile-img.png">
	                                            					
	                                            				</div>
	                                            				<div class="team-info">
	                                        						<div class="team-content -manager-">
	                                        							<h5>-Manager</h5>
	                                        							<p>Amit Parmar</p>
	                                        						</div>
	                                        					</div>
	                                        				</a>
                                            			</div>
                                            			<div class="tem-member-box">
                                            				<a href="#">
	                                            				<div class="team-member-img">
	                                            					<img src="images/profile-img.png">

	                                            				</div>
	                                            				<div class="team-info">
	                                        						<div class="team-content">
	                                        							<h5>-manager</h5>
	                                        							<p>Amit Parmar</p>
	                                        						</div>
                                        						</div>
                                        					</a>
                                            			</div>
                                            		</div>
                                            		<div class="team-member-row">
                                            			<div class="tem-member-box">
                                            				<a href="#">
	                                            				<div class="team-member-img">
	                                            					<img src="images/profile-img.png">

	                                            				</div>
	                                            				<div class="team-info">
	                                        						<div class="team-content">
	                                        							<h5>Deilivery Boy</h5>
	                                        							<p>Amit Parmar</p>
	                                        						</div>
	                                        					</div>
	                                        				</a>
                                            			</div>
                                            			<div class="tem-member-box">
                                            				<a href="#">
	                                            				<div class="team-member-img">
	                                            					<img src="images/profile-img.png">

	                                            				</div>
	                                            				<div class="team-info">
	                                        						<div class="team-content">
	                                        							<h5>Deilivery Boy</h5>
	                                        							<p>Amit Parmar</p>
	                                        						</div>
	                                        					</div>
	                                        				</a>
                                            			</div>
                                            			<div class="tem-member-box">
                                            				<a href="#">
	                                            				<div class="team-member-img">
	                                            					<img src="images/profile-img.png">

	                                            				</div>
	                                            				<div class="team-info">
	                                        						<div class="team-content">
	                                        							<h5>Deilivery Boy</h5>
	                                        							<p>Amit Parmar</p>
	                                        						</div>
	                                        					</div>
	                                        				</a>
                                            			</div>
                                            		</div>
                                            	</div>

                                            	<div id="stat-2" class="team-member-box dsi-none">
                                            		
                                            		<div class="team-member-row">
                                            			<div class="tem-member-box">
                                            				<a href="#">
	                                            				<div class="team-box-main com-box-shodow">
		                                            				<div class="team-member-img">
		                                            					<img src="images/profile-img.png">
		                                            					<!-- <div class="tem-poisiotn">
		                                            						<h5>CEO</h5>
		                                            					</div> -->
		                                            				</div>
		                                            			</div>
		                                            				<div class="team-info CEO-tab">
		                                        						<div class="team-content">
		                                        							<h5>-CEO</h5>
		                                        							<p>Amit Parmar</p>
																		</div>
		                                        					</div>
		                                        				</a>
		                                        				</div>
		                                        			
                                            			</div>
                                            		
                                            		<div class="team-member-row">
                                            			<div class="tem-member-box">
                                            				<a href="#">
	                                            				<div class="team-member-img">
	                                            					<img src="images/profile-img.png">

	                                            				</div>
	                                            				<div class="team-info">
	                                        						<div class="team-content -manager-">
	                                        							<h5>-Manager</h5>
	                                        							<p>Amit Parmar</p>
	                                        						</div>
	                                        					</div>
	                                        				</a>
                                            			</div>
                                            			<div class="tem-member-box">
                                            				<a href="#">
	                                            				<div class="team-member-img">
	                                            					<img src="images/profile-img.png">

	                                            				</div>
	                                            				<div class="team-info">
	                                        						<div class="team-content">
	                                        							<h5>-manager</h5>
	                                        							<p>Amit Parmar</p>
	                                        						</div>
                                        						</div>
                                        					</a>
                                            			</div>
                                            		</div>
                                            		<div class="team-member-row">
                                            			<div class="tem-member-box">
                                            				<a href="#">
	                                            				<div class="team-member-img">
	                                            					<img src="images/profile-img.png">

	                                            				</div>
	                                            				<div class="team-info">
	                                        						<div class="team-content">
	                                        							<h5>Deilivery Boy</h5>
	                                        							<p>Amit Parmar</p>
	                                        						</div>
	                                        					</div>
	                                        				</a>
                                            			</div>
                                            			<div class="tem-member-box">
                                            				<a href="#">
	                                            				<div class="team-member-img">
	                                            					<img src="images/profile-img.png">

	                                            				</div>
	                                            				<div class="team-info">
	                                        						<div class="team-content">
	                                        							<h5>Deilivery Boy</h5>
	                                        							<p>Amit Parmar</p>
	                                        						</div>
	                                        					</div>
	                                        				</a>
                                            			</div>
                                            			<div class="tem-member-box">
                                            				<a href="#">
	                                            				<div class="team-member-img">
	                                            					<img src="images/profile-img.png">

	                                            				</div>
	                                            				<div class="team-info">
	                                        						<div class="team-content">
	                                        							<h5>Deilivery Boy</h5>
	                                        							<p>Amit Parmar</p>
	                                        						</div>
	                                        					</div>
	                                        				</a>
                                            			</div>
                                            		</div>
                                            	</div>

                                            	<div id="stat-3" class="team-member-box dsi-none">
                                            		
                                            		<div class="team-member-row">
                                            			<div class="tem-member-box">
                                            				<a href="#">
	                                            				<div class="team-box-main com-box-shodow">
		                                            				<div class="team-member-img">
		                                            					<img src="images/profile-img.png">
		                                            					<!-- <div class="tem-poisiotn">
		                                            						<h5>CEO</h5>
		                                            					</div> -->
		                                            				</div>
		                                            			</div>
		                                            				<div class="team-info CEO-tab">
		                                        						<div class="team-content">
		                                        							<h5>-CEO</h5>
		                                        							<p>Amit Parmar</p>
																		</div>
		                                        					</div>
		                                        				</a>
		                                        				</div>
		                                        			
                                            			</div>
                                            		
                                            		<div class="team-member-row">
                                            			<div class="tem-member-box">
                                            				<a href="#">
	                                            				<div class="team-member-img">
	                                            					<img src="images/profile-img.png">

	                                            				</div>
	                                            				<div class="team-info">
	                                        						<div class="team-content -manager-">
	                                        							<h5>-Manager</h5>
	                                        							<p>Amit Parmar</p>
	                                        						</div>
	                                        					</div>
	                                        				</a>
                                            			</div>
                                            			<div class="tem-member-box">
                                            				<a href="#">
	                                            				<div class="team-member-img">
	                                            					<img src="images/profile-img.png">

	                                            				</div>
	                                            				<div class="team-info">
	                                        						<div class="team-content">
	                                        							<h5>-manager</h5>
	                                        							<p>Amit Parmar</p>
	                                        						</div>
                                        						</div>
                                        					</a>
                                            			</div>
                                            		</div>
                                            		<div class="team-member-row">
                                            			<div class="tem-member-box">
                                            				<a href="#">
	                                            				<div class="team-member-img">
	                                            					<img src="images/profile-img.png">

	                                            				</div>
	                                            				<div class="team-info">
	                                        						<div class="team-content">
	                                        							<h5>Deilivery Boy</h5>
	                                        							<p>Amit Parmar</p>
	                                        						</div>
	                                        					</div>
	                                        				</a>
                                            			</div>
                                            			<div class="tem-member-box">
                                            				<a href="#">
	                                            				<div class="team-member-img">
	                                            					<img src="images/profile-img.png">

	                                            				</div>
	                                            				<div class="team-info">
	                                        						<div class="team-content">
	                                        							<h5>Deilivery Boy</h5>
	                                        							<p>Amit Parmar</p>
	                                        						</div>
	                                        					</div>
	                                        				</a>
                                            			</div>
                                            			<div class="tem-member-box">
                                            				<a href="#">
	                                            				<div class="team-member-img">
	                                            					<img src="images/profile-img.png">

	                                            				</div>
	                                            				<div class="team-info">
	                                        						<div class="team-content">
	                                        							<h5>Deilivery Boy</h5>
	                                        							<p>Amit Parmar</p>
	                                        						</div>
	                                        					</div>
	                                        				</a>
                                            			</div>
                                            		</div>
                                            	</div>
                                            </div>
                                       </div>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

</div>
      <div class="modal fade bd-example-modal-lg" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLongTitle">Add Category</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="invoice-info add-cat">
                <div class="add-cat-img-main">
                    <div class="add-cat-img">
                        <input type="file" name="">
                        <i class="fas fa-plus-circle"></i>
                    </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 float-left p-0">
                    <div class="frm-group">
                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                            <input class="mdl-textfield__input" type="text" id="sample3">
                            <label class="mdl-textfield__label" for="sample3">Category Name</label>
                          </div>
                    </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 float-left p-0">
                    <div class="frm-group">
                          <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                            <input class="mdl-textfield__input" type="text" id="sample3">
                            <label class="mdl-textfield__label" for="sample3">Discription</label>
                          </div>
                    </div>
                </div>
            </div>
          </div>
          <div class="modal-footer">
            <a href="index.html">
                <button type="button" class="model-btn">Submit</button>
            </a>
          </div>
        </div>
      </div>
    </div>
    <div class="modal fade bd-example-modal-lg" id="Editcatogory" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLongTitle">Edit Category</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="invoice-info add-cat">
                <div class="edit-img">
                    <div class="edit-image">
                        <img src="images/pro.jpeg">
                        <div class="edit-icon">
                            <div class="emg-edit-sec">
                                <input type="file" name="">
                                <i class="fas fa-pencil-alt"></i>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 float-left p-0">
                    <div class="frm-group">
                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                            <input class="mdl-textfield__input" type="text" id="sample3" value="Namkeen">
                            <label class="mdl-textfield__label" for="sample3">Category Name</label>
                          </div>
                    </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 float-left p-0">
                    <div class="frm-group">
                          <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                            <input class="mdl-textfield__input" type="text" id="sample3" value="Lorem Ipsum is simply dummy text ">
                            <label class="mdl-textfield__label" for="sample3">Discription</label>
                          </div>
                    </div>
                </div>
            </div>
          </div>
          <div class="modal-footer">
            <a href="index.html">
                <button type="button" class="model-btn">Submit</button>
            </a>
          </div>
        </div>
      </div>
    </div>
    @endsection

      
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="js/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <!-- <script src="js/popper.js"></script> -->
    <script src="js/bootstrap.min.js"></script>
    <script src="js/material.min.js"></script>
    <script src="js/getmdl-select.js"></script>
    <script defer src="js/solid.js"></script>
    <script defer src="js/fontawesome.js"></script>
    <script src="js/canvasjs.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
    <script type="text/javascript">
    	$('.branch-select .mdl-menu__item').on('click',function(){
            $('.team-member-box ').css('display','none');
            if($(this).data("val") == "100gm"){
                $('#stat-1').css('display','block');
            }
            else if($(this).data("val") == "200gm"){
                $('#stat-2').css('display','block');
            }
            else if($(this).data("val") == "300gm"){
                $('#stat-3').css('display','block');
            }
        });
        $(document).ready(function () {
            $('#sidebarCollapse').on('click', function () {
                $('#sidebar').toggleClass('active');
                $('.main-container').toggleClass('open-menu');
            });
        });
    </script>
    <script>
        $(document).ready(function() {
            $('#example').DataTable();

        });

    </script>
  </body>
</html>

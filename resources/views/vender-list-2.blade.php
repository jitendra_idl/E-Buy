@extends('layouts.app')

@section('content') 
            <div class="main-order-section">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="bread-crum">
                                <nav class="breadcrumb">
                                  <a class="breadcrumb-item" href="index.html">Home</a>
                                  <span class="breadcrumb-item active">My Vendors</span>
                                </nav>
                            </div>
                            <div class="vender-list">                        
                                <div class="vender-list-top-add-btn">
                                    <h1>My Vendors</h1>
                                </div>
                                <div class="vendor-listing-main">
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 float-left m-b-10 p-l-t-7">
                                        <div class="main-vendor-box bg-white com-box-shodow rounded float-left">
                                            <div class="vendor-img">
                                                <div class="vendor-image">
                                                    <img src="images/logo-1.png">
                                                </div>
                                            </div>
                                            <div class="vender-detail">
                                                <a href="product-list-2.html" class="vender-name">Anand Multi Trade</a>
                                                <p>+91 8889596562</p>
                                                <a href="#" class="vender-email">Info@gmail.com</a>
                                                <p>India</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 float-left m-b-10 p-l-t-7">
                                        <div class="main-vendor-box bg-white com-box-shodow rounded float-left">
                                            <div class="vendor-img">
                                                <div class="vendor-image">
                                                    <img src="images/logo-6.png">
                                                </div>
                                            </div>
                                            <div class="vender-detail">
                                                <a href="product-list-2.html" class="vender-name">Anand Multi Trade</a>
                                                <p>+91 8889596562</p>
                                                <a href="#" class="vender-email">Info@gmail.com</a>
                                                <p>India</p>
                                                
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 float-left m-b-10 p-l-t-7">
                                        <div class="main-vendor-box bg-white com-box-shodow rounded float-left">
                                            <div class="vendor-img">
                                                <div class="vendor-image">
                                                    <img src="images/logo-3.png">
                                                </div>
                                                
                                            </div>
                                            <div class="vender-detail">
                                                <a href="product-list-2.html" class="vender-name">Anand Multi Trade</a>
                                                <p>+91 8889596562</p>
                                                <a href="#" class="vender-email">Info@gmail.com</a>
                                                <p>India</p>
                                                
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 float-left m-b-10 p-l-t-7">
                                        <div class="main-vendor-box bg-white com-box-shodow rounded float-left">
                                            <div class="vendor-img">
                                                <div class="vendor-image">
                                                    <img src="images/logo-4.png">
                                                </div>
                                                <!--  -->
                                            </div>
                                            <div class="vender-detail">
                                                <a href="product-list-2.html" class="vender-name">Anand Multi Trade</a>
                                                <p>+91 8889596562</p>
                                                <a href="#" class="vender-email">Info@gmail.com</a>
                                                <p>India</p>
                                                
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 float-left m-b-10 p-l-t-7">
                                        <div class="main-vendor-box bg-white com-box-shodow rounded float-left">
                                            <div class="vendor-img">
                                                <div class="vendor-image">
                                                    <img src="images/logo-5.png">
                                                </div>
                                                <!--  -->
                                            </div>
                                            <div class="vender-detail">
                                                <a href="product-list-2.html" class="vender-name">Anand Multi Trade</a>
                                                <p>+91 8889596562</p>
                                                <a href="#" class="vender-email">Info@gmail.com</a>
                                                <p>India</p>
                                                
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 float-left m-b-10 p-l-t-7">
                                        <div class="main-vendor-box bg-white com-box-shodow rounded float-left">
                                            <div class="vendor-img">
                                                <div class="vendor-image">
                                                    <img src="images/logo-6.png">
                                                </div>
                                                <!--  -->
                                            </div>
                                            <div class="vender-detail">
                                                <a href="product-list-2.html" class="vender-name">Anand Multi Trade</a>
                                                <p>+91 8889596562</p>
                                                <a href="#" class="vender-email">Info@gmail.com</a>
                                                <p>India</p>
                                                
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 float-left m-b-10 p-l-t-7">
                                        <div class="main-vendor-box bg-white com-box-shodow rounded float-left">
                                            <div class="vendor-img">
                                                <div class="vendor-image">
                                                    <img src="images/logo-5.png">
                                                </div>
                                                <!--  -->
                                            </div>
                                            <div class="vender-detail">
                                                <a href="product-list-2.html" class="vender-name">Anand Multi Trade</a>
                                                <p>+91 8889596562</p>
                                                <a href="#" class="vender-email">Info@gmail.com</a>
                                                <p>India</p>
                                                
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 float-left m-b-10 p-l-t-7">
                                        <div class="main-vendor-box bg-white com-box-shodow rounded float-left">
                                            <div class="vendor-img">
                                                <div class="vendor-image">
                                                    <img src="images/logo-2.png">
                                                </div>
                                                <!--  -->
                                            </div>
                                            <div class="vender-detail">
                                                <a href="product-list-2.html" class="vender-name">Anand Multi Trade</a>
                                                <p>+91 8889596562</p>
                                                <a href="#" class="vender-email">Info@gmail.com</a>
                                                <p>India</p>
                                                
                                            </div>
                                        </div>
                                    </div>


                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 float-left m-b-10 p-l-t-7">
                                        <div class="main-vendor-box bg-white com-box-shodow rounded float-left">
                                            <div class="vendor-img">
                                                <div class="vendor-image">
                                                    <img src="images/logo-1.png">
                                                </div>
                                                <!--  -->
                                            </div>
                                            <div class="vender-detail">
                                                <a href="product-list-2.html" class="vender-name">Anand Multi Trade</a>
                                                <p>+91 8889596562</p>
                                                <a href="#" class="vender-email">Info@gmail.com</a>
                                                <p>India</p>
                                                
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 float-left m-b-10 p-l-t-7">
                                        <div class="main-vendor-box bg-white com-box-shodow rounded float-left">
                                            <div class="vendor-img">
                                                <div class="vendor-image">
                                                    <img src="images/logo-6.png">
                                                </div>
                                                <!--  -->
                                            </div>
                                            <div class="vender-detail">
                                                <a href="product-list-2.html" class="vender-name">Anand Multi Trade</a>
                                                <p>+91 8889596562</p>
                                                <a href="#" class="vender-email">Info@gmail.com</a>
                                                <p>India</p>
                                                
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 float-left m-b-10 p-l-t-7">
                                        <div class="main-vendor-box bg-white com-box-shodow rounded float-left">
                                            <div class="vendor-img">
                                                <div class="vendor-image">
                                                    <img src="images/logo-3.png">
                                                </div>
                                                <!--  -->
                                            </div>
                                            <div class="vender-detail">
                                                <a href="product-list-2.html" class="vender-name">Anand Multi Trade</a>
                                                <p>+91 8889596562</p>
                                                <a href="#" class="vender-email">Info@gmail.com</a>
                                                <p>India</p>
                                                
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 float-left m-b-10 p-l-t-7">
                                        <div class="main-vendor-box bg-white com-box-shodow rounded float-left">
                                            <div class="vendor-img">
                                                <div class="vendor-image">
                                                    <img src="images/logo-4.png">
                                                </div>
                                                <!--  -->
                                            </div>
                                            <div class="vender-detail">
                                                <a href="product-list-2.html" class="vender-name">Anand Multi Trade</a>
                                                <p>+91 8889596562</p>
                                                <a href="#" class="vender-email">Info@gmail.com</a>
                                                <p>India</p>
                                                
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 float-left m-b-10 p-l-t-7">
                                        <div class="main-vendor-box bg-white com-box-shodow rounded float-left">
                                            <div class="vendor-img">
                                                <div class="vendor-image">
                                                    <img src="images/logo-5.png">
                                                </div>
                                                <!--  -->
                                            </div>
                                            <div class="vender-detail">
                                                <a href="product-list-2.html" class="vender-name">Anand Multi Trade</a>
                                                <p>+91 8889596562</p>
                                                <a href="#" class="vender-email">Info@gmail.com</a>
                                                <p>India</p>
                                                
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 float-left m-b-10 p-l-t-7">
                                        <div class="main-vendor-box bg-white com-box-shodow rounded float-left">
                                            <div class="vendor-img">
                                                <div class="vendor-image">
                                                    <img src="images/logo-6.png">
                                                </div>
                                                <!--  -->
                                            </div>
                                            <div class="vender-detail">
                                                <a href="product-list-2.html" class="vender-name">Anand Multi Trade</a>
                                                <p>+91 8889596562</p>
                                                <a href="#" class="vender-email">Info@gmail.com</a>
                                                <p>India</p>
                                                
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 float-left m-b-10 p-l-t-7">
                                        <div class="main-vendor-box bg-white com-box-shodow rounded float-left">
                                            <div class="vendor-img">
                                                <div class="vendor-image">
                                                    <img src="images/logo-5.png">
                                                </div>
                                                <!--  -->
                                            </div>
                                            <div class="vender-detail">
                                                <a href="product-list-2.html" class="vender-name">Anand Multi Trade</a>
                                                <p>+91 8889596562</p>
                                                <a href="#" class="vender-email">Info@gmail.com</a>
                                                <p>India</p>
                                                
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 float-left m-b-10 p-l-t-7">
                                        <div class="main-vendor-box bg-white com-box-shodow rounded float-left">
                                            <div class="vendor-img">
                                                <div class="vendor-image">
                                                    <img src="images/logo-2.png">
                                                </div>
                                                <!--  -->
                                            </div>
                                            <div class="vender-detail">
                                                <a href="product-list-2.html" class="vender-name">Anand Multi Trade</a>
                                                <p>+91 8889596562</p>
                                                <a href="#" class="vender-email">Info@gmail.com</a>
                                                <p>India</p>
                                                
                                            </div>
                                        </div>
                                    </div>


                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 float-left m-b-10 p-l-t-7">
                                        <div class="main-vendor-box bg-white com-box-shodow rounded float-left">
                                            <div class="vendor-img">
                                                <div class="vendor-image">
                                                    <img src="images/logo-1.png">
                                                </div>
                                                <!--  -->
                                            </div>
                                            <div class="vender-detail">
                                                <a href="product-list-2.html" class="vender-name">Anand Multi Trade</a>
                                                <p>+91 8889596562</p>
                                                <a href="#" class="vender-email">Info@gmail.com</a>
                                                <p>India</p>
                                                
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 float-left m-b-10 p-l-t-7">
                                        <div class="main-vendor-box bg-white com-box-shodow rounded float-left">
                                            <div class="vendor-img">
                                                <div class="vendor-image">
                                                    <img src="images/logo-6.png">
                                                </div>
                                                <!--  -->
                                            </div>
                                            <div class="vender-detail">
                                                <a href="product-list-2.html" class="vender-name">Anand Multi Trade</a>
                                                <p>+91 8889596562</p>
                                                <a href="#" class="vender-email">Info@gmail.com</a>
                                                <p>India</p>
                                                
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 float-left m-b-10 p-l-t-7">
                                        <div class="main-vendor-box bg-white com-box-shodow rounded float-left">
                                            <div class="vendor-img">
                                                <div class="vendor-image">
                                                    <img src="images/logo-3.png">
                                                </div>
                                                <!--  -->
                                            </div>
                                            <div class="vender-detail">
                                                <a href="product-list-2.html" class="vender-name">Anand Multi Trade</a>
                                                <p>+91 8889596562</p>
                                                <a href="#" class="vender-email">Info@gmail.com</a>
                                                <p>India</p>
                                                
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 float-left m-b-10 p-l-t-7">
                                        <div class="main-vendor-box bg-white com-box-shodow rounded float-left">
                                            <div class="vendor-img">
                                                <div class="vendor-image">
                                                    <img src="images/logo-4.png">
                                                </div>
                                                <!--  -->
                                            </div>
                                            <div class="vender-detail">
                                                <a href="product-list-2.html" class="vender-name">Anand Multi Trade</a>
                                                <p>+91 8889596562</p>
                                                <a href="#" class="vender-email">Info@gmail.com</a>
                                                <p>India</p>
                                                
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
@endsection

      
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="js/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <!-- <script src="js/popper.js"></script> -->
    <script src="js/bootstrap.min.js"></script>
    <script src="js/material.min.js"></script>
    
    <script defer src="js/solid.js"></script>
    <script defer src="js/fontawesome.js"></script>
    <script src="js/canvasjs.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#sidebarCollapse').on('click', function () {
                $('#sidebar').toggleClass('active');
                $('.main-container').toggleClass('open-menu');
            });
        });
    </script>
   <script type="text/javascript">
window.onload = function () {

var chart = new CanvasJS.Chart("chartContainer", {
    animationEnabled: true,
    title:{
        text: "Total Sales"
    },
    axisY: {
        title: "    "
    },
    legend: {
        cursor:"pointer",
        itemclick : toggleDataSeries
    },
    toolTip: {
        shared: true,
        content: toolTipFormatter
    },
    data: [{
        type: "bar",
        showInLegend: true,
        name: "New Order receives",
        color: "gold",
        dataPoints: [
            { y: 243, label: "Monday" },
            { y: 236, label: "Tuesday" },
            { y: 243, label: "Wednesday" },
            { y: 273, label: "Thursday" },
            { y: 269, label: "Friday" },
            { y: 196, label: "Saturday" },
            { y: 1118, label: "Sunday" }
        ]
    },
    {
        type: "bar",
        showInLegend: true,
        name: "Item to be Delivered",
        color: "silver",
        dataPoints: [
            { y: 212, label: "Monday" },
            { y: 186, label: "Tuesday" },
            { y: 272, label: "Wednesday" },
            { y: 299, label: "Thursday" },
            { y: 270, label: "Friday" },
            { y: 165, label: "Saturday" },
            { y: 896, label: "Sunday" }
        ]
    },
    {
        type: "bar",
        showInLegend: true,
        name: "Item to be Packed",
        color: "#A57164",
        dataPoints: [
            { y: 236, label: "Monday" },
            { y: 172, label: "Tuesday" },
            { y: 309, label: "Wednesday" },
            { y: 302, label: "Thursday" },
            { y: 285, label: "Friday" },
            { y: 188, label: "Saturday" },
            { y: 788, label: "Sunday" }
        ]
    }]
});
chart.render();

function toolTipFormatter(e) {
    var str = "";
    var total = 0 ;
    var str3;
    var str2 ;
    for (var i = 0; i < e.entries.length; i++){
        var str1 = "<span style= 'color:"+e.entries[i].dataSeries.color + "'>" + e.entries[i].dataSeries.name + "</span>: <strong>"+  e.entries[i].dataPoint.y + "</strong> <br/>" ;
        total = e.entries[i].dataPoint.y + total;
        str = str.concat(str1);
    }
    str2 = "<strong>" + e.entries[0].dataPoint.label + "</strong> <br/>";
    str3 = "<span style = 'color:Tomato'>Total: </span><strong>" + total + "</strong><br/>";
    return (str2.concat(str)).concat(str3);
}

function toggleDataSeries(e) {
    if (typeof (e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
        e.dataSeries.visible = false;
    }
    else {
        e.dataSeries.visible = true;
    }
    chart.render();
}

}
</script>
<script>
        $(document).ready(function() {
            $('#example').DataTable();
        } );

        equalheight = function(container){

        var currentTallest = 0,
             currentRowStart = 0,
             rowDivs = new Array(),
             $el,
             topPosition = 0;
         $(container).each(function() {

           $el = $(this);
           $($el).height('auto')
           topPostion = $el.position().top;

           if (currentRowStart != topPostion) {
             for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
               rowDivs[currentDiv].height(currentTallest);
             }
             rowDivs.length = 0; // empty the array
             currentRowStart = topPostion;
             currentTallest = $el.height();
             rowDivs.push($el);
           } else {
             rowDivs.push($el);
             currentTallest = (currentTallest < $el.height()) ? ($el.height()) : (currentTallest);
          }
           for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
             rowDivs[currentDiv].height(currentTallest);
           }
         });
        }

        $(window).load(function() {
          equalheight('.midd-btm-same');  
        });


        $(window).resize(function(){
          equalheight('.midd-btm-same');
        });
    </script>
  </body>
</html>

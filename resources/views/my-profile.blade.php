    <!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Welcome</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <link href="css/material.min.css" rel="stylesheet">
    <link href="css/getmdl-select.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/menu-css.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="css/responsive.css">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
      <div class="main">
          <aside>
              <nav id="sidebar" class="active">
                  <div class="sidebar-header">
                      <h3>Logo</h3>
                      <strong>LG</strong>
                  </div>
                  <ul class="list-unstyled components">
                    <!-- <li class="active">
                        <a href="#">
                            <i class="fas fa-briefcase"></i>
                            <span>About</span>
                        </a> -->
<!--
                        <a href="#homeSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                            <i class="fas fa-home"></i>
                            <span>Home</span>
                        </a>
-->
<!--
                        <ul class="collapse list-unstyled" id="homeSubmenu">
                            <li>
                                <a href="#">Home 1</a>
                            </li>
                            <li>
                                <a href="#">Home 2</a>
                            </li>
                            <li>
                                <a href="#">Home 3</a>
                            </li>
                        </ul>
-->
                    <!-- </li> -->
                    <li>
                        <a href="index.html">
                            <i class="fas fa-home"></i>
                            <span>Home</span>
                        </a>
                    </li>
                    <li>
                        <a href="order.html">
                            <i class="fas fa-clipboard-list"></i>
                            <span>Order</span>
                        </a>
                    </li>
                    <li>
                        <a href="catalogue.html">
                            <i class="fas fa-th"></i>
                            <span> Catalogue </span>
                        </a>
                    </li>
                    <li>
                        <a href="settings.html">
                            <i class="fas fa-cog"></i>
                            <span>Setting</span>
                        </a>
                    </li>
                    <li>
                        <a href="vender-list.html">
                            <i class="fas fa-user-shield"></i>
                            <span>My Vender</span>
                        </a>
                    </li>
                    <li>
                        <a href="invoice-listing.html">
                            <i class="fas fa-file-alt"></i>
                            <span>Invoice</span>
                        </a>
                    </li>
                </ul>
            </nav>
        </aside>
        <div class="main-container">
            <header>
                <div class="container-fluid">
                    <div class="row">
                        <div class="main-haeder">
                            <button type="button" id="sidebarCollapse" class="menu-icon-main float-left">
                                <img src="images/menu-icon.svg">
                            </button>
                            <div class="search-bar col-sm-4 col-md-4 col-xs-6 float-left">
                                <form action="#">
                                  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                    <input class="mdl-textfield__input" type="text" id="sample3">
                                    <label class="mdl-textfield__label" for="sample3">Search...</label>
                                  </div>
                                </form>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 float-right p-r-0">
                                <div class="profile-box-new">
                                    <div class="profile-picture cart-icon noti-icon">
                                        <div class="por-img rounded-circle mt-1">
                                            <div class="dropdown ">
                                                <span class="dropdown-toggle" data-counter="2" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <i class="fas fa-bell"></i>
                                                </span>
                                              </div>
                                        </div>
                                    </div>
                                    <div class="profile-picture cart-icon">
                                        <div class="por-img rounded-circle mt-1">
                                            <div class="dropdown ">
                                                <span class="dropdown-toggle" data-counter="2" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <i class="fas fa-shopping-cart"></i>    
                                                </span>
                                                <div class="dropdown-menu dropdown-menu-right com-box-shodow b-0 drop-menu cart-menu" aria-labelledby="dropdownMenuButton">
                                                    <div class="caet-row">
                                                        <div class="cart-img">  
                                                            <img src="images/pro-3.jpg">
                                                        </div>
                                                        <div class="cart-detail">
                                                            <a href="#">Save Khamni</a>
                                                            <p>Stock Qty 6</p>
                                                        </div>
                                                    </div>
                                                    <div class="caet-row">
                                                        <div class="cart-img">  
                                                            <img src="images/pro-3.jpg">
                                                        </div>
                                                        <div class="cart-detail">
                                                            <a href="#">Save Khamni</a>
                                                            <p>Stock Qty 6</p>
                                                        </div>
                                                    </div>
                                                </div>
                                              </div>
                                        </div>
                                    </div>
                                    <div class="profile-picture">
                                        <div class="por-img rounded-circle mt-1">
                                            <div class="dropdown ">
                                                <span class="dropdown-toggle" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <img src="images/profile-pic.jpg" class="pro-img-main">
                                                    <i class="fas fa-angle-down"></i>
                                                </span>
                                                <div class="dropdown-menu dropdown-menu-right com-box-shodow b-0 drop-menu" aria-labelledby="dropdownMenuButton">
                                                <a class="dropdown-item" href="my-profile.html"><i class="fas fa-user-alt"></i> Profile</a>
                                                <a class="dropdown-item" href="settings.html"><i class="fas fa-cog"></i> Settings</a>
                                                <a class="dropdown-item" href="login.html"><i class="fas fa-power-off"></i> Logout</a>
                                              </div>
                                              </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
            
            <div class="main-order-section">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="bread-crum">
                                <nav class="breadcrumb">
                                  <a class="breadcrumb-item" href="index.html">Home</a>
                                  <span class="breadcrumb-item active">My Profile</span>
                                </nav>
                            </div>
                            <div class="vender-list-top-add-btn">
                                <h1>My Profile</h1>
                            </div>
                                <div class="main-profile-page bg-white com-box-shodow rounded float-left">
                                    <div class="profile-left-side">
                                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 float-left p-l-0">
                                            <div class="profile-div">
                                                <div class="progile-image my-profile midd-btm-same">
                                                    <div class="al">
                                                        <div class="alm">
                                                            <div class="profile-img-img">
                                                                <div class="add-cat-img-main">
                                                                    <div class="add-cat-img">
                                                                        <input name="" type="file">
                                                                        <i class="fas fa-plus-circle"></i>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 float-left">
                                            <div class="vender-detail-info midd-btm-same">
                                                <div class="top-tab-penal">
                                                    <div class="form-content">     
                                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 offset-lg-2 float-left">
                                                            <div class="frm-group">
                                                                <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                                                   <input class="mdl-textfield__input" type="text" id="sample3">
                                                                   <label class="mdl-textfield__label" for="sample3">First Name</label>
                                                                </div>
                                                            </div>
                                                            <div class="frm-group">
                                                                <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                                                   <input class="mdl-textfield__input" type="text" id="sample3">
                                                                   <label class="mdl-textfield__label" for="sample3">Lasr Name</label>
                                                                </div>
                                                            </div>
                                                            <div class="frm-group">
                                                                <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                                                   <input class="mdl-textfield__input" type="text" id="sample3">
                                                                   <label class="mdl-textfield__label" for="sample3">Email</label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 float-left">
                                                            <div class="frm-group">
                                                                <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                                                   <input class="mdl-textfield__input" type="text" id="sample3">
                                                                   <label class="mdl-textfield__label" for="sample3">Number</label>
                                                                </div>
                                                            </div>
                                                            <div class="frm-group">
                                                                <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                                                   <input class="mdl-textfield__input" type="text" id="sample3">
                                                                   <label class="mdl-textfield__label" for="sample3">Business Name</label>
                                                                </div>
                                                            </div>
                                                            <div class="frm-group">
                                                                <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                                                   <input class="mdl-textfield__input" type="text" id="sample3">
                                                                   <label class="mdl-textfield__label" for="sample3">Address</label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                           <div class="invoice-btn tax-submit-btn add-vender-btn">
                                                <div class="btn-group float-right">
                                                    <a href="index.html">Submit</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


      
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="js/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/popper.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/material.min.js"></script>
    <script src="js/getmdl-select.js"></script>
    <script defer src="js/solid.js"></script>
    <script defer src="js/fontawesome.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            
            $('#sidebarCollapse').on('click', function () {
                $('#sidebar').toggleClass('active');
                $('.main-container').toggleClass('open-menu');
            });

            $('#weight-id .mdl-menu__item').on('click',function(){
                $('.packe-wight').css('display','none');
                if($(this).data("val") == "GRM"){
                    $('#weight-1').css('display','block');
                }
                else if($(this).data("val") == "KLM"){
                    $('#weight-3').css('display','block');
                }
                else if($(this).data("val") == "LTR"){
                    $('#weight-2').css('display','block');
                }
            })
        });

    </script>
    <script>
        $(document).ready(function() {
            $('#example').DataTable();
        } );

        equalheight = function(container){

        var currentTallest = 0,
             currentRowStart = 0,
             rowDivs = new Array(),
             $el,
             topPosition = 0;
         $(container).each(function() {

           $el = $(this);
           $($el).height('auto')
           topPostion = $el.position().top;

           if (currentRowStart != topPostion) {
             for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
               rowDivs[currentDiv].height(currentTallest);
             }
             rowDivs.length = 0; // empty the array
             currentRowStart = topPostion;
             currentTallest = $el.height();
             rowDivs.push($el);
           } else {
             rowDivs.push($el);
             currentTallest = (currentTallest < $el.height()) ? ($el.height()) : (currentTallest);
          }
           for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
             rowDivs[currentDiv].height(currentTallest);
           }
         });
        }

        $(window).load(function() {
          equalheight('.midd-btm-same');  
        });


        $(window).resize(function(){
          equalheight('.midd-btm-same');
        });
    </script>
  </body>
</html>
@extends('layouts.app')

@section('content')  
            <div class="main-order-section">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="bread-crum">
                                <nav class="breadcrumb">
                                  <a class="breadcrumb-item" href="index.html">Home</a>
                                  <span class="breadcrumb-item active">Add Product</span>
                                </nav>
                            </div>
                            <div class="vender-list-top-add-btn">
                                <h1>Add Product</h1>
                            </div>
                                <div class="main-profile-page add-product-box bg-white com-box-shodow rounded float-left">
                                    <div class="profile-left-side">
                                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 float-left p-l-0">
                                            <div class="profile-div">
                                                <div class="progile-image add-vernder-profile add-product-img">
                                                    <div class="al">
                                                        <div class="alm">
                                                            <div class="profile-img-img">
                                                                <div class="add-cat-img-main">
                                                                    <div class="add-cat-img">
                                                                        <input name="" type="file">
                                                                        <i class="fas fa-plus-circle"></i>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 float-left">
                                            <div class="vender-detail-info">
                                                <div class="top-tab-penal">
                                                    <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                                                      <li class="nav-item">
                                                        <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#w-app" role="tab" aria-controls="pills-home" aria-selected="true">Product Details</a>
                                                      </li>
                                                      <li class="nav-item">
                                                        <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#out-dill" role="tab" aria-controls="pills-profile" aria-selected="false">Shipping</a>
                                                      </li>
                                                      <li class="nav-item">
                                                        <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#wp-pricing" role="tab" aria-controls="pills-profile" aria-selected="false">Pricing</a>
                                                      </li>
                                                    </ul>
                                                    <div class="tab-content" id="pills-tabContent">
                                                        <div class="tab-pane fade show active" id="w-app" role="tabpanel" aria-labelledby="pills-home-tab">
                                                            <div class="form-content">     
                                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 offset-lg-2 float-left">
                                                                    <div class="frm-group">
                                                                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                                                           <input class="mdl-textfield__input" type="text" id="sample3">
                                                                           <label class="mdl-textfield__label" for="sample3">Product Name</label>
                                                                        </div>
                                                                    </div>
                                                                    <div class="frm-group">
                                                                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                                                           <input class="mdl-textfield__input" type="text" id="sample3">
                                                                           <label class="mdl-textfield__label" for="sample3">Product Discount</label>
                                                                        </div>
                                                                    </div>
                                                                    <div class="frm-group">
                                                                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                                                           <input class="mdl-textfield__input" type="text" id="sample3">
                                                                           <label class="mdl-textfield__label" for="sample3">Brand</label>
                                                                        </div>
                                                                    </div>
                                                                    <div class="frm-group">
                                                                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                                                           <input class="mdl-textfield__input" type="text" id="sample3">
                                                                           <label class="mdl-textfield__label" for="sample3">Categories</label>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 float-left">
                                                                    <div class="frm-group">
                                                                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                                                           <input class="mdl-textfield__input" type="text" id="sample3">
                                                                           <label class="mdl-textfield__label" for="sample3">Quantities</label>
                                                                        </div>
                                                                    </div>
                                                                    <div class="frm-group">
                                                                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                                                           <input class="mdl-textfield__input" type="text" id="sample3">
                                                                           <label class="mdl-textfield__label" for="sample3">Minimum Quantities</label>
                                                                        </div>
                                                                    </div>
                                                                
                                                                    <div class="frm-group">
                                                                          <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select">
                                                                            <input type="text" value="" class="mdl-textfield__input" id="sample4" readonly>
                                                                            <input type="hidden" value="" name="sample4">
                                                                            <i class="mdl-icon-toggle__label material-icons"></i>
                                                                            <label for="sample4" class="mdl-textfield__label">Add Package Weight</label>
                                                                            <ul id="weight-id" for="sample4" class="mdl-menu mdl-menu--bottom-left mdl-js-menu">
                                                                                <li class="mdl-menu__item" data-val="GRM" onclick="add_weight(1)">Gram</li>
                                                                                <li class="mdl-menu__item" data-val="KLM" onclick="add_weight(2)">kilogram</li>
                                                                                <li class="mdl-menu__item" data-val="LTR" onclick="add_weight(3)">Liter</li>
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                    <div class="frm-group packe-wight" id="weight-1">
                                                                         <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select">
                                                                            <input type="text" value="" class="mdl-textfield__input" id="sample7" readonly>
                                                                            <input type="hidden" value="" name="sample7">
                                                                            <i class="mdl-icon-toggle__label material-icons"></i>
                                                                            <label for="sample7" class="mdl-textfield__label">Gram</label>
                                                                            <ul for="sample7" class="mdl-menu mdl-menu--bottom-left mdl-js-menu">
                                                                                <li class="mdl-menu__item" data-val="100gm">100gm</li>
                                                                                <li class="mdl-menu__item" data-val="200gm">300gm</li>
                                                                                <li class="mdl-menu__item" data-val="300gm">300gm</li>
                                                                                <li class="mdl-menu__item" data-val="400gm">400gm</li>
                                                                                <li class="mdl-menu__item" data-val="500gm">500gm</li>
                                                                                <li class="mdl-menu__item" data-val="600gm">600gm</li>
                                                                                <li class="mdl-menu__item" data-val="700gm">700gm</li>
                                                                                <li class="mdl-menu__item" data-val="800gm">800gm</li>
                                                                                <li class="mdl-menu__item" data-val="900gm">900gm</li>
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                    <div class="frm-group packe-wight" id="weight-3">
                                                                         <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select">
                                                                            <input type="text" value="" class="mdl-textfield__input" id="sample9" readonly>
                                                                            <input type="hidden" value="" name="sample9">
                                                                            <i class="mdl-icon-toggle__label material-icons"></i>
                                                                            <label for="sample9" class="mdl-textfield__label">Kilogram</label>
                                                                            <ul for="sample9" class="mdl-menu mdl-menu--bottom-left mdl-js-menu">
                                                                                <li class="mdl-menu__item" data-val="1kg">1kg</li>
                                                                                <li class="mdl-menu__item" data-val="2kg">2kg</li>
                                                                                <li class="mdl-menu__item" data-val="3kg">3kg</li>
                                                                                <li class="mdl-menu__item" data-val="4kg">4kg</li>
                                                                                <li class="mdl-menu__item" data-val="5kg">5kg</li>
                                                                                <li class="mdl-menu__item" data-val="6kg">6kg</li>
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                    <div class="frm-group packe-wight" id="weight-2">
                                                                         <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select">
                                                                            <input type="text" value="" class="mdl-textfield__input" id="sample8" readonly>
                                                                            <input type="hidden" value="" name="sample8">
                                                                            <i class="mdl-icon-toggle__label material-icons"></i>
                                                                            <label for="sample8" class="mdl-textfield__label">Liter</label>
                                                                            <ul for="sample8" class="mdl-menu mdl-menu--bottom-left mdl-js-menu">
                                                                                <li class="mdl-menu__item" data-val="1liter">1 liter</li>
                                                                                <li class="mdl-menu__item" data-val="2liter">2 liter</li>
                                                                                <li class="mdl-menu__item" data-val="3liter">3 liter</li>
                                                                                <li class="mdl-menu__item" data-val="4liter">4 liter</li>
                                                                                <li class="mdl-menu__item" data-val="5liter">5 liter</li>
                                                                                <li class="mdl-menu__item" data-val="6liter">6 liter</li>
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                    
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="tab-pane fade" id="out-dill" role="tabpanel" aria-labelledby="pills-profile-tab">
                                                             <div class="form-content">     
                                                                <div class="col-lg-8 offset-lg-2 float-left packe-dimation">
                                                                    <h6>Package dimension</h6>
                                                                    <p>Charge additional shipping costs based on packet dimensions coverd here</p>
                                                                </div>
                                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 offset-lg-2 float-left">
                                                                    <div class="frm-group">
                                                                        <h6>Package Weight</h6>
                                                                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                                                           <input class="mdl-textfield__input" type="text" id="sample3">
                                                                           <label class="mdl-textfield__label" for="sample3">Width (cm)</label>
                                                                        </div>
                                                                    </div>
                                                                    <div class="frm-group">
                                                                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                                                           <input class="mdl-textfield__input" type="text" id="sample3">
                                                                           <label class="mdl-textfield__label" for="sample3">Height (cm)</label>
                                                                        </div>
                                                                    </div>
                                                                    <div class="frm-group">
                                                                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                                                           <input class="mdl-textfield__input" type="text" id="sample3">
                                                                           <label class="mdl-textfield__label" for="sample3">Depth (cm)</label>
                                                                        </div>
                                                                    </div>
                                                                    <div class="frm-group">
                                                                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                                                           <input class="mdl-textfield__input" type="text" id="sample3">
                                                                           <label class="mdl-textfield__label" for="sample3">weight (kg)</label>
                                                                        </div>
                                                                    </div>
                                                                    <div class="main-gender-grp">
                                                                        <div class="frm-group">
                                                                            <h6>Delivery time</h6>
                                                                            <label class="mdl-radio mdl-js-radio mdl-js-ripple-effect" for="option-1">
                                                                              <input type="radio" id="option-1" class="mdl-radio__button" name="options" value="1" checked>
                                                                              <span class="mdl-radio__label">None</span>
                                                                            </label>
                                                                            <label class="mdl-radio mdl-js-radio mdl-js-ripple-effect" for="option-2">
                                                                              <input type="radio" id="option-2" class="mdl-radio__button" name="options" value="2">
                                                                              <span class="mdl-radio__label">Default delivery time</span>
                                                                            </label>
                                                                            <label class="mdl-radio mdl-js-radio mdl-js-ripple-effect" for="option-2">
                                                                              <input type="radio" id="option-2" class="mdl-radio__button" name="options" value="2">
                                                                              <span class="mdl-radio__label">Spacific delivery to this product</span>
                                                                            </label>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 float-left">
                                                                     <div class="frm-group">
                                                                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                                                           <input class="mdl-textfield__input" type="text" id="sample3">
                                                                           <label class="mdl-textfield__label" for="sample3">Delivery time of in stock product</label>
                                                                        </div>
                                                                    </div>
                                                                    <div class="frm-group">
                                                                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                                                           <input class="mdl-textfield__input" type="text" id="sample3">
                                                                           <label class="mdl-textfield__label" for="sample3">Delivery time of out of stock product</label>
                                                                        </div>
                                                                    </div>
                                                                    <div class="frm-group">
                                                                        <h6>Shipping Fees</h6>
                                                                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                                                           <input class="mdl-textfield__input" type="text" id="sample3">
                                                                           <label class="mdl-textfield__label" for="sample3">Does this product include additional shipping costs</label>
                                                                        </div>
                                                                    </div>
                                                                    <div class="frm-group">
                                                                        <h6>Avaliable carriers</h6>
                                                                        <label class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect" for="checkbox-1">
                                                                            <input type="checkbox" id="checkbox-1" class="mdl-checkbox__input">
                                                                            <span class="mdl-checkbox__label">PrestaShop (Pick up in-store)</span>
                                                                        </label>
                                                                        <label class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect" for="checkbox-2">
                                                                            <input type="checkbox" id="checkbox-2" class="mdl-checkbox__input">
                                                                            <span class="mdl-checkbox__label">My carrier (Delvery next day)</span>
                                                                        </label>
                                                                    </div>
                                                                   
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="tab-pane fade" id="wp-pricing" role="tabpanel" aria-labelledby="pills-profile-tab">
                                                             <div class="form-content">     
                                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 offset-lg-2 float-left">
                                                                    <div class="frm-group">
                                                                        <h6>Retail price</h6>
                                                                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                                                           <input class="mdl-textfield__input" type="text" id="sample3">
                                                                           <label class="mdl-textfield__label" for="sample3">Price (Tax excl.)</label>
                                                                        </div>
                                                                    </div>
                                                                    <div class="frm-group">
                                                                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                                                           <input class="mdl-textfield__input" type="text" id="sample3">
                                                                           <label class="mdl-textfield__label" for="sample3">Price (Tax incl.)</label>
                                                                        </div>
                                                                    </div>
                                                                    <div class="frm-group">
                                                                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                                                           <input class="mdl-textfield__input" type="text" id="sample3">
                                                                           <label class="mdl-textfield__label" for="sample3">Price per unit (Tax excl.)</label>
                                                                        </div>
                                                                    </div>
                                                                    <div class="frm-group">
                                                                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                                                           <input class="mdl-textfield__input" type="text" id="sample3">
                                                                           <label class="mdl-textfield__label" for="sample3">Per Kilo,per liter</label>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 float-left">
                                                                     <div class="frm-group">
                                                                         <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select">
                                                                            <input type="text" value="" class="mdl-textfield__input" id="sample10" readonly>
                                                                            <input type="hidden" value="" name="sample10">
                                                                            <i class="mdl-icon-toggle__label material-icons"></i>
                                                                            <label for="sample10" class="mdl-textfield__label">Tax Rule </label>
                                                                            <ul for="sample10" class="mdl-menu mdl-menu--bottom-left mdl-js-menu">
                                                                                <li class="mdl-menu__item" data-val="1liter">US-AL Rate 4%</li>
                                                                                <li class="mdl-menu__item" data-val="2liter">US-AL Rate 4%</li>
                                                                                <li class="mdl-menu__item" data-val="3liter">US-AL Rate 4%</li>
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                     <div class="frm-group">
                                                                        <h6>Cost price</h6>
                                                                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                                                           <input class="mdl-textfield__input" type="text" id="sample3">
                                                                           <label class="mdl-textfield__label" for="sample3">Price (Tax excl.)</label>
                                                                        </div>
                                                                    </div>
                                                                    
                                                                    
                                                                   
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="invoice-btn tax-submit-btn add-vender-btn">
                                                            <div class="btn-group float-right">
                                                                <a href="index.html">Submit</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
@endsection

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="js/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <!-- <script src="js/popper.js"></script> -->
    <script src="js/bootstrap.min.js"></script>
    <script src="js/material.min.js"></script>
    <script src="js/getmdl-select.js"></script>
    <script defer src="js/solid.js"></script>
    <script defer src="js/fontawesome.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            
            $('#sidebarCollapse').on('click', function () {
                $('#sidebar').toggleClass('active');
                $('.main-container').toggleClass('open-menu');
            });

            $('#weight-id .mdl-menu__item').on('click',function(){
                $('.packe-wight').css('display','none');
                if($(this).data("val") == "GRM"){
                    $('#weight-1').css('display','block');
                }
                else if($(this).data("val") == "KLM"){
                    $('#weight-3').css('display','block');
                }
                else if($(this).data("val") == "LTR"){
                    $('#weight-2').css('display','block');
                }
            })
        });

    </script>
    <script>
        $(document).ready(function() {
            $('#example').DataTable();
        } );

        equalheight = function(container){

        var currentTallest = 0,
             currentRowStart = 0,
             rowDivs = new Array(),
             $el,
             topPosition = 0;
         $(container).each(function() {

           $el = $(this);
           $($el).height('auto')
           topPostion = $el.position().top;

           if (currentRowStart != topPostion) {
             for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
               rowDivs[currentDiv].height(currentTallest);
             }
             rowDivs.length = 0; // empty the array
             currentRowStart = topPostion;
             currentTallest = $el.height();
             rowDivs.push($el);
           } else {
             rowDivs.push($el);
             currentTallest = (currentTallest < $el.height()) ? ($el.height()) : (currentTallest);
          }
           for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
             rowDivs[currentDiv].height(currentTallest);
           }
         });
        }

        $(window).load(function() {
          equalheight('.midd-btm-same');  
        });


        $(window).resize(function(){
          equalheight('.midd-btm-same');
        });
    </script>
  </body>
</html>

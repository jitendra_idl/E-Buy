<header>
                <div class="container-fluid">
                    <div class="row">
                        <div class="main-haeder">
                            <button type="button" id="sidebarCollapse" class="menu-icon-main float-left">
                                <img src="/images/menu-icon.svg">
                            </button>
                            <div class="search-bar col-sm-4 col-md-4 col-xs-6 float-left">
                                <form action="#">
                                  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                    <input class="mdl-textfield__input" type="text" id="sample3">
                                    <label class="mdl-textfield__label" for="sample3">Search...</label>
                                  </div>
                                </form>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 float-right p-r-0">
                                <div class="profile-box-new">
                                    <div class="profile-picture cart-icon noti-icon">
                                        <div class="por-img rounded-circle mt-1">
                                            <div class="dropdown ">
                                                
                                                    <span class="dropdown-toggle" data-counter="2" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        <i class="fas fa-bell"></i>
                                                    </span>
                                                </a>
                                              </div>
                                        </div>
                                    </div>
                                    <div class="profile-picture cart-icon">
                                        <div class="por-img rounded-circle mt-1">
                                            <div class="dropdown ">
                                                <span class="dropdown-toggle" data-counter="2" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <i class="fas fa-shopping-cart"></i>    
                                                </span>
                                                <div class="dropdown-menu dropdown-menu-right com-box-shodow b-0 drop-menu cart-menu" aria-labelledby="dropdownMenuButton">
                                                    <div class="caet-row">
                                                        <div class="cart-img">  
                                                            <img src="/images/pro-3.jpg">
                                                        </div>
                                                        <div class="cart-detail">
                                                            <a href="#">Save Khamni</a>
                                                            <p>Stock Qty 6</p>
                                                        </div>
                                                    </div>
                                                    <div class="caet-row">
                                                        <div class="cart-img">  
                                                            <img src="/images/pro-3.jpg">
                                                        </div>
                                                        <div class="cart-detail">
                                                            <a href="#">Save Khamni</a>
                                                            <p>Stock Qty 6</p>
                                                        </div>
                                                    </div>
                                                </div>
                                              </div>
                                        </div>
                                    </div>

@guest
                                    <div class="profile-picture">
                                        <div class="por-img rounded-circle mt-1">
                                        	<div class="dropdown ">
                                        		<span class="dropdown-toggle" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    												<img src="/images/profile-pic.jpg" class="pro-img-main">
    												<i class="fas fa-angle-down"></i>
    											</span>
                                            	<div class="dropdown-menu dropdown-menu-right com-box-shodow b-0 drop-menu" aria-labelledby="dropdownMenuButton">
    										    <a class="dropdown-item" href="my-profile.html"><i class="fas fa-user-alt"></i> Profile</a>
    										    <a class="dropdown-item" href="settings.html"><i class="fas fa-cog"></i> Settings</a>
    										    <a class="dropdown-item" href="login.html"><i class="fas fa-power-off"></i> Logout</a>
    										  </div>
    										  </div>
                                        </div>
                                    </div>
                                    @else

                                     <div class="profile-picture">
                                        <div class="por-img rounded-circle mt-1">
                                        	<div class="dropdown ">
                                        		<span class="dropdown-toggle" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    												<img src="/images/profile-pic.jpg" class="pro-img-main">
    												<i class="fas fa-angle-down"></i>
    											</span>
                                            	<div class="dropdown-menu dropdown-menu-right com-box-shodow b-0 drop-menu" aria-labelledby="dropdownMenuButton">
    										    <a class="dropdown-item" href="my-profile.html"><i class="fas fa-user-alt"></i> Profile</a>
    										    <a class="dropdown-item" href="settings.html"><i class="fas fa-cog"></i> Settings</a>
                                                <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                       </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>

    										    <!-- <a class="dropdown-item" href="login.html"><i class="fas fa-power-off"></i> Logout</a> -->
    										  </div>
    										  </div>
                                        </div>
                                    </div>

                                    @endguest



                                </div>
                            </div>
                        </div>
                    </div>
                </div>
</header>

<script src="js/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/popper.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/material.min.js"></script>
    <script src="js/getmdl-select.js"></script>
    <script defer src="js/solid.js"></script>
    <script defer src="js/fontawesome.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            
            $('#sidebarCollapse').on('click', function () {
                $('#sidebar').toggleClass('active');
                $('.main-container').toggleClass('open-menu');
            });
        });
    </script>
    <script>
        $(document).ready(function() {
            $('#example').DataTable();
        } );

        equalheight = function(container){

        var currentTallest = 0,
             currentRowStart = 0,
             rowDivs = new Array(),
             $el,
             topPosition = 0;
         $(container).each(function() {

           $el = $(this);
           $($el).height('auto')
           topPostion = $el.position().top;

           if (currentRowStart != topPostion) {
             for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
               rowDivs[currentDiv].height(currentTallest);
             }
             rowDivs.length = 0; // empty the array
             currentRowStart = topPostion;
             currentTallest = $el.height();
             rowDivs.push($el);
           } else {
             rowDivs.push($el);
             currentTallest = (currentTallest < $el.height()) ? ($el.height()) : (currentTallest);
          }
           for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
             rowDivs[currentDiv].height(currentTallest);
           }
         });
        }

        $(window).load(function() {
          equalheight('.midd-btm-same');  
        });


        $(window).resize(function(){
          equalheight('.midd-btm-same');
        });
    </script>
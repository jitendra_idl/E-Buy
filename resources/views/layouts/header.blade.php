<aside>
              <nav id="sidebar" class="active">
                  <div class="sidebar-header">
                      <h3>Logo</h3>
                      <strong>LG</strong>
                  </div>
                  <ul class="list-unstyled components">
                    <li class="active">
                        <a href="{{ route('home') }}">
                            <i class="fas fa-home"></i>
                            <span>Home</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('order') }}">
                            <i class="fas fa-clipboard-list"></i>
                            <span>Order</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('catalogue') }}">
                            <i class="fas fa-th"></i>
                            <span> Catalogue </span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('settings') }}">
                            <i class="fas fa-cog"></i>
                            <span>Setting</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('venderList') }}">
                            <i class="fas fa-user-shield"></i>
                            <span>My Vender</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('invoiceList') }}">
                            <i class="fas fa-file-alt"></i>
                            <span>Invoice</span>
                        </a>
                    </li>
                </ul>
            </nav>
        </aside>
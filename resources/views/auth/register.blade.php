@extends('layouts.app')

@section('content')

<form method="POST" action="{{ route('register') }}" aria-label="{{ __('Register') }}">
                        @csrf
                <div class="main">
                        <div class="login-form-main-content">
                            <div class="al">
                                <div class="alm">
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 fomr-width">
                                        <div class="form-back bg-white com-box-shodow float-left p-5 rounded">
                                            <h4>Sign Up</h4>
                                            <div class="frm-group">
                                                <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                                    <input class="mdl-textfield__input" type="text" id="sample3" name="name">
                                                    <label class="mdl-textfield__label" for="sample3">Name</label>
                                                </div>
                                            </div>
                                            <div class="frm-group">
                                                <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                                    <input class="mdl-textfield__input" type="text" id="sample3" name="email">
                                                    <label class="mdl-textfield__label" for="sample3">Login OR Email</label>
                                                </div>
                                            </div>
                                            <div class="frm-group">
                                                <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                                    <input class="mdl-textfield__input" type="text" id="sample3" name="password">
                                                    <label class="mdl-textfield__label" for="sample3">Password</label>
                                                </div>
                                            </div>
                                            <div class="frm-group">
                                                <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                                    <input class="mdl-textfield__input" type="text" id="sample3" name="password_confirmation">
                                                    <label class="mdl-textfield__label" for="sample3">Repeat Password</label>
                                                </div>
                                            </div>
                                            <div class="forgot-link">
                                                <div class="float-left">
                                                    <label class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect" for="checkbox-1">
                                                        <input type="checkbox" id="checkbox-1" class="mdl-checkbox__input" checked>
                                                        <span class="mdl-checkbox__label">I agree to all Terms & Condition</span>
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="login-foo-btn">
                                            <button type="submit" class="model-btn m-r-10">
                                             {{ __('Register') }}
                                           </button>
                                                <a href="{{ route('login') }}" class="model-btn regiser-btn">Login</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>
</form>






@endsection

 <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
 <script src="js/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/popper.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/material.min.js"></script>
    <script src="js/getmdl-select.js"></script>
    <script defer src="js/solid.js"></script>
    <script defer src="js/fontawesome.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            
            $('#sidebarCollapse').on('click', function () {
                $('#sidebar').toggleClass('active');
                $('.main-container').toggleClass('open-menu');
            });
        });
    </script>
    <script>
        $(document).ready(function() {
            $('#example').DataTable();
        } );

        equalheight = function(container){

        var currentTallest = 0,
             currentRowStart = 0,
             rowDivs = new Array(),
             $el,
             topPosition = 0;
         $(container).each(function() {

           $el = $(this);
           $($el).height('auto')
           topPostion = $el.position().top;

           if (currentRowStart != topPostion) {
             for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
               rowDivs[currentDiv].height(currentTallest);
             }
             rowDivs.length = 0; // empty the array
             currentRowStart = topPostion;
             currentTallest = $el.height();
             rowDivs.push($el);
           } else {
             rowDivs.push($el);
             currentTallest = (currentTallest < $el.height()) ? ($el.height()) : (currentTallest);
          }
           for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
             rowDivs[currentDiv].height(currentTallest);
           }
         });
        }

        $(window).load(function() {
          equalheight('.midd-btm-same');  
        });


        $(window).resize(function(){
          equalheight('.midd-btm-same');
        });
    </script>

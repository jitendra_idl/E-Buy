@extends('layouts.app')

@section('content')  
            
            <div class="main-order-section">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="bread-crum">
                                <nav class="breadcrumb">
                                  <a class="breadcrumb-item" href="index.html">Home</a>
                                  <a class="breadcrumb-item" href="catalogue.html">Catalouges</a>
                                  <a class="breadcrumb-item" href="product-list.html">Product Listing</a>
                                  <span class="breadcrumb-item active">Order List</span>
                                </nav>
                            </div>
                                <div class="vender-list-top-add-btn float-left vender-list-mew">
                                    <h1>Order List</h1>
                                </div>
                                      <div class="col-lg-12 float-left">
                                        <div class="row">
                                            <div class="order-main-box bg-white com-box-shodow rounded float-left m-b-20">
                                                
                                                <div class="order-detail">
                                                    <div class="order-pro-img">
                                                        <img src="images/pro.jpeg">
                                                    </div>
                                                    <div class="order-pro-info">
                                                        <h5>Save Khamni</h5>
                                                        <p>Lorem Ipsum is simply dummy text</p>
                                                        
                                                        
                                                            
                                                        
                                                        <div class="order-prise">
                                                            <h2>₹ 500.00</h2>
                                                        </div>
                                                    
                                                    </div>
                                                </div>
                                                <div class="order-detail">
                                                    <div class="order-pro-img">
                                                        <img src="images/pro-1.jpg">
                                                    </div>
                                                    <div class="order-pro-info">
                                                        <h5>Save Khamni</h5>
                                                        <p>Lorem Ipsum is simply</p>
                                                        
                                                        
                                                        <div class="order-prise">
                                                            <h2>₹ 500.00</h2>
                                                        </div>
                                                    </div>
                                                    
                                                </div>
                                                <div class="all-order-total">
                                                     <!-- Select with floating label -->
                                                   <div class="float-left">
                                                    <!-- Select with floating label and arrow -->
                                                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select float-left">
                                                            <input type="text" value="" class="mdl-textfield__input" id="sample49" readonly>
                                                            <input type="hidden" value="" name="sample49">
                                                            <!-- <i class="mdl-icon-toggle__label material-icons">keyboard_arrow_down</i> -->
                                                            <label for="sample49" class="mdl-textfield__label">Waiting for Approval</label>
                                                            <ul for="sample49" class="mdl-menu mdl-menu--bottom-left mdl-js-menu">
                                                                <li class="mdl-menu__item" data-val="DEU">Out for delivery</li>
                                                                <li class="mdl-menu__item" data-val="BLR">Order Delivered</li>
                                                                <li class="mdl-menu__item" data-val="RUS">Cancel</li>
                                                            </ul>
                                                        </div>
                                                        <div class="upload-satus">
                                                            <a href="index.html" >Upload Status</a>
                                                        </div>
                                                   </div>
                                                                                            
                                                    <div class="float-right">
                                                        <h1>Order Total: ₹ 1000.00</h1>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="order-main-box bg-white com-box-shodow rounded float-left m-b-20">
                                                
                                                <div class="order-detail">
                                                    <div class="order-pro-img">
                                                        <img src="images/pro-2.jpg">
                                                    </div>
                                                    <div class="order-pro-info">
                                                        <h5>Save Khamni</h5>
                                                        <p>Lorem Ipsum is simply dummy text</p>
                                                        
                                                        
                                                        <div class="order-prise">
                                                            <h2>₹ 500.00</h2>
                                                        </div>
                                                    </div>
                                                    
                                                </div>
                                                
                                                <div class="all-order-total">
                                                    <div class="float-left">
                                                    <!-- Select with floating label and arrow -->
                                                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select float-left">
                                                            <input type="text" value="" class="mdl-textfield__input" id="sample42" readonly>
                                                            <input type="hidden" value="" name="sample42">
                                                            <!-- <i class="mdl-icon-toggle__label material-icons">keyboard_arrow_down</i> -->
                                                            <label for="sample42" class="mdl-textfield__label">Waiting for Approval</label>
                                                            <ul for="sample4" class="mdl-menu mdl-menu--bottom-left mdl-js-menu">
                                                                <li class="mdl-menu__item" data-val="DEU">Out for delivery</li>
                                                                <li class="mdl-menu__item" data-val="BLR">Order Delivered</li>
                                                                <li class="mdl-menu__item" data-val="RUS">Cancel</li>
                                                            </ul>
                                                        </div>
                                                        <div class="upload-satus">
                                                            <a href="index.html" >Upload Status</a>
                                                        </div>
                                                   </div>
                                                    <div class="float-right">
                                                        <h1>Order Total: ₹ 1000.00</h1>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="order-main-box bg-white com-box-shodow rounded float-left m-b-20">
                                                
                                                <div class="order-detail">
                                                    <div class="order-pro-img">
                                                        <img src="images/pro-3.jpg">
                                                    </div>
                                                    <div class="order-pro-info">
                                                        <h5>Save Khamni</h5>
                                                        <p>Lorem Ipsum is simply dummy text</p>
                                                        
                                                        
                                                        <div class="order-prise">
                                                            <h2>₹ 500.00</h2>
                                                        </div>
                                                    </div>
                                                    
                                                </div>
                                                <div class="all-order-total">
                                                    <div class="float-left">
                                                    <!-- Select with floating label and arrow -->
                                                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select float-left">
                                                            <input type="text" value="" class="mdl-textfield__input" id="sample43" readonly>
                                                            <input type="hidden" value="" name="sample43">
                                                            <!-- <i class="mdl-icon-toggle__label material-icons">keyboard_arrow_down</i> -->
                                                            <label for="sample43" class="mdl-textfield__label">Waiting for Approval</label>
                                                            <ul for="sample43" class="mdl-menu mdl-menu--bottom-left mdl-js-menu">
                                                                <li class="mdl-menu__item" data-val="DEU">Out for delivery</li>
                                                                <li class="mdl-menu__item" data-val="BLR">Order Delivered</li>
                                                                <li class="mdl-menu__item" data-val="RUS">Cancel</li>
                                                            </ul>
                                                        </div>
                                                        <div class="upload-satus">
                                                            <a href="index.html" >Upload Status</a>
                                                        </div>
                                                   </div>
                                                    <div class="float-right">
                                                        <h1>Order Total: ₹ 1000.00</h1>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="order-main-box bg-white com-box-shodow rounded float-left m-b-20">
                                                
                                                <div class="order-detail">
                                                    <div class="order-pro-img">
                                                        <img src="images/pro-4.png">
                                                    </div>
                                                    <div class="order-pro-info">
                                                        <h5>Save Khamni</h5>
                                                        <p>Lorem Ipsum is </p>
                                                        
                                                        
                                                        <div class="order-prise">
                                                            <h2>₹ 500.00</h2>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="all-order-total">
                                                    <div class="float-left">
                                                    <!-- Select with floating label and arrow -->
                                                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select float-left">
                                                            <input type="text" value="" class="mdl-textfield__input" id="sample44" readonly>
                                                            <input type="hidden" value="" name="sample44">
                                                            <!-- <i class="mdl-icon-toggle__label material-icons">keyboard_arrow_down</i> -->
                                                            <label for="sample44" class="mdl-textfield__label">Waiting for Approval</label>
                                                            <ul for="sample44" class="mdl-menu mdl-menu--bottom-left mdl-js-menu">
                                                                <li class="mdl-menu__item" data-val="DEU">Out for delivery</li>
                                                                <li class="mdl-menu__item" data-val="BLR">Order Delivered</li>
                                                                <li class="mdl-menu__item" data-val="RUS">Cancel</li>
                                                            </ul>
                                                        </div>
                                                        <div class="upload-satus">
                                                            <a href="index.html" >Upload Status</a>
                                                        </div>
                                                   </div>
                                                    <div class="float-right">
                                                        <h1>Order Total: ₹ 1000.00</h1>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                      </div>
                                  
                                  
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
 @endsection

     
      <!-- Button trigger modal -->

<!-- Modal -->
    <!-- <div class="modal fade bd-example-modal-lg" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLongTitle">Choose Delivery Boy</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="search-bar col-sm-12 col-md-12 col-xs-12 float-left p-0">
                <form action="#">
                  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                    <input class="mdl-textfield__input" type="text" id="sample3">
                    <label class="mdl-textfield__label" for="sample3">Search...</label>
                  </div>
                </form>
            </div>
            <div class="delivery-boy-listing">
                <div class="first-heading">
                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 float-left">
                        <p>Image</p>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 float-left">
                        <p>Name</p>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 float-left">
                        <p>Mobile NO</p>
                    </div>
                </div>
                <div class="list-body">
                    <div class="list-body-row">
                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 float-left">
                            <div class="profile-img">
                                <img src="images/profile-img.png">
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 float-left">
                            <div class="profile-name">
                                <h6>Mohil Sojitra</h6>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 float-left">
                            <div class="profile-name">
                                <h6>+91 9865986598</h6>
                                <label class="mdl-radio mdl-js-radio mdl-js-ripple-effect" for="option-1">
                                    <input type="radio" id="option-1" class="mdl-radio__button" name="options" value="1" checked>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="list-body-row">
                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 float-left">
                            <div class="profile-img">
                                <img src="images/profile-img.png">
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 float-left">
                            <div class="profile-name">
                                <h6>Mohil Sojitra</h6>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 float-left">
                            <div class="profile-name">
                                <h6>+91 9865986598</h6>
                                <label class="mdl-radio mdl-js-radio mdl-js-ripple-effect" for="option-2">
                                    <input type="radio" id="option-2" class="mdl-radio__button" name="options" value="1" checked>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="list-body-row">
                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 float-left">
                            <div class="profile-img">
                                <img src="images/profile-img.png">
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 float-left">
                            <div class="profile-name">
                                <h6>Mohil Sojitra</h6>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 float-left">
                            <div class="profile-name">
                                <h6>+91 9865986598</h6>
                                <label class="mdl-radio mdl-js-radio mdl-js-ripple-effect" for="option-3">
                                    <input type="radio" id="option-3" class="mdl-radio__button" name="options" value="1" checked>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="list-body-row">
                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 float-left">
                            <div class="profile-img">
                                <img src="images/profile-img.png">
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 float-left">
                            <div class="profile-name">
                                <h6>Mohil Sojitra</h6>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 float-left">
                            <div class="profile-name">
                                <h6>+91 9865986598</h6>
                                <label class="mdl-radio mdl-js-radio mdl-js-ripple-effect" for="option-4">
                                    <input type="radio" id="option-4" class="mdl-radio__button" name="options" value="1" checked>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="list-body-row">
                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 float-left">
                            <div class="profile-img">
                                <img src="images/profile-img.png">
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 float-left">
                            <div class="profile-name">
                                <h6>Mohil Sojitra</h6>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 float-left">
                            <div class="profile-name">
                                <h6>+91 9865986598</h6>
                                <label class="mdl-radio mdl-js-radio mdl-js-ripple-effect" for="option-5">
                                    <input type="radio" id="option-5" class="mdl-radio__button" name="options" value="1" checked>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="list-body-row">
                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 float-left">
                            <div class="profile-img">
                                <img src="images/profile-img.png">
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 float-left">
                            <div class="profile-name">
                                <h6>Mohil Sojitra</h6>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 float-left">
                            <div class="profile-name">
                                <h6>+91 9865986598</h6>
                                <label class="mdl-radio mdl-js-radio mdl-js-ripple-effect" for="option-6">
                                    <input type="radio" id="option-6" class="mdl-radio__button" name="options" value="1" checked>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
          </div>
          <div class="modal-footer">
            <a href="index.html">
                <button type="button" class="model-btn">Submit</button>
            </a>
          </div>
        </div>
      </div>
    </div> -->

      
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="js/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <!-- <script src="js/popper.js"></script> -->
    <script src="js/bootstrap.min.js"></script>
    <script src="js/material.min.js"></script>
    <script src="js/getmdl-select.js"></script>
    
    
    <script defer src="js/solid.js"></script>
    <script defer src="js/fontawesome.js"></script>
    <script src="js/canvasjs.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#sidebarCollapse').on('click', function () {
                $('#sidebar').toggleClass('active');
                $('.main-container').toggleClass('open-menu');
            });
        });
        $(document).ready(function(){

            var quantitiy=0;
               $('.quantity-right-plus').click(function(e){
                    
                    // Stop acting like a button
                    e.preventDefault();
                    // Get the field name
                    var quantity = parseInt($(this).closest('.input-group').find('#quantity').val());
                    
                    // If is not undefined
                        
                        $(this).closest('.input-group').find('#quantity').val(quantity + 1);

                      
                        // Increment
                    
                });

                 $('.quantity-left-minus').click(function(e){
                    // Stop acting like a button
                    e.preventDefault();
                    // Get the field name
                    var quantity = parseInt($(this).closest('.input-group').find('#quantity').val());
                    
                    // If is not undefined
                  
                        // Increment
                        if(quantity>0){
                        $(this).closest('.input-group').find('#quantity').val(quantity - 1);
                        }
                });
                
            });
    </script>
   
  </body>
</html>
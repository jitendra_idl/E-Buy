<!DOCTYPE html>
@extends('layouts.app')
@section('content')  
            
            <div class="main-order-section">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="bread-crum">
                                <nav class="breadcrumb">
                                  <a class="breadcrumb-item" href="index.html">Home</a>
                                  <a class="breadcrumb-item" href="catalogue.html">Catalouges</a>
                                  <span class="breadcrumb-item active">Product Listing</span>
                                </nav>
                            </div>
                            <div class="catalouge-list">  
                                <div class="main-product-list">
                                    <div class="product-list-box">
                                        <div class="vender-list-top-add-btn text-left">
                                            <h1>Product Listing</h1>
                                            <div class="placeorder-btn">
                                                <div class="order-btn">
                                                    <a href="new-order.html">Placeorder</a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="prodcut-category-lilst-box row">


                                        @foreach($products as $product)
                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                                <div class="order-detail com-box-shodow rounded bg-white p-0 m-b-30">
                                                    <div class="order-pro-img midd-btm-same">
                                                        <img src="{{ asset('storage/'.$product->media->path) }}">
                                                    </div>
                                                    <div class="pro-detail-list midd-btm-same">
                                                        <div class="order-pro-info">
                                                            <a href="#Editcatogory" data-toggle="modal"><h5>{{ $product->name }}</h5></a>
                                                            <p>{{ $product->description }}</p>
                                                            <p>Product Code: <strong>{{ $product->product_code }}</strong></p>
                                                            <p>Stock Qty: <strong>{{ $product->quantities }}</strong></p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach


                                        
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
     
     
        <div class="modal fade bd-example-modal-lg product-detail-popup-main" id="Editcatogory" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Edit Category</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="invoice-info add-cat">
                        <div class="edit-img">
                            <div class="edit-image">
                                <img src="/images/pro.jpeg">
                            </div>
                        </div>
                        <div class="product-detail-popup">
                            <div class="top-tab-penal">
                                <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#w-app" role="tab" aria-controls="pills-home" aria-selected="true">Product Details</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#out-dill" role="tab" aria-controls="pills-profile" aria-selected="false">Pricing</a>
                                </li>
                                </ul>
                                <div class="tab-content" id="pills-tabContent">
                                    <div class="tab-pane fade show active" id="w-app" role="tabpanel" aria-labelledby="pills-home-tab">
                                        <div class="form-content">     
                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 float-left">
                                                <div class="frm-group">
                                                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                                    <input class="mdl-textfield__input" type="text" id="sample3">
                                                    <label class="mdl-textfield__label" for="sample3">Product Name</label>
                                                    </div>
                                                </div>
                                                <div class="frm-group">
                                                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                                    <input class="mdl-textfield__input" type="text" id="sample3">
                                                    <label class="mdl-textfield__label" for="sample3">Product Discount</label>
                                                    </div>
                                                </div>
                                                <div class="frm-group">
                                                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                                    <input class="mdl-textfield__input" type="text" id="sample3">
                                                    <label class="mdl-textfield__label" for="sample3">Brand</label>
                                                    </div>
                                                </div>
                                                <div class="frm-group">
                                                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                                    <input class="mdl-textfield__input" type="text" id="sample3">
                                                    <label class="mdl-textfield__label" for="sample3">Categories</label>
                                                    </div>
                                                </div>
                                                
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 float-left">
                                                <div class="frm-group">
                                                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                                    <input class="mdl-textfield__input" type="text" id="sample3">
                                                    <label class="mdl-textfield__label" for="sample3">Quantities</label>
                                                    </div>
                                                </div>
                                                
                                                <div class="frm-group">
                                                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                                    <input class="mdl-textfield__input" type="text" id="sample3">
                                                    <label class="mdl-textfield__label" for="sample3">Minimum Quantities</label>
                                                    </div>
                                                </div>
                                                <div class="frm-group">
                                                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select">
                                                        <input type="text" value="" class="mdl-textfield__input" id="weight-main" readonly>
                                                        <input type="hidden" value="" name="weight-main">
                                                        
                                                        <label for="weight-main" class="mdl-textfield__label">Add Package Weight</label>
                                                        <ul for="weight-main" class="mdl-menu mdl-menu--bottom-left mdl-js-menu">
                                                            <li class="mdl-menu__item" data-val="DEU">gr</li>
                                                            <li class="mdl-menu__item" data-val="BLR">kg</li>
                                                            <li class="mdl-menu__item" data-val="RUS">ltr</li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class="frm-group">
                                                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                                    <input class="mdl-textfield__input" type="text" id="sample3">
                                                    <label class="mdl-textfield__label" for="sample3">Unit</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="foo-btn">
                                            <div class="foo-btn-content">
                                                <a href="index.html">
                                                    <button type="button" class="model-btn">View as Reader</button>
                                                </a>
                                                <a href="index.html" class="m-l-10">
                                                    <button type="button" class="model-btn">View as Editable</button>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="out-dill" role="tabpanel" aria-labelledby="pills-profile-tab">
                                        <div class="form-content">     
                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 float-left">
                                                <div class="frm-group">
                                                    <h6>Retail price</h6>
                                                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                                    <input class="mdl-textfield__input" type="text" id="sample3">
                                                    <label class="mdl-textfield__label" for="sample3">Price (Tax excl.)</label>
                                                    </div>
                                                </div>
                                                <div class="frm-group">
                                                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                                    <input class="mdl-textfield__input" type="text" id="sample3">
                                                    <label class="mdl-textfield__label" for="sample3">Price (Tax incl.)</label>
                                                    </div>
                                                </div>
                                                <div class="frm-group">
                                                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                                    <input class="mdl-textfield__input" type="text" id="sample3">
                                                    <label class="mdl-textfield__label" for="sample3">Price per unit (Tax excl.)</label>
                                                    </div>
                                                </div>
                                                <div class="frm-group">
                                                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                                    <input class="mdl-textfield__input" type="text" id="sample3">
                                                    <label class="mdl-textfield__label" for="sample3">Per Kilo,per liter</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 float-left">
                                                <div class="frm-group">
                                                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select">
                                                        <input type="text" value="" class="mdl-textfield__input" id="sample10" readonly>
                                                        <input type="hidden" value="" name="sample10">
                                                        <label for="sample10" class="mdl-textfield__label">Tax Rule </label>
                                                        <ul for="sample10" class="mdl-menu mdl-menu--bottom-left mdl-js-menu">
                                                            <li class="mdl-menu__item" data-val="1liter">US-AL Rate 4%</li>
                                                            <li class="mdl-menu__item" data-val="2liter">US-AL Rate 4%</li>
                                                            <li class="mdl-menu__item" data-val="3liter">US-AL Rate 4%</li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class="frm-group">
                                                    <h6>Retail price</h6>
                                                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                                    <input class="mdl-textfield__input" type="text" id="sample3">
                                                    <label class="mdl-textfield__label" for="sample3">Email</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="foo-btn">
                                            <div class="foo-btn-content">
                                                <a href="index.html">
                                                    <button type="button" class="model-btn">Save</button>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- <div class="modal-footer">
                    <a href="index.html">
                        <button type="button" class="model-btn">Submit</button>
                    </a>
                </div> -->
                </div>
            </div>
        </div>

@endsection
        
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="/js/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="/js/popper.js"></script>
    <script src="/js/bootstrap.min.js"></script>
    <script src="/js/material.min.js"></script>
    <script src="/js/getmdl-select.js"></script>
    <script defer src="/js/solid.js"></script>
    <script defer src="/js/fontawesome.js"></script>
    <script src="/js/canvasjs.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#sidebarCollapse').on('click', function () {
                $('#sidebar').toggleClass('active');
                $('.main-container').toggleClass('open-menu');
            });
        });
    </script>
   <script type="text/javascript">
window.onload = function () {

var chart = new CanvasJS.Chart("chartContainer", {
    animationEnabled: true,
    title:{
        text: "Total Sales"
    },
    axisY: {
        title: "    "
    },
    legend: {
        cursor:"pointer",
        itemclick : toggleDataSeries
    },
    toolTip: {
        shared: true,
        content: toolTipFormatter
    },
    data: [{
        type: "bar",
        showInLegend: true,
        name: "New Order receives",
        color: "gold",
        dataPoints: [
            { y: 243, label: "Monday" },
            { y: 236, label: "Tuesday" },
            { y: 243, label: "Wednesday" },
            { y: 273, label: "Thursday" },
            { y: 269, label: "Friday" },
            { y: 196, label: "Saturday" },
            { y: 1118, label: "Sunday" }
        ]
    },
    {
        type: "bar",
        showInLegend: true,
        name: "Item to be Delivered",
        color: "silver",
        dataPoints: [
            { y: 212, label: "Monday" },
            { y: 186, label: "Tuesday" },
            { y: 272, label: "Wednesday" },
            { y: 299, label: "Thursday" },
            { y: 270, label: "Friday" },
            { y: 165, label: "Saturday" },
            { y: 896, label: "Sunday" }
        ]
    },
    {
        type: "bar",
        showInLegend: true,
        name: "Item to be Packed",
        color: "#A57164",
        dataPoints: [
            { y: 236, label: "Monday" },
            { y: 172, label: "Tuesday" },
            { y: 309, label: "Wednesday" },
            { y: 302, label: "Thursday" },
            { y: 285, label: "Friday" },
            { y: 188, label: "Saturday" },
            { y: 788, label: "Sunday" }
        ]
    }]
});
chart.render();

function toolTipFormatter(e) {
    var str = "";
    var total = 0 ;
    var str3;
    var str2 ;
    for (var i = 0; i < e.entries.length; i++){
        var str1 = "<span style= 'color:"+e.entries[i].dataSeries.color + "'>" + e.entries[i].dataSeries.name + "</span>: <strong>"+  e.entries[i].dataPoint.y + "</strong> <br/>" ;
        total = e.entries[i].dataPoint.y + total;
        str = str.concat(str1);
    }
    str2 = "<strong>" + e.entries[0].dataPoint.label + "</strong> <br/>";
    str3 = "<span style = 'color:Tomato'>Total: </span><strong>" + total + "</strong><br/>";
    return (str2.concat(str)).concat(str3);
}

function toggleDataSeries(e) {
    if (typeof (e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
        e.dataSeries.visible = false;
    }
    else {
        e.dataSeries.visible = true;
    }
    chart.render();
}

}
</script>
<script>
        $(document).ready(function() {
            $('#example').DataTable();
        } );

        equalheight = function(container){

        var currentTallest = 0,
             currentRowStart = 0,
             rowDivs = new Array(),
             $el,
             topPosition = 0;
         $(container).each(function() {

           $el = $(this);
           $($el).height('auto')
           topPostion = $el.position().top;

           if (currentRowStart != topPostion) {
             for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
               rowDivs[currentDiv].height(currentTallest);
             }
             rowDivs.length = 0; // empty the array
             currentRowStart = topPostion;
             currentTallest = $el.height();
             rowDivs.push($el);
           } else {
             rowDivs.push($el);
             currentTallest = (currentTallest < $el.height()) ? ($el.height()) : (currentTallest);
          }
           for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
             rowDivs[currentDiv].height(currentTallest);
           }
         });
        }

        $(window).load(function() {
          equalheight('.midd-btm-same');  
        });


        $(window).resize(function(){
          equalheight('.midd-btm-same');
        });
    </script>


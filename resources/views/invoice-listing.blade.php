@extends('layouts.app')

@section('content')   
            <div class="main-order-section">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="bread-crum">
                                <nav class="breadcrumb">
                                  <a class="breadcrumb-item" href="index.html">Home</a>
                                  <span class="breadcrumb-item active">Invoice List</span>
                                </nav>
                            </div>
                            <div class="catalouge-list">    
                                <div class="page-headeing">
                                    <h5>Invoice List</h5>
                                </div>
                                <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                                  <li class="nav-item">
                                    <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#w-app" role="tab" aria-controls="pills-home" aria-selected="true">Unpaid</a>
                                  </li>
                                  <li class="nav-item">
                                    <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#out-dill" role="tab" aria-controls="pills-profile" aria-selected="false">All</a>
                                  </li>
                                  <li class="nav-item">
                                    <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#order-dille" role="tab" aria-controls="pills-profile" aria-selected="false">Dilvery Slip</a>
                                  </li>
                                </ul>
                                <div class="placeorder-btn">
                                    <div class="order-btn">
                                        <a href="add-invoice.html">Add Invoice</a>
                                    </div>
                                </div>
                                <div class="tab-content" id="pills-tabContent">
                                  <div class="tab-pane fade show active" id="w-app" role="tabpanel" aria-labelledby="pills-home-tab">
                                     <table id="example" class="table text-left" style="width:100%">
                                        <thead>
                                            <tr>
                                                <th>Order Id</th>
                                                <th>Name</th>
                                                <th>Status</th>
                                                <th>Price</th>
                                                <th>Date</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                           <tr>
                                                <td class="order-td">#001232</td>
                                                <td class="name-td">Manshukhbhai Shodhi</td>
                                                <td class="Status-td text-success">Paid</td>
                                                <td class="price-td">₹ 500</td>
                                                <td class="date-td">13 jul 2018 
                                                    <a href="add-invoice.html"><i class="fas fa-edit"></i></a>
                                                </td>
                                           </tr>
                                           <tr>
                                                <td class="order-td">#001232</td>
                                                <td class="name-td">Manshukhbhai Shodhi</td>
                                                <td class="Status-td text-success">Paid</td>
                                                <td class="price-td">₹ 500</td>
                                                <td class="date-td">13 jul 2018 
                                                    <a href="add-invoice.html"><i class="fas fa-edit"></i></a>
                                                </td>
                                           </tr>
                                           <tr>
                                                <td class="order-td">#001232</td>
                                                <td class="name-td">Manshukhbhai Shodhi</td>
                                                <td class="Status-td text-success">Paid</td>
                                                <td class="price-td">₹ 500</td>
                                                <td class="date-td">13 jul 2018 
                                                    <a href="add-invoice.html"><i class="fas fa-edit"></i></a>
                                                </td>
                                           </tr>
                                           <tr>
                                                <td class="order-td">#001232</td>
                                                <td class="name-td">Manshukhbhai Shodhi</td>
                                                <td class="Status-td text-success">Paid</td>
                                                <td class="price-td">₹ 500</td>
                                                <td class="date-td">13 jul 2018 
                                                    <a href="add-invoice.html"><i class="fas fa-edit"></i></a>
                                                </td>
                                           </tr>
                                        </tbody>
                                    </table>
                                  </div>
                                  <div class="tab-pane fade" id="out-dill" role="tabpanel" aria-labelledby="pills-profile-tab">
                                    <table id="example" class="table text-left" style="width:100%">
                                        <thead>
                                            <tr>
                                                <th>Order Id</th>
                                                <th>Name</th>
                                                <th>Status</th>
                                                <th>Price</th>
                                                <th>Date</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                           <tr>
                                                <td class="order-td">#001232</td>
                                                <td class="name-td">Manshukhbhai Shodhi</td>
                                                <td class="Status-td text-success">Paid</td>
                                                <td class="price-td">₹ 500</td>
                                                <td class="date-td">13 jul 2018 
                                                    <a href="add-invoice.html"><i class="fas fa-edit"></i></a>
                                                </td>
                                           </tr>
                                           <tr>
                                                <td class="order-td">#001232</td>
                                                <td class="name-td">Manshukhbhai Shodhi</td>
                                                <td class="Status-td text-danger">Unpaid</td>
                                                <td class="price-td">₹ 500</td>
                                                <td class="date-td">13 jul 2018 
                                                    <a href="add-invoice.html"><i class="fas fa-edit"></i></a>
                                                </td>
                                           </tr>
                                           <tr>
                                                <td class="order-td">#001232</td>
                                                <td class="name-td">Manshukhbhai Shodhi</td>
                                                <td class="Status-td text-success">Paid</td>
                                                <td class="price-td">₹ 500</td>
                                                <td class="date-td">13 jul 2018 
                                                    <a href="add-invoice.html"><i class="fas fa-edit"></i></a>
                                                </td>
                                           </tr>
                                           <tr>
                                                <td class="order-td">#001232</td>
                                                <td class="name-td">Manshukhbhai Shodhi</td>
                                                <td class="Status-td text-success">Paid</td>
                                                <td class="price-td">₹ 500</td>
                                                <td class="date-td">13 jul 2018 
                                                    <a href="add-invoice.html"><i class="fas fa-edit"></i></a>
                                                </td>
                                           </tr>
                                        </tbody>
                                    </table>
                                  </div>
                                  <div class="tab-pane fade" id="order-dille" role="tabpanel" aria-labelledby="pills-profile-tab">
                                       <table id="example" class="table text-left" style="width:100%">
                                        <thead>
                                            <tr>
                                                <th>Item Code</th>
                                                <th>Image</th>
                                                <th>Item No</th>
                                                <th>Address</th>
                                                <th>Status</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                           <tr>
                                                <td class="order-td">#001232</td>
                                                <td class="order-td"><img src="images/pro-6.jpg"></td>
                                                <td class="order-td">#23</td>
                                                <td class="name-td">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</td>
                                                <td class="Status-td text-warning">Pending 
                                                    <a href="add-invoice.html"><i class="fas fa-edit"></i></a>
                                                </td>
                                           </tr>
                                           <tr>
                                                <td class="order-td">#001232</td>
                                                <td class="order-td"><img src="images/pro-2.jpg"></td>
                                                <td class="order-td">#23</td>
                                                <td class="name-td">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</td>
                                                <td class="Status-td text-warning">Pending 
                                                    <a href="add-invoice.html"><i class="fas fa-edit"></i></a>
                                                </td>
                                           </tr>
                                           <tr>
                                                <td class="order-td">#001232</td>
                                                <td class="order-td"><img src="images/pro-3.jpg"></td>
                                                <td class="order-td">#23</td>
                                                <td class="name-td">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</td>
                                                <td class="Status-td text-warning">Pending 
                                                    <a href="add-invoice.html"><i class="fas fa-edit"></i></a>
                                                </td>
                                           </tr>
                                           <tr>
                                                <td class="order-td">#001232</td>
                                                <td class="order-td"><img src="images/pro-5.jpg"></td>
                                                <td class="order-td">#23</td>
                                                <td class="name-td">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</td>
                                                <td class="Status-td text-warning">Pending 
                                                    <a href="add-invoice.html"><i class="fas fa-edit"></i></a>
                                                </td>
                                           </tr>
                                        </tbody>
                                    </table>   
                                  </div>
                                  <div class="tab-pane fade" id="cancle" role="tabpanel" aria-labelledby="pills-profile-tab">
                                      
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
@endsection

      
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="js/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <!-- <script src="js/popper.js"></script> -->
    <script src="js/bootstrap.min.js"></script>
    <script src="js/material.min.js"></script>
    <script src="js/getmdl-select.js"></script>
    
    
    <script defer src="js/solid.js"></script>
    <script defer src="js/fontawesome.js"></script>
    <script src="js/canvasjs.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#sidebarCollapse').on('click', function () {
                $('#sidebar').toggleClass('active');
                $('.main-container').toggleClass('open-menu');
            });
        });
    </script>
    <script>
        $(document).ready(function() {
            $('#example').DataTable();
        } );
    </script>
  </body>
</html>

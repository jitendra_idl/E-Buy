@extends('layouts.app')

@section('content')  
            <div class="main-order-section">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="bread-crum">
                                <nav class="breadcrumb">
                                  <a class="breadcrumb-item" href="index.html">Home</a>
                                  <a class="breadcrumb-item" href="vender-list.html">My Vendors</a>
                                  <span class="breadcrumb-item active">Vender Profile</span>
                                </nav>
                            </div>
                            <div class="vender-list-top-add-btn">
                                <h1>Vender Profile</h1>
                            </div>
                                <div class="main-profile-page bg-white com-box-shodow rounded float-left">
                                    <div class="profile-left-side vender-profile-img">
                                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 float-left p-l-0">
                                            <div class="profile-div">
                                                <div class="progile-image">
                                                    <div class="profile-img-img">
                                                        <img src="images/profile-img.png">
                                                    </div>
                                                    <div class="vender-name">
                                                        <h2>Shamjibhai Lodha</h2>
                                                    </div>
                                                    <div class="order-info-vender">
                                                        <div class="orde-detil-box">
                                                            <h5>10</h5>
                                                            <p>Product Sale</p>
                                                        </div>
                                                        <div class="orde-detil-box bg-warning">
                                                            <h5>10</h5>
                                                            <p>Product Sale</p>
                                                        </div>
                                                        <div class="orde-detil-box bg-danger">
                                                            <h5>10</h5>
                                                            <p>Product Sale</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 float-left">
                                            <div class="vender-detail-info">
                                                <div class="top-tab-penal">
                                                    <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                                                      <li class="nav-item">
                                                        <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#w-app" role="tab" aria-controls="pills-home" aria-selected="true">About</a>
                                                      </li>
                                                      <li class="nav-item">
                                                        <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#out-dill" role="tab" aria-controls="pills-profile" aria-selected="false">Organization Detail</a>
                                                      </li>
                                                    </ul>
                                                    <div class="tab-content" id="pills-tabContent">
                                                        <div class="tab-pane fade show active" id="w-app" role="tabpanel" aria-labelledby="pills-home-tab">
                                                            <div class="form-content">     
                                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 offset-lg-2 float-left">
                                                                    <div class="frm-group">
                                                                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                                                           <input class="mdl-textfield__input" type="text" id="sample3">
                                                                           <label class="mdl-textfield__label" for="sample3">Name</label>
                                                                        </div>
                                                                    </div>
                                                                    <div class="frm-group">
                                                                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                                                           <input class="mdl-textfield__input" type="text" id="sample3">
                                                                           <label class="mdl-textfield__label" for="sample3">Address 1</label>
                                                                        </div>
                                                                    </div>
                                                                    <div class="frm-group">
                                                                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                                                           <input class="mdl-textfield__input" type="text" id="sample3">
                                                                           <label class="mdl-textfield__label" for="sample3">Mobile NO</label>
                                                                        </div>
                                                                    </div>

                                                                    <div class="frm-group">
                                                                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                                                           <input class="mdl-textfield__input" type="text" id="sample3">
                                                                           <label class="mdl-textfield__label" for="sample3">Email</label>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 float-left">
                                                                    <div class="frm-group">
                                                                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                                                           <input class="mdl-textfield__input" type="text" id="sample3">
                                                                           <label class="mdl-textfield__label" for="sample3">RT NO</label>
                                                                        </div>
                                                                    </div>
                                                                    <div class="frm-group">
                                                                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                                                           <input class="mdl-textfield__input" type="text" id="sample3">
                                                                           <label class="mdl-textfield__label" for="sample3">City</label>
                                                                        </div>
                                                                    </div>
                                                                    <div class="frm-group">
                                                                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                                                           <input class="mdl-textfield__input" type="text" id="sample3">
                                                                           <label class="mdl-textfield__label" for="sample3">Country</label>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="tab-pane fade" id="out-dill" role="tabpanel" aria-labelledby="pills-profile-tab">
                                                             <div class="form-content">     
                                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 offset-lg-2 float-left">
                                                                    <div class="frm-group">
                                                                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                                                           <input class="mdl-textfield__input" type="text" id="sample3">
                                                                           <label class="mdl-textfield__label" for="sample3">Name</label>
                                                                        </div>
                                                                    </div>
                                                                    <div class="frm-group">
                                                                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                                                           <input class="mdl-textfield__input" type="text" id="sample3">
                                                                           <label class="mdl-textfield__label" for="sample3">Mobile NO</label>
                                                                        </div>
                                                                    </div>
                                                                    <div class="frm-group">
                                                                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                                                           <input class="mdl-textfield__input" type="text" id="sample3">
                                                                           <label class="mdl-textfield__label" for="sample3">Address 1</label>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 float-left">
                                                                    <div class="frm-group">
                                                                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                                                           <input class="mdl-textfield__input" type="text" id="sample3">
                                                                           <label class="mdl-textfield__label" for="sample3">Site-url</label>
                                                                        </div>
                                                                    </div>
                                                                    <div class="frm-group">
                                                                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                                                           <input class="mdl-textfield__input" type="text" id="sample3">
                                                                           <label class="mdl-textfield__label" for="sample3">Email</label>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                
                                                                    
                                                                    
                                                                
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="catalouge-list vender-order">    
                                <table id="example" class="table text-left" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>Item No</th>
                                            <th>Image</th>
                                            <th>Item Name</th>
                                            <th>Status</th>
                                            <th>Date</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                       <tr>
                                            <td class="order-td">#001232</td>
                                            <td class="order-td"><img src="images/pro-6.jpg"></td>
                                            <td class="name-td">Lorem Ipsum is simply</td>
                                            <td class="Status-td text-success">Paid</td>
                                            <td class="date-td">10 jul 2018</td>
                                       </tr>
                                       <tr>
                                            <td class="order-td">#001232</td>
                                            <td class="order-td"><img src="images/pro-6.jpg"></td>
                                            <td class="name-td">Lorem Ipsum is simply dummy</td>
                                            <td class="Status-td text-warning">Pending</td>
                                            <td class="date-td">10 jul 2018</td>
                                       </tr>
                                       <tr>
                                            <td class="order-td">#001232</td>
                                            <td class="order-td"><img src="images/pro-6.jpg"></td>
                                            <td class="name-td">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</td>
                                            <td class="Status-td text-warning">Pending</td>
                                            <td class="date-td">10 jul 2018</td>
                                       </tr><tr>
                                            <td class="order-td">#001232</td>
                                            <td class="order-td"><img src="images/pro-6.jpg"></td>
                                            <td class="name-td">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</td>
                                            <td class="Status-td text-warning">Pending</td>
                                            <td class="date-td">10 jul 2018</td>
                                       </tr>
                                       <tr>
                                            <td class="order-td">#001232</td>
                                            <td class="order-td"><img src="images/pro-6.jpg"></td>
                                            <td class="name-td">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</td>
                                            <td class="Status-td text-warning">Pending</td>
                                            <td class="date-td">10 jul 2018</td>
                                       </tr>

                                       
                                       
                                       
                                    </tbody>
                                </table>   
                            </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
@endsection

      
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="js/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <!-- <script src="js/popper.js"></script> -->
    <script src="js/bootstrap.min.js"></script>
    <script src="js/material.min.js"></script>
    <script src="js/getmdl-select.js"></script>
    <script defer src="js/solid.js"></script>
    <script defer src="js/fontawesome.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#sidebarCollapse').on('click', function () {
                $('#sidebar').toggleClass('active');
                $('.main-container').toggleClass('open-menu');
            });
        });
    </script>
    <script>
        $(document).ready(function() {
            $('#example').DataTable();
        } );
    </script>
  </body>
</html>
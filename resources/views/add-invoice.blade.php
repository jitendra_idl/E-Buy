@extends('layouts.app')

@section('content')  
            
            
            <div class="main-order-section">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="bread-crum">
                                <nav class="breadcrumb">
                                  <a class="breadcrumb-item" href="index.html">Home</a>
                                  <a class="breadcrumb-item" href="invoice-listing.html">Invoice List</a>
                                  <span class="breadcrumb-item active">Add Invoice</span>
                                </nav>
                            </div>
                            <div class="vender-list">                        
                                <div class="vender-list-top-add-btn">
                                    <h1>Invoice</h1>
                                </div>
                                <div class="invoice-detail-main bg-white com-box-shodow rounded float-left p-2">
                                    <div class="top-heading-bar">
                                        <div class="col-lg-6 from-logo">
                                            <img src="images/logo-5.png">
                                            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 p-0">
                                                <p>Prahalad Plot Main Road,<br> Near RAJSHREE CINEMA,<br>Bangalore 530068<br><strong>+91 9999555525</strong></p>
                                            </div>
                                            <div class="invoice-info">
                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 float-left pl-0">
                                                    <div class="frm-group">
                                                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                                            <input class="mdl-textfield__input" type="text" id="sample3">
                                                            <label class="mdl-textfield__label" for="sample3">Invoice No</label>
                                                          </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 float-left">
                                                    <div class="frm-group">
                                                          <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                                            <input class="mdl-textfield__input" type="text" id="sample3">
                                                            <label class="mdl-textfield__label" for="sample3">Invoice Date</label>
                                                          </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 float-left">
                                                    <div class="frm-group">
                                                          <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                                            <input class="mdl-textfield__input" type="text" id="sample3">
                                                            <label class="mdl-textfield__label" for="sample3">Payment Due</label>
                                                          </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 to-logo p-0">
                                            <div class="to-detail">
                                                <h6>TO,</h6>
                                                <div class="to-img">
                                                    <h5>Manshukhai Shodhi</h5>
                                                </div>
                                                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 float-right p-0">
                                                    <div class="to-to-detail">
                                                        <p>Prahalad Plot Main Road,<br> Near RAJSHREE CINEMA,<br>Mumbai 230532<br><strong>+91 9999555525</strong>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="invoice-detail">
                                        <div class="item-name-row">
                                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 float-left">
                                                    <p>Name</p>
                                                </div>
                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6 float-left">
                                                    <p>Description</p>
                                                </div>
                                                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6 float-left">
                                                    <p>Quantity</p>
                                                </div>
                                                <div class="col-lg-1 col-md-1 col-sm-1 col-xs-6 float-left">
                                                    <p>Price</p>
                                                </div>
                                                <div class="col-lg-1 col-md-1 col-sm-1 col-xs-6 float-left">
                                                    <p>Tax</p>
                                                </div>
                                        </div>
                                        <div class="add-itemt-row">
                                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 float-left">       
                                                <div class="frm-group">
                                                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                                        <input class="mdl-textfield__input" type="text" id="sample3">
                                                        <!-- <label class="mdl-textfield__label" for="sample3">Name</label> -->
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6 float-left">       
                                                <div class="frm-group">
                                                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                                        <input class="mdl-textfield__input" type="text" id="sample3">
                                                        <!-- <label class="mdl-textfield__label" for="sample3">Discription</label> -->
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6 float-left">       
                                                <div class="frm-group">
                                                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                                        <input class="mdl-textfield__input" type="text" id="sample3">
                                                        <!-- <label class="mdl-textfield__label" for="sample3">Quantity</label> -->
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-6 float-left">       
                                                <div class="frm-group">
                                                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                                        <input class="mdl-textfield__input" type="text" id="sample3">
                                                        <!-- <label class="mdl-textfield__label" for="sample3">Price</label> -->
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-6 float-left ">       
                                                <div class="frm-group">
                                                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                                        <input class="mdl-textfield__input" type="text" id="sample3">
                                                        <!-- <label class="mdl-textfield__label" for="sample3">Tax</label> -->
                                                    </div>
                                                    
                                                </div>
                                            </div>
                                            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-6 float-left">
                                                <div class="add-btn">
                                                    <a href="#"><i class="fas fa-plus"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="add-itemt-row">
                                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 float-left">       
                                                <div class="frm-group">
                                                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                                        <input class="mdl-textfield__input" type="text" id="sample3">
                                                        <!-- <label class="mdl-textfield__label" for="sample3">Name</label> -->
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6 float-left">       
                                                <div class="frm-group">
                                                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                                        <input class="mdl-textfield__input" type="text" id="sample3">
                                                        <!-- <label class="mdl-textfield__label" for="sample3">Discription</label> -->
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6 float-left">       
                                                <div class="frm-group">
                                                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                                        <input class="mdl-textfield__input" type="text" id="sample3">
                                                        <!-- <label class="mdl-textfield__label" for="sample3">Quantity</label> -->
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-6 float-left">       
                                                <div class="frm-group">
                                                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                                        <input class="mdl-textfield__input" type="text" id="sample3">
                                                        <!-- <label class="mdl-textfield__label" for="sample3">Price</label> -->
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-6 float-left ">       
                                                <div class="frm-group">
                                                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                                        <input class="mdl-textfield__input" type="text" id="sample3">
                                                        <!-- <label class="mdl-textfield__label" for="sample3">Tax</label> -->
                                                    </div>
                                                    
                                                </div>
                                            </div>
                                            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-6 float-left">
                                                <div class="add-btn">
                                                    <a href="#"><i class="fas fa-minus"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="add-itemt-row">
                                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 float-left">       
                                                <div class="frm-group">
                                                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                                        <input class="mdl-textfield__input" type="text" id="sample3">
                                                        <!-- <label class="mdl-textfield__label" for="sample3">Name</label> -->
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6 float-left">       
                                                <div class="frm-group">
                                                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                                        <input class="mdl-textfield__input" type="text" id="sample3">
                                                        <!-- <label class="mdl-textfield__label" for="sample3">Discription</label> -->
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6 float-left">       
                                                <div class="frm-group">
                                                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                                        <input class="mdl-textfield__input" type="text" id="sample3">
                                                        <!-- <label class="mdl-textfield__label" for="sample3">Quantity</label> -->
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-6 float-left">       
                                                <div class="frm-group">
                                                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                                        <input class="mdl-textfield__input" type="text" id="sample3">
                                                        <!-- <label class="mdl-textfield__label" for="sample3">Price</label> -->
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-6 float-left ">       
                                                <div class="frm-group">
                                                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                                        <input class="mdl-textfield__input" type="text" id="sample3">
                                                        <!-- <label class="mdl-textfield__label" for="sample3">Tax</label> -->
                                                    </div>
                                                    
                                                </div>
                                            </div>
                                            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-6 float-left">
                                                <div class="add-btn">
                                                    <a href="#"><i class="fas fa-minus"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="calculation">
                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 float-right">
                                            <div class="right-content col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                 <div class="frm-group invoice-add">
                                                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                                        <input class="mdl-textfield__input" type="text" id="sample3">
                                                        <label class="mdl-textfield__label" for="sample3">Sub Total</label>
                                                    </div>
                                                </div>
                                                <div class="frm-group invoice-add">
                                                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                                        <input class="mdl-textfield__input" type="text" id="sample3">
                                                        <label class="mdl-textfield__label" for="sample3">Total</label>
                                                    </div>
                                                </div>
                                                <div class="Greand-total">
                                                    <h5>Due(INR) ₹ 0.00</h5>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="invoice-footer">
                                         <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 float-left">
                                            <div class="right-content col-lg-6 col-md-6 col-sm-6 col-xs-12 float-left">
                                                 <div class="frm-group">
                                                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                                        <input class="mdl-textfield__input" type="text" id="sample3">
                                                        <label class="mdl-textfield__label" for="sample3">Note</label>
                                                    </div>
                                                    <label class="add-note">Additional notes visible to your customer</label>
                                                </div>
                                            </div>
                                            <div class="right-content col-lg-6 col-md-6 col-sm-6 col-xs-12 float-left">
                                                <div class="frm-group">
                                                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                                        <input class="mdl-textfield__input" type="text" id="sample3">
                                                        <label class="mdl-textfield__label" for="sample3">Footer</label>
                                                    </div>
                                                    <label class="add-note">Tax Information or a thank you note for customer</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 float-left">
                                            <div class="invoice-btn col-lg-12 col-md-12 col-sm-12 col-xs-12 float-right">
                                                <div class="btn-group float-right">
                                                    <a href="index.html">Record Payment</a>
                                                    <a href="index.html">Send Invoice</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
 @endsection

      
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="js/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <!-- <script src="js/popper.js"></script> -->
    <script src="js/bootstrap.min.js"></script>
    <script src="js/material.min.js"></script>
    <script defer src="js/solid.js"></script>
    <script defer src="js/fontawesome.js"></script>
    <script src="js/canvasjs.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#sidebarCollapse').on('click', function () {
                $('#sidebar').toggleClass('active');
                $('.main-container').toggleClass('open-menu');
            });
        });
    </script>
   
  </body>
</html>

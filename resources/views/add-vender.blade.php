@extends('layouts.app')

@section('content')  
            <div class="main-order-section">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="bread-crum">
                                <nav class="breadcrumb">
                                  <a class="breadcrumb-item" href="index.html">Home</a>
                                  <a class="breadcrumb-item" href="vender-list.html#">My Vendors</a>
                                  <span class="breadcrumb-item active">Add Vender</span>
                                </nav>
                            </div>
                            <div class="vender-list-top-add-btn">
                                <h1>Add Vender</h1>
                            </div>
                                <div class="main-profile-page bg-white com-box-shodow rounded float-left">
                                    <div class="profile-left-side">
                                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 float-left p-l-0">
                                            <div class="profile-div">
                                                <div class="progile-image add-vernder-profile">
                                                    <div class="al">
                                                        <div class="alm">
                                                            <div class="profile-img-img">
                                                                <div class="add-cat-img-main">
                                                                    <div class="add-cat-img">
                                                                        <input name="" type="file">
                                                                        <i class="fas fa-plus-circle"></i>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 float-left">
                                            <div class="vender-detail-info">
                                                <div class="top-tab-penal">
                                                    <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                                                      <li class="nav-item">
                                                        <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#w-app" role="tab" aria-controls="pills-home" aria-selected="true">About</a>
                                                      </li>
                                                      <li class="nav-item">
                                                        <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#out-dill" role="tab" aria-controls="pills-profile" aria-selected="false">Organization Detail</a>
                                                      </li>
                                                    </ul>
                                                    <div class="tab-content" id="pills-tabContent">
                                                        <div class="tab-pane fade show active" id="w-app" role="tabpanel" aria-labelledby="pills-home-tab">
                                                            <div class="form-content">     
                                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 offset-lg-2 float-left add-vender-box">
                                                                    <div class="frm-group">
                                                                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                                                           <input class="mdl-textfield__input" type="text" id="sample3">
                                                                           <label class="mdl-textfield__label" for="sample3">First Name</label>
                                                                        </div>
                                                                    </div>
                                                                    <div class="frm-group">
                                                                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                                                           <input class="mdl-textfield__input" type="text" id="sample3">
                                                                           <label class="mdl-textfield__label" for="sample3">Last Name</label>
                                                                        </div>
                                                                    </div>
                                                                    <div class="main-gender-grp">
                                                                        <div class="left-lebel float-left">
                                                                            <label>Gender</label>
                                                                        </div>
                                                                        <label class="mdl-radio mdl-js-radio mdl-js-ripple-effect" for="option-1">
                                                                          <input type="radio" id="option-1" class="mdl-radio__button" name="options" value="1" checked>
                                                                          <span class="mdl-radio__label">Male</span>
                                                                        </label>
                                                                        <label class="mdl-radio mdl-js-radio mdl-js-ripple-effect" for="option-2">
                                                                          <input type="radio" id="option-2" class="mdl-radio__button" name="options" value="2">
                                                                          <span class="mdl-radio__label">Female</span>
                                                                        </label>
                                                                    </div>
                                                                    <div class="frm-group">
                                                                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                                                           <input class="mdl-textfield__input" type="text" id="sample3">
                                                                           <label class="mdl-textfield__label" for="sample3">Email</label>
                                                                        </div>
                                                                    </div>
                                                                    <div class="frm-group">
                                                                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                                                           <input class="mdl-textfield__input" type="text" id="sample3">
                                                                           <label class="mdl-textfield__label" for="sample3">Password</label>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 float-left add-vender-box">
                                                                    <div class="frm-group">
                                                                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                                                           <input class="mdl-textfield__input" type="text" id="sample3">
                                                                           <label class="mdl-textfield__label" for="sample3">Mobile Number</label>
                                                                        </div>
                                                                    </div>
                                                                
                                                                    <div class="frm-group">
                                                                          <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select">
                                                                            <input type="text" value="" class="mdl-textfield__input" id="sample4" readonly>
                                                                            <input type="hidden" value="" name="sample4">
                                                                            <i class="mdl-icon-toggle__label material-icons"></i>
                                                                            <label for="sample4" class="mdl-textfield__label">City</label>
                                                                            <ul for="sample4" class="mdl-menu mdl-menu--bottom-left mdl-js-menu">
                                                                                <li class="mdl-menu__item" data-val="DEU">Bangalore</li>
                                                                                <li class="mdl-menu__item" data-val="BLR">Pune</li>
                                                                                <li class="mdl-menu__item" data-val="RUS">Chennai</li>
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                    <div class="frm-group">
                                                                         <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select">
                                                                            <input type="text" value="" class="mdl-textfield__input" id="sample4" readonly>
                                                                            <input type="hidden" value="" name="sample4">
                                                                            <i class="mdl-icon-toggle__label material-icons"></i>
                                                                            <label for="sample4" class="mdl-textfield__label">State</label>
                                                                            <ul for="sample4" class="mdl-menu mdl-menu--bottom-left mdl-js-menu">
                                                                                <li class="mdl-menu__item" data-val="DEU">Ahmedabad</li>
                                                                                <li class="mdl-menu__item" data-val="BLR">Mumbai</li>
                                                                                <li class="mdl-menu__item" data-val="RUS">Goa</li>
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="tab-pane fade" id="out-dill" role="tabpanel" aria-labelledby="pills-profile-tab">
                                                             <div class="form-content">     
                                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 offset-lg-2 float-left">
                                                                    <div class="frm-group">
                                                                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                                                           <input class="mdl-textfield__input" type="text" id="sample3">
                                                                           <label class="mdl-textfield__label" for="sample3">Company Name</label>
                                                                        </div>
                                                                    </div>
                                                                    <div class="frm-group">
                                                                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                                                           <input class="mdl-textfield__input" type="text" id="sample3">
                                                                           <label class="mdl-textfield__label" for="sample3">Address</label>
                                                                        </div>
                                                                    </div>
                                                                    <div class="frm-group">
                                                                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                                                           <input class="mdl-textfield__input" type="text" id="sample3">
                                                                           <label class="mdl-textfield__label" for="sample3">Email</label>
                                                                        </div>
                                                                    </div>
                                                                    <div class="frm-group">
                                                                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                                                           <input class="mdl-textfield__input" type="text" id="sample3">
                                                                           <label class="mdl-textfield__label" for="sample3">City</label>
                                                                        </div>
                                                                    </div>
                                                                    <div class="frm-group">
                                                                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                                                           <input class="mdl-textfield__input" type="text" id="sample3">
                                                                           <label class="mdl-textfield__label" for="sample3">Postal Code</label>
                                                                        </div>
                                                                    </div>
                                                                    <div class="frm-group">
                                                                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                                                           <input class="mdl-textfield__input" type="text" id="sample3">
                                                                           <label class="mdl-textfield__label" for="sample3">Country</label>
                                                                        </div>
                                                                    </div>
                                                                    <div class="frm-group">
                                                                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                                                           <input class="mdl-textfield__input" type="text" id="sample3">
                                                                           <label class="mdl-textfield__label" for="sample3">Region/state</label>
                                                                        </div>
                                                                    </div>


                                                                </div>
                                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 float-left">
                                                                     <div class="frm-group">
                                                                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                                                           <input class="mdl-textfield__input" type="text" id="sample3">
                                                                           <label class="mdl-textfield__label" for="sample3">TIN NO</label>
                                                                        </div>
                                                                    </div>
                                                                    <div class="frm-group">
                                                                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                                                           <input class="mdl-textfield__input" type="text" id="sample3">
                                                                           <label class="mdl-textfield__label" for="sample3">GSTIN NO</label>
                                                                        </div>
                                                                    </div>
                                                                    <div class="frm-group">
                                                                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                                                           <input class="mdl-textfield__input" type="text" id="sample3">
                                                                           <label class="mdl-textfield__label" for="sample3">CIN NO</label>
                                                                        </div>
                                                                    </div>
                                                                    <div class="frm-group">
                                                                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                                                           <input class="mdl-textfield__input" type="text" id="sample3">
                                                                           <label class="mdl-textfield__label" for="sample3">Contact</label>
                                                                        </div>
                                                                    </div>
                                                                    <div class="frm-group">
                                                                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                                                           <input class="mdl-textfield__input" type="text" id="sample3">
                                                                           <label class="mdl-textfield__label" for="sample3">Landline</label>
                                                                        </div>
                                                                    </div>
                                                                    <div class="frm-group">
                                                                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                                                           <input class="mdl-textfield__input" type="text" id="sample3">
                                                                           <label class="mdl-textfield__label" for="sample3">Website</label>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                
                                                                    
                                                                    
                                                                
                                                            </div>
                                                        </div>
                                                        <div class="invoice-btn tax-submit-btn add-vender-btn">
                                                            <div class="btn-group float-right">
                                                                <a href="index.html">Submit</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                
                            </div>
                        </div>
                    </div>
                </div>

            </div>
@endsection

      
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="js/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <!-- <script src="js/popper.js"></script> -->
    <script src="js/bootstrap.min.js"></script>
    <script src="js/material.min.js"></script>
    <script src="js/getmdl-select.js"></script>
    <script defer src="js/solid.js"></script>
    <script defer src="js/fontawesome.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            
            $('#sidebarCollapse').on('click', function () {
                $('#sidebar').toggleClass('active');
                $('.main-container').toggleClass('open-menu');
            });
        });
    </script>
    <script>
        $(document).ready(function() {
            $('#example').DataTable();
        } );

        equalheight = function(container){

        var currentTallest = 0,
             currentRowStart = 0,
             rowDivs = new Array(),
             $el,
             topPosition = 0;
         $(container).each(function() {

           $el = $(this);
           $($el).height('auto')
           topPostion = $el.position().top;

           if (currentRowStart != topPostion) {
             for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
               rowDivs[currentDiv].height(currentTallest);
             }
             rowDivs.length = 0; // empty the array
             currentRowStart = topPostion;
             currentTallest = $el.height();
             rowDivs.push($el);
           } else {
             rowDivs.push($el);
             currentTallest = (currentTallest < $el.height()) ? ($el.height()) : (currentTallest);
          }
           for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
             rowDivs[currentDiv].height(currentTallest);
           }
         });
        }

        $(window).load(function() {
          equalheight('.midd-btm-same');  
        });


        $(window).resize(function(){
          equalheight('.midd-btm-same');
        });
    </script>
  </body>
</html>
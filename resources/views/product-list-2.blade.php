@extends('layouts.app')

@section('content')  
            <div class="main-order-section">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="bread-crum">
                                <nav class="breadcrumb">
                                  <a class="breadcrumb-item" href="index.html">Home</a>
                                  <a class="breadcrumb-item" href="catalogue.html">Catalouges</a>
                                  <span class="breadcrumb-item active">Product Listing</span>
                                </nav>
                            </div>
                            <div class="catalouge-list">  
                                <div class="main-product-list">
                                    <div class="product-list-box">
                                        <div class="vender-list-top-add-btn text-left">
                                            <h1>Product Listing</h1>
                                            <div class="placeorder-btn">
                                                <div class="order-btn">
                                                    <a href="new-order.html">Placeorder</a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="prodcut-category-lilst-box row">
                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                                <div class="order-detail com-box-shodow rounded bg-white p-0 m-b-30">
                                                    <div class="order-pro-img midd-btm-same">
                                                        <img src="images/bis-1.jpg">
                                                    </div>
                                                    <div class="pro-detail-list midd-btm-same">
                                                        <div class="order-pro-info">
                                                            <a href="new-order.html"><h5>Biscuits - (5kg)</h5></a>
                                                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,</p>
                                                            <p>Product Code: <strong>#001122</strong></p>
                                                            <p>Stock Qty: <strong>6</strong></p>
                                                            <div class="input-group">
                                                                <span class="input-group-btn">
                                                                    <button class="mdl-button mdl-js-button mdl-button--fab mdl-button--mini-fab quantity-left-minus" type="button" data-type="minus">
                                                                      <i class="fas fa-minus"></i>
                                                                    </button>
                                                                    
                                                                </span>
                                                                <input type="text" id="quantity" name="quantity" class="form-control input-number" value="0" min="1" max="100">
                                                                <span class="input-group-btn">
                                                                    <button class="mdl-button mdl-js-button mdl-button--fab mdl-button--mini-fab quantity-right-plus" type="button" data-type="minus">
                                                                      <i class="fas fa-plus"></i>
                                                                    </button>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                                <div class="order-detail com-box-shodow rounded bg-white p-0 m-b-30">
                                                    <div class="order-pro-img midd-btm-same">
                                                        <img src="images/bi-2.jpg">
                                                    </div>
                                                    <div class="pro-detail-list midd-btm-same">
                                                        <div class="order-pro-info">
                                                            <a href="new-order.html"><h5>Biscuits - (3kg)</h5></a>
                                                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,</p>
                                                            <p>Product Code: <strong>#001122</strong></p>
                                                            <p>Stock Qty: <strong>6</strong></p>
                                                            <div class="input-group">
                                                                <span class="input-group-btn">
                                                                    <button class="mdl-button mdl-js-button mdl-button--fab mdl-button--mini-fab quantity-left-minus" type="button" data-type="minus">
                                                                      <i class="fas fa-minus"></i>
                                                                    </button>
                                                                    
                                                                </span>
                                                                <input type="text" id="quantity" name="quantity" class="form-control input-number" value="0" min="1" max="100">
                                                                <span class="input-group-btn">
                                                                    <button class="mdl-button mdl-js-button mdl-button--fab mdl-button--mini-fab quantity-right-plus" type="button" data-type="minus">
                                                                      <i class="fas fa-plus"></i>
                                                                    </button>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                                <div class="order-detail com-box-shodow rounded bg-white p-0 m-b-30">
                                                    <div class="order-pro-img midd-btm-same">
                                                        <img src="images/bis-3.jpg">
                                                    </div>
                                                    <div class="pro-detail-list midd-btm-same">
                                                        <div class="order-pro-info">
                                                            <a href="new-order.html"><h5>Biscuits - (1kg)</h5></a>
                                                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,</p>
                                                            <p>Product Code: <strong>#001122</strong></p>
                                                            <p>Stock Qty: <strong>6</strong></p>
                                                            <div class="input-group">
                                                                <span class="input-group-btn">
                                                                    <button class="mdl-button mdl-js-button mdl-button--fab mdl-button--mini-fab quantity-left-minus" type="button" data-type="minus">
                                                                      <i class="fas fa-minus"></i>
                                                                    </button>
                                                                    
                                                                </span>
                                                                <input type="text" id="quantity" name="quantity" class="form-control input-number" value="0" min="1" max="100">
                                                                <span class="input-group-btn">
                                                                    <button class="mdl-button mdl-js-button mdl-button--fab mdl-button--mini-fab quantity-right-plus" type="button" data-type="minus">
                                                                      <i class="fas fa-plus"></i>
                                                                    </button>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                                <div class="order-detail com-box-shodow rounded bg-white p-0 m-b-30">
                                                    <div class="order-pro-img midd-btm-same">
                                                        <img src="images/bis-4.jpg">
                                                    </div>
                                                    <div class="pro-detail-list midd-btm-same">
                                                        <div class="order-pro-info">
                                                            <a href="new-order.html"><h5>Biscuits - (500gm)</h5></a>
                                                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,</p>
                                                            <p>Product Code: <strong>#001122</strong></p>
                                                            <p>Stock Qty: <strong>6</strong></p>
                                                            <div class="input-group">
                                                                <span class="input-group-btn">
                                                                    <button class="mdl-button mdl-js-button mdl-button--fab mdl-button--mini-fab quantity-left-minus" type="button" data-type="minus">
                                                                      <i class="fas fa-minus"></i>
                                                                    </button>
                                                                    
                                                                </span>
                                                                <input type="text" id="quantity" name="quantity" class="form-control input-number" value="0" min="1" max="100">
                                                                <span class="input-group-btn">
                                                                    <button class="mdl-button mdl-js-button mdl-button--fab mdl-button--mini-fab quantity-right-plus" type="button" data-type="minus">
                                                                      <i class="fas fa-plus"></i>
                                                                    </button>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                                <div class="order-detail com-box-shodow rounded bg-white p-0 m-b-30">
                                                    <div class="order-pro-img midd-btm-same">
                                                        <img src="images/bis-5.jpg">
                                                    </div>
                                                    <div class="pro-detail-list midd-btm-same">
                                                        <div class="order-pro-info">
                                                            <a href="new-order.html"><h5>Biscuits - (250gm)</h5></a>
                                                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,</p>
                                                            <p>Product Code: <strong>#001122</strong></p>
                                                            <p>Stock Qty: <strong>6</strong></p>
                                                            <div class="input-group">
                                                                <span class="input-group-btn">
                                                                    <button class="mdl-button mdl-js-button mdl-button--fab mdl-button--mini-fab quantity-left-minus" type="button" data-type="minus">
                                                                      <i class="fas fa-minus"></i>
                                                                    </button>
                                                                    
                                                                </span>
                                                                <input type="text" id="quantity" name="quantity" class="form-control input-number" value="0" min="1" max="100">
                                                                <span class="input-group-btn">
                                                                    <button class="mdl-button mdl-js-button mdl-button--fab mdl-button--mini-fab quantity-right-plus" type="button" data-type="minus">
                                                                      <i class="fas fa-plus"></i>
                                                                    </button>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                                <div class="order-detail com-box-shodow rounded bg-white p-0 m-b-30">
                                                    <div class="order-pro-img midd-btm-same">
                                                        <img src="images/bis-6.jpg">
                                                    </div>
                                                    <div class="pro-detail-list midd-btm-same">
                                                        <div class="order-pro-info">
                                                            <a href="new-order.html"><h5>Biscuits - (150gm)</h5></a>
                                                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,</p>
                                                            <p>Product Code: <strong>#001122</strong></p>
                                                            <p>Stock Qty: <strong>6</strong></p>
                                                            <div class="input-group">
                                                                <span class="input-group-btn">
                                                                    <button class="mdl-button mdl-js-button mdl-button--fab mdl-button--mini-fab quantity-left-minus" type="button" data-type="minus">
                                                                      <i class="fas fa-minus"></i>
                                                                    </button>
                                                                    
                                                                </span>
                                                                <input type="text" id="quantity" name="quantity" class="form-control input-number" value="0" min="1" max="100">
                                                                <span class="input-group-btn">
                                                                    <button class="mdl-button mdl-js-button mdl-button--fab mdl-button--mini-fab quantity-right-plus" type="button" data-type="minus">
                                                                      <i class="fas fa-plus"></i>
                                                                    </button>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                                <div class="order-detail com-box-shodow rounded bg-white p-0 m-b-30">
                                                    <div class="order-pro-img midd-btm-same">
                                                        <img src="images/bi-2.jpg">
                                                    </div>
                                                    <div class="pro-detail-list midd-btm-same">
                                                        <div class="order-pro-info">
                                                            <a href="new-order.html"><h5>Biscuits - (300gm)</h5></a>
                                                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,</p>
                                                            <p>Product Code: <strong>#001122</strong></p>
                                                            <p>Stock Qty: <strong>6</strong></p>
                                                            <div class="input-group">
                                                                <span class="input-group-btn">
                                                                    <button class="mdl-button mdl-js-button mdl-button--fab mdl-button--mini-fab quantity-left-minus" type="button" data-type="minus">
                                                                      <i class="fas fa-minus"></i>
                                                                    </button>
                                                                    
                                                                </span>
                                                                <input type="text" id="quantity" name="quantity" class="form-control input-number" value="0" min="1" max="100">
                                                                <span class="input-group-btn">
                                                                    <button class="mdl-button mdl-js-button mdl-button--fab mdl-button--mini-fab quantity-right-plus" type="button" data-type="minus">
                                                                      <i class="fas fa-plus"></i>
                                                                    </button>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
@endsection
 
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="js/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <!-- <script src="js/popper.js"></script> -->
    <script src="js/bootstrap.min.js"></script>
    <script src="js/material.min.js"></script>
    
    <script defer src="js/solid.js"></script>
    <script defer src="js/fontawesome.js"></script>
    <script src="js/canvasjs.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#sidebarCollapse').on('click', function () {
                $('#sidebar').toggleClass('active');
                $('.main-container').toggleClass('open-menu');
            });
        });
    </script>
   <script type="text/javascript">
window.onload = function () {

var chart = new CanvasJS.Chart("chartContainer", {
    animationEnabled: true,
    title:{
        text: "Total Sales"
    },
    axisY: {
        title: "    "
    },
    legend: {
        cursor:"pointer",
        itemclick : toggleDataSeries
    },
    toolTip: {
        shared: true,
        content: toolTipFormatter
    },
    data: [{
        type: "bar",
        showInLegend: true,
        name: "New Order receives",
        color: "gold",
        dataPoints: [
            { y: 243, label: "Monday" },
            { y: 236, label: "Tuesday" },
            { y: 243, label: "Wednesday" },
            { y: 273, label: "Thursday" },
            { y: 269, label: "Friday" },
            { y: 196, label: "Saturday" },
            { y: 1118, label: "Sunday" }
        ]
    },
    {
        type: "bar",
        showInLegend: true,
        name: "Item to be Delivered",
        color: "silver",
        dataPoints: [
            { y: 212, label: "Monday" },
            { y: 186, label: "Tuesday" },
            { y: 272, label: "Wednesday" },
            { y: 299, label: "Thursday" },
            { y: 270, label: "Friday" },
            { y: 165, label: "Saturday" },
            { y: 896, label: "Sunday" }
        ]
    },
    {
        type: "bar",
        showInLegend: true,
        name: "Item to be Packed",
        color: "#A57164",
        dataPoints: [
            { y: 236, label: "Monday" },
            { y: 172, label: "Tuesday" },
            { y: 309, label: "Wednesday" },
            { y: 302, label: "Thursday" },
            { y: 285, label: "Friday" },
            { y: 188, label: "Saturday" },
            { y: 788, label: "Sunday" }
        ]
    }]
});
chart.render();

function toolTipFormatter(e) {
    var str = "";
    var total = 0 ;
    var str3;
    var str2 ;
    for (var i = 0; i < e.entries.length; i++){
        var str1 = "<span style= 'color:"+e.entries[i].dataSeries.color + "'>" + e.entries[i].dataSeries.name + "</span>: <strong>"+  e.entries[i].dataPoint.y + "</strong> <br/>" ;
        total = e.entries[i].dataPoint.y + total;
        str = str.concat(str1);
    }
    str2 = "<strong>" + e.entries[0].dataPoint.label + "</strong> <br/>";
    str3 = "<span style = 'color:Tomato'>Total: </span><strong>" + total + "</strong><br/>";
    return (str2.concat(str)).concat(str3);
}

function toggleDataSeries(e) {
    if (typeof (e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
        e.dataSeries.visible = false;
    }
    else {
        e.dataSeries.visible = true;
    }
    chart.render();
}
}

$(document).ready(function(){

    var quantitiy=0;
       $('.quantity-right-plus').click(function(e){
            
            // Stop acting like a button
            e.preventDefault();
            // Get the field name
            var quantity = parseInt($(this).closest('.input-group').find('#quantity').val());
            
            // If is not undefined
                
                $(this).closest('.input-group').find('#quantity').val(quantity + 1);

              
                // Increment
            
        });

         $('.quantity-left-minus').click(function(e){
            // Stop acting like a button
            e.preventDefault();
            // Get the field name
            var quantity = parseInt($(this).closest('.input-group').find('#quantity').val());
            
            // If is not undefined
          
                // Increment
                if(quantity>0){
                $(this).closest('.input-group').find('#quantity').val(quantity - 1);
                }
        });
        
    });
</script>
<script>
        $(document).ready(function() {
            $('#example').DataTable();
        } );

        equalheight = function(container){

        var currentTallest = 0,
             currentRowStart = 0,
             rowDivs = new Array(),
             $el,
             topPosition = 0;
         $(container).each(function() {

           $el = $(this);
           $($el).height('auto')
           topPostion = $el.position().top;

           if (currentRowStart != topPostion) {
             for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
               rowDivs[currentDiv].height(currentTallest);
             }
             rowDivs.length = 0; // empty the array
             currentRowStart = topPostion;
             currentTallest = $el.height();
             rowDivs.push($el);
           } else {
             rowDivs.push($el);
             currentTallest = (currentTallest < $el.height()) ? ($el.height()) : (currentTallest);
          }
           for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
             rowDivs[currentDiv].height(currentTallest);
           }
         });
        }

        $(window).load(function() {
          equalheight('.midd-btm-same');  
        });


        $(window).resize(function(){
          equalheight('.midd-btm-same');
        });
    </script>
  </body>
</html>

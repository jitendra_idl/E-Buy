@extends('layouts.app')

@section('content')  
            <div class="main-order-section">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="bread-crum">
                                <nav class="breadcrumb">
                                  <a class="breadcrumb-item" href="index.html">Home</a>
                                  <a class="breadcrumb-item" href="order.html">Order List</a>
                                  <span class="breadcrumb-item active">Order History</span>
                                </nav>
                            </div>
                            <div class="vender-list-top-add-btn">
                                <h1>Order History</h1>
                            </div>
                            <div class="catalouge-list">    
                                <table id="example" class="table text-left" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>Item No</th>
                                            <th>Image</th>
                                            <th>Item Name</th>
                                            <th>Status</th>
                                            <th>Date</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                       <tr>
                                            <td class="order-td">#001232</td>
                                            <td class="order-td"><img src="images/pro-6.jpg"></td>
                                            <td class="name-td">Lorem Ipsum is simply</td>
                                            <td class="Status-td text-success">Paid</td>
                                            <td class="date-td">10 jul 2018</td>
                                       </tr>
                                       <tr>
                                            <td class="order-td">#001232</td>
                                            <td class="order-td"><img src="images/pro-6.jpg"></td>
                                            <td class="name-td">Lorem Ipsum is simply dummy</td>
                                            <td class="Status-td text-warning">Pending</td>
                                            <td class="date-td">10 jul 2018</td>
                                       </tr>
                                       <tr>
                                            <td class="order-td">#001232</td>
                                            <td class="order-td"><img src="images/pro-6.jpg"></td>
                                            <td class="name-td">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</td>
                                            <td class="Status-td text-warning">Pending</td>
                                            <td class="date-td">10 jul 2018</td>
                                       </tr><tr>
                                            <td class="order-td">#001232</td>
                                            <td class="order-td"><img src="images/pro-6.jpg"></td>
                                            <td class="name-td">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</td>
                                            <td class="Status-td text-warning">Pending</td>
                                            <td class="date-td">10 jul 2018</td>
                                       </tr>
                                       <tr>
                                            <td class="order-td">#001232</td>
                                            <td class="order-td"><img src="images/pro-6.jpg"></td>
                                            <td class="name-td">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</td>
                                            <td class="Status-td text-warning">Pending</td>
                                            <td class="date-td">10 jul 2018</td>
                                       </tr>

                                       
                                       
                                       
                                    </tbody>
                                </table>   
                            </div>
                        </div>
                    </div>
                </div>

            </div>
@endsection


    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="js/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <!-- <script src="js/popper.js"></script> -->
    <script src="js/bootstrap.min.js"></script>
    <script src="js/material.min.js"></script>
    <script src="js/getmdl-select.js"></script>
    <script defer src="js/solid.js"></script>
    <script defer src="js/fontawesome.js"></script>
    <script src="js/canvasjs.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#sidebarCollapse').on('click', function () {
                $('#sidebar').toggleClass('active');
                $('.main-container').toggleClass('open-menu');
            });
        });
    </script>
    <script>
        $(document).ready(function() {
            $('#example').DataTable();
        } );
    </script>
  </body>
</html>
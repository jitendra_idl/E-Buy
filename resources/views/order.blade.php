@extends('layouts.app')

@section('content') 
            <div class="main-order-section">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="bread-crum">
                                <nav class="breadcrumb">
                                  <a class="breadcrumb-item" href="index.html">Home</a>
                                  <span class="breadcrumb-item active">Order List</span>
                                </nav>
                            </div>
                            <div class="catalouge-list">  
                                <div class="page-headeing">
                                    <h5>Order List</h5>
                                </div>
                                <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                                  <li class="nav-item">
                                    <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#w-app" role="tab" aria-controls="pills-home" aria-selected="true">Waiting for Approval</a>
                                  </li>
                                  <li class="nav-item">
                                    <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#out-dill" role="tab" aria-controls="pills-profile" aria-selected="false">Out of Delivery</a>
                                  </li>
                                  <li class="nav-item">
                                    <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#order-dille" role="tab" aria-controls="pills-profile" aria-selected="false">Order Delivery</a>
                                  </li>
                                  <li class="nav-item">
                                    <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#cancle" role="tab" aria-controls="pills-profile" aria-selected="false">Cancel</a>
                                  </li>
                                </ul>
                                <div class="placeorder-btn">
                                    <div class="order-btn">
                                        <a href="{{ route('venderList2') }}">Placeorder</a>
                                    </div>
                                </div>
                                <div class="tab-content" id="pills-tabContent">
                                  <div class="tab-pane fade show active" id="w-app" role="tabpanel" aria-labelledby="pills-home-tab">
                                      <div class="col-lg-12">
                                        <div class="row">
                                            <div class="order-main-box bg-white com-box-shodow rounded float-left m-b-20">
                                                <div class="order-top-bar">
                                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 float-left">
                                                        <div class="order-id">
                                                            <h2>Manshukhnhai Ajudiya</h2>
                                                            <h5>Order No <span>#001200</span></h5>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 float-left">
                                                        <div class="float-right">
                                                            <div class="date-formate">
                                                                <i class="fas fa-calendar-alt"></i>
                                                                <p>13-07-2018</p>
                                                            </div>
                                                            <div class="date-formate">
                                                                <i class="fas fa-clock"></i>
                                                                <p>10:00 AM</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="order-detail">
                                                    <div class="order-pro-img">
                                                        <img src="images/pro.jpeg">
                                                    </div>
                                                    <div class="order-pro-info">
                                                        <h5>Save Khamni</h5>
                                                        <p>Lorem Ipsum is simply dummy text</p>
                                                        <!-- <p><span>Order Delivered</span> (10-06-2018)</p> -->
                                                        <div class="order-prise">
                                                            <h2>₹ 500.00</h2>
                                                        </div>
                                                    </div>
                                                    
                                                </div>
                                                <div class="order-detail">
                                                    <div class="order-pro-img">
                                                        <img src="images/pro-1.jpg">
                                                    </div>
                                                    <div class="order-pro-info">
                                                        <h5>Save Khamni</h5>
                                                        <p>Lorem Ipsum is simply</p>
                                                        <!-- <p><span>Order Delivered</span> (10-06-2018)</p> -->
                                                        <div class="order-prise">
                                                            <h2>₹ 500.00</h2>
                                                        </div>
                                                    </div>
                                                    
                                                </div>
                                                <div class="all-order-total">
                                                     <!-- Select with floating label -->
                                                   <div class="float-left">
                                                    <!-- Select with floating label and arrow -->
                                                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select float-left">
                                                            <input type="text" value="" class="mdl-textfield__input" id="sample49" readonly>
                                                            <input type="hidden" value="" name="sample49">
                                                            <!-- <i class="mdl-icon-toggle__label material-icons">keyboard_arrow_down</i> -->
                                                            <label for="sample49" class="mdl-textfield__label">Waiting for Approval</label>
                                                            <ul for="sample49" class="mdl-menu mdl-menu--bottom-left mdl-js-menu">
                                                                <li class="mdl-menu__item" data-val="DEU">Out for delivery</li>
                                                                <li class="mdl-menu__item" data-val="BLR">Order Delivered</li>
                                                                <li class="mdl-menu__item" data-val="RUS">Cancel</li>
                                                            </ul>
                                                        </div>
                                                        <div class="upload-satus">
                                                            <a href="#exampleModalCenter" data-toggle="modal">Upload Status</a>
                                                        </div>
                                                   </div>
                                                                                            
                                                    <div class="float-right">
                                                        <h1>Order Total: ₹ 1000.00</h1>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="order-main-box bg-white com-box-shodow rounded float-left m-b-20">
                                                <div class="order-top-bar">
                                                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6 float-left">
                                                        <div class="order-id">
                                                            <h2>Manshukhnhai Ajudiya</h2>
                                                            <h5>Order No <span>#001200</span></h5>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-9 col-md-9 col-sm-8 col-xs-6 float-left">
                                                        <div class="float-right">
                                                            <div class="date-formate">
                                                                <i class="fas fa-calendar-alt"></i>
                                                                <p>13-07-2018</p>
                                                            </div>
                                                            <div class="date-formate">
                                                                <i class="fas fa-clock"></i>
                                                                <p>10:00 AM</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="order-detail">
                                                    <div class="order-pro-img">
                                                        <img src="images/pro-2.jpg">
                                                    </div>
                                                    <div class="order-pro-info">
                                                        <h5>Save Khamni</h5>
                                                        <p>Lorem Ipsum is simply dummy text</p>
                                                        <!-- <p><span>Order Delivered</span> (10-06-2018)</p> -->
                                                        <div class="order-prise">
                                                            <h2>₹ 500.00</h2>
                                                        </div>
                                                    </div>
                                                    
                                                </div>
                                                
                                                <div class="all-order-total">
                                                    <div class="float-left">
                                                    <!-- Select with floating label and arrow -->
                                                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select float-left">
                                                            <input type="text" value="" class="mdl-textfield__input" id="sample42" readonly>
                                                            <input type="hidden" value="" name="sample42">
                                                            <!-- <i class="mdl-icon-toggle__label material-icons">keyboard_arrow_down</i> -->
                                                            <label for="sample42" class="mdl-textfield__label">Waiting for Approval</label>
                                                            <ul for="sample4" class="mdl-menu mdl-menu--bottom-left mdl-js-menu">
                                                                <li class="mdl-menu__item" data-val="DEU">Out for delivery</li>
                                                                <li class="mdl-menu__item" data-val="BLR">Order Delivered</li>
                                                                <li class="mdl-menu__item" data-val="RUS">Cancel</li>
                                                            </ul>
                                                        </div>
                                                        <div class="upload-satus">
                                                            <a href="#exampleModalCenter" data-toggle="modal">Upload Status</a>
                                                        </div>
                                                   </div>
                                                    <div class="float-right">
                                                        <h1>Order Total: ₹ 1000.00</h1>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="order-main-box bg-white com-box-shodow rounded float-left m-b-20">
                                                <div class="order-top-bar">
                                                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6 float-left">
                                                        <div class="order-id">
                                                            <h2>Manshukhnhai Ajudiya</h2>
                                                            <h5>Order No <span>#001200</span></h5>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-9 col-md-9 col-sm-8 col-xs-6 float-left">
                                                        <div class="float-right">
                                                            <div class="date-formate">
                                                                <i class="fas fa-calendar-alt"></i>
                                                                <p>13-07-2018</p>
                                                            </div>
                                                            <div class="date-formate">
                                                                <i class="fas fa-clock"></i>
                                                                <p>10:00 AM</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="order-detail">
                                                    <div class="order-pro-img">
                                                        <img src="images/pro-3.jpg">
                                                    </div>
                                                    <div class="order-pro-info">
                                                        <h5>Save Khamni</h5>
                                                        <p>Lorem Ipsum is simply dummy text</p>
                                                        <!-- <p><span>Order Delivered</span> (10-06-2018)</p> -->
                                                        <div class="order-prise">
                                                            <h2>₹ 500.00</h2>
                                                        </div>
                                                    </div>
                                                    
                                                </div>
                                                <div class="all-order-total">
                                                    <div class="float-left">
                                                    <!-- Select with floating label and arrow -->
                                                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select float-left">
                                                            <input type="text" value="" class="mdl-textfield__input" id="sample43" readonly>
                                                            <input type="hidden" value="" name="sample43">
                                                            <!-- <i class="mdl-icon-toggle__label material-icons">keyboard_arrow_down</i> -->
                                                            <label for="sample43" class="mdl-textfield__label">Waiting for Approval</label>
                                                            <ul for="sample43" class="mdl-menu mdl-menu--bottom-left mdl-js-menu">
                                                                <li class="mdl-menu__item" data-val="DEU">Out for delivery</li>
                                                                <li class="mdl-menu__item" data-val="BLR">Order Delivered</li>
                                                                <li class="mdl-menu__item" data-val="RUS">Cancel</li>
                                                            </ul>
                                                        </div>
                                                        <div class="upload-satus">
                                                            <a href="#exampleModalCenter" data-toggle="modal">Upload Status</a>
                                                        </div>
                                                   </div>
                                                    <div class="float-right">
                                                        <h1>Order Total: ₹ 1000.00</h1>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="order-main-box bg-white com-box-shodow rounded float-left m-b-20">
                                                <div class="order-top-bar">
                                                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6 float-left">
                                                        <div class="order-id">
                                                            <h2>Manshukhnhai Ajudiya</h2>
                                                            <h5>Order No <span>#001200</span></h5>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-9 col-md-9 col-sm-8 col-xs-6 float-left">
                                                        <div class="float-right">
                                                            <div class="date-formate">
                                                                <i class="fas fa-calendar-alt"></i>
                                                                <p>13-07-2018</p>
                                                            </div>
                                                            <div class="date-formate">
                                                                <i class="fas fa-clock"></i>
                                                                <p>10:00 AM</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="order-detail">
                                                    <div class="order-pro-img">
                                                        <img src="images/pro-4.png">
                                                    </div>
                                                    <div class="order-pro-info">
                                                        <h5>Save Khamni</h5>
                                                        <p>Lorem Ipsum is </p>
                                                        <!-- <p><span>Order Delivered</span> (10-06-2018)</p> -->
                                                        <div class="order-prise">
                                                            <h2>₹ 500.00</h2>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="all-order-total">
                                                    <div class="float-left">
                                                    <!-- Select with floating label and arrow -->
                                                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select float-left">
                                                            <input type="text" value="" class="mdl-textfield__input" id="sample44" readonly>
                                                            <input type="hidden" value="" name="sample44">
                                                            <!-- <i class="mdl-icon-toggle__label material-icons">keyboard_arrow_down</i> -->
                                                            <label for="sample44" class="mdl-textfield__label">Waiting for Approval</label>
                                                            <ul for="sample44" class="mdl-menu mdl-menu--bottom-left mdl-js-menu">
                                                                <li class="mdl-menu__item" data-val="DEU">Out for delivery</li>
                                                                <li class="mdl-menu__item" data-val="BLR">Order Delivered</li>
                                                                <li class="mdl-menu__item" data-val="RUS">Cancel</li>
                                                            </ul>
                                                        </div>
                                                        <div class="upload-satus">
                                                            <a href="#exampleModalCenter" data-toggle="modal">Upload Status</a>
                                                        </div>
                                                   </div>
                                                    <div class="float-right">
                                                        <h1>Order Total: ₹ 1000.00</h1>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                      </div>
                                  </div>
                                  <div class="tab-pane fade" id="out-dill" role="tabpanel" aria-labelledby="pills-profile-tab">
                                      <div class="order-main-box bg-white com-box-shodow rounded float-left m-b-20">
                                                <div class="order-top-bar">
                                                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6 float-left">
                                                        <div class="order-id">
                                                            <h2>Manshukhnhai Ajudiya</h2>
                                                            <h5>Order No <span>#001200</span></h5>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-9 col-md-9 col-sm-8 col-xs-6 float-left">
                                                        <div class="float-right">
                                                            <div class="date-formate">
                                                                <i class="fas fa-calendar-alt"></i>
                                                                <p>13-07-2018</p>
                                                            </div>
                                                            <div class="date-formate">
                                                                <i class="fas fa-clock"></i>
                                                                <p>10:00 AM</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="order-detail">
                                                    <div class="order-pro-img">
                                                        <img src="images/pro-2.jpg">
                                                    </div>
                                                    <div class="order-pro-info">
                                                        <h5>Save Khamni</h5>
                                                        <p>Lorem Ipsum is simply dummy text Lorem Ipsum is simply dummy text</p>
                                                        <!-- <p><span>Order Delivered</span> (10-06-2018)</p> -->
                                                        <div class="order-prise">
                                                            <h2>₹ 500.00</h2>
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                                <div class="all-order-total">
                                                    <div class="float-right">
                                                        <h1>Order Total: ₹ 1000.00</h1>
                                                    </div>
                                                </div>
                                            </div>
                                  </div>
                                  <div class="tab-pane fade" id="order-dille" role="tabpanel" aria-labelledby="pills-profile-tab">
                                      <div class="order-main-box bg-white com-box-shodow rounded float-left m-b-20">
                                                <div class="order-top-bar">
                                                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6 float-left">
                                                        <div class="order-id">
                                                            <h2>Manshukhnhai Ajudiya</h2>
                                                            <h5>Order No <span>#001200</span></h5>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-9 col-md-9 col-sm-8 col-xs-6 float-left">
                                                        <div class="float-right">
                                                            <div class="date-formate">
                                                                <i class="fas fa-calendar-alt"></i>
                                                                <p>13-07-2018</p>
                                                            </div>
                                                            <div class="date-formate">
                                                                <i class="fas fa-clock"></i>
                                                                <p>10:00 AM</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="order-detail">
                                                    <div class="order-pro-img">
                                                        <img src="images/pro-4.png">
                                                    </div>
                                                    <div class="order-pro-info">
                                                        <h5>Save Khamni</h5>
                                                        <p>Lorem Ipsum is simply dummy text </p>
                                                        <!-- <p><span>Order Delivered</span> (10-06-2018)</p> -->
                                                        <div class="order-prise">
                                                            <h2>₹ 500.00</h2>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="all-order-total">
                                                    <div class="float-right">
                                                        <h1>Order Total: ₹ 1000.00</h1>
                                                    </div>
                                                </div>
                                            </div>
                                  </div>
                                  <div class="tab-pane fade" id="cancle" role="tabpanel" aria-labelledby="pills-profile-tab">
                                      <div class="order-main-box bg-white com-box-shodow rounded float-left m-b-20">
                                                <div class="order-top-bar">
                                                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6 float-left">
                                                        <div class="order-id">
                                                            <h2>Manshukhnhai Ajudiya</h2>
                                                            <h5>Order No <span>#001200</span></h5>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-9 col-md-9 col-sm-8 col-xs-6 float-left">
                                                        <div class="float-right">
                                                            <div class="date-formate">
                                                                <i class="fas fa-calendar-alt"></i>
                                                                <p>13-07-2018</p>
                                                            </div>
                                                            <div class="date-formate">
                                                                <i class="fas fa-clock"></i>
                                                                <p>10:00 AM</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="order-detail">
                                                    <div class="order-pro-img">
                                                        <img src="images/pro-2.jpg">
                                                    </div>
                                                    <div class="order-pro-info">
                                                        <h5>Save Khamni</h5>
                                                        <p>Lorem Ipsum is simply dummy</p>
                                                        
                                                        <div class="order-prise">
                                                            <h2>₹ 500.00</h2>
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                                <div class="all-order-total">
                                                    <div class="float-right">
                                                        <h1>Order Total: ₹ 1000.00</h1>
                                                    </div>
                                                </div>
                                            </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
   
      <!-- Button trigger modal -->

<!-- Modal -->
    <div class="modal fade bd-example-modal-lg" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLongTitle">Choose Delivery Boy</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="search-bar col-sm-12 col-md-12 col-xs-12 float-left p-0">
                <form action="#">
                  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                    <input class="mdl-textfield__input" type="text" id="sample3">
                    <label class="mdl-textfield__label" for="sample3">Search...</label>
                  </div>
                </form>
            </div>
            <div class="delivery-boy-listing">
                <div class="first-heading">
                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 float-left">
                        <p>Image</p>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 float-left">
                        <p>Name</p>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 float-left">
                        <p>Mobile NO</p>
                    </div>
                </div>
                <div class="list-body">
                    <div class="list-body-row">
                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 float-left">
                            <div class="profile-img">
                                <img src="images/profile-img.png">
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 float-left">
                            <div class="profile-name">
                                <h6>Mohil Sojitra</h6>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 float-left">
                            <div class="profile-name">
                                <h6>+91 9865986598</h6>
                                <label class="mdl-radio mdl-js-radio mdl-js-ripple-effect" for="option-1">
                                    <input type="radio" id="option-1" class="mdl-radio__button" name="options" value="1" checked>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="list-body-row">
                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 float-left">
                            <div class="profile-img">
                                <img src="images/profile-img.png">
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 float-left">
                            <div class="profile-name">
                                <h6>Mohil Sojitra</h6>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 float-left">
                            <div class="profile-name">
                                <h6>+91 9865986598</h6>
                                <label class="mdl-radio mdl-js-radio mdl-js-ripple-effect" for="option-2">
                                    <input type="radio" id="option-2" class="mdl-radio__button" name="options" value="1" checked>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="list-body-row">
                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 float-left">
                            <div class="profile-img">
                                <img src="images/profile-img.png">
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 float-left">
                            <div class="profile-name">
                                <h6>Mohil Sojitra</h6>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 float-left">
                            <div class="profile-name">
                                <h6>+91 9865986598</h6>
                                <label class="mdl-radio mdl-js-radio mdl-js-ripple-effect" for="option-3">
                                    <input type="radio" id="option-3" class="mdl-radio__button" name="options" value="1" checked>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="list-body-row">
                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 float-left">
                            <div class="profile-img">
                                <img src="images/profile-img.png">
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 float-left">
                            <div class="profile-name">
                                <h6>Mohil Sojitra</h6>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 float-left">
                            <div class="profile-name">
                                <h6>+91 9865986598</h6>
                                <label class="mdl-radio mdl-js-radio mdl-js-ripple-effect" for="option-4">
                                    <input type="radio" id="option-4" class="mdl-radio__button" name="options" value="1" checked>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="list-body-row">
                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 float-left">
                            <div class="profile-img">
                                <img src="images/profile-img.png">
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 float-left">
                            <div class="profile-name">
                                <h6>Mohil Sojitra</h6>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 float-left">
                            <div class="profile-name">
                                <h6>+91 9865986598</h6>
                                <label class="mdl-radio mdl-js-radio mdl-js-ripple-effect" for="option-5">
                                    <input type="radio" id="option-5" class="mdl-radio__button" name="options" value="1" checked>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="list-body-row">
                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 float-left">
                            <div class="profile-img">
                                <img src="images/profile-img.png">
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 float-left">
                            <div class="profile-name">
                                <h6>Mohil Sojitra</h6>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 float-left">
                            <div class="profile-name">
                                <h6>+91 9865986598</h6>
                                <label class="mdl-radio mdl-js-radio mdl-js-ripple-effect" for="option-6">
                                    <input type="radio" id="option-6" class="mdl-radio__button" name="options" value="1" checked>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
          </div>
          <div class="modal-footer">
            <a href="index.html">
                <button type="button" class="model-btn">Submit</button>
            </a>
          </div>
        </div>
      </div>
    </div>

@endsection
   
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="js/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <!-- <script src="js/popper.js"></script> -->
    <script src="js/bootstrap.min.js"></script>
    <script src="js/material.min.js"></script>
    <script src="js/getmdl-select.js"></script>
    
    
    <script defer src="js/solid.js"></script>
    <script defer src="js/fontawesome.js"></script>
    <script src="js/canvasjs.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#sidebarCollapse').on('click', function () {
                $('#sidebar').toggleClass('active');
                $('.main-container').toggleClass('open-menu');
            });
        });
    </script>
   
  </body>
</html>
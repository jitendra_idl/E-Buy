@extends('layouts.app')

@section('content')    
            <div class="main-order-section">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12 float-left">
                            <div class="row">
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6 float-left ">
                                    <div class="main-boxes bg-white com-box-shodow rounded">
                                        <div class="order-box-content p-10 ">
                                            <h2>{{ $data['New Order Received'] }}</h2>
                                            <p>New Order Received</p>
                                        </div>
                                        <div class="icon-new">
                                            <i class="fas fa-users"></i>
                                        </div>
                                    </div>
                                </div>
                                <div class=" col-lg-4 col-md-4 col-sm-4 col-xs-6 float-left">
                                    <div class="main-boxes bg-white com-box-shodow rounded">
                                        <div class="order-box-content p-10">
                                            <h2>{{ $data['Order Cancel'] }}</h2>
                                            <p>Order Cancel</p>
                                        </div>
                                        <div class="icon-new">
                                            <i class="fas fa-users"></i>
                                        </div>
                                    </div>
                                </div>
                                <div class=" col-lg-4 col-md-4 col-sm-4 col-xs-6 float-left">
                                    <div class="main-boxes bg-white com-box-shodow rounded">
                                        <div class="order-box-content p-10 ">
                                            <h2 class="text-warning">{{ $data['No Of Items Solds'] }}</h2>
                                            <p>No of item sold</p>
                                        </div>
                                        <div class="icon-new">
                                            <i class="fas fa-users"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="chart-section">
                            <div class="col-lg-8 col-md-8 col-sm-6 col-xs-12 float-left">
                                <div id="chartContainer" style="height: 481px; max-width: 100%; margin: 0px auto;"></div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 float-left right-side-order">
                                 <div class=" col-lg-12 col-md-12 col-sm-12 col-xs-12 float-left p-l-0 p-r-0 m-b-15">
                                    <div class="main-boxes bg-white com-box-shodow rounded">
                                        <div class="order-box-content p-10 ">
                                            <h2 class="text-danger">78</h2>
                                            <p>Item Out of Stock</p>
                                        </div>
                                        <div class="icon-new">
                                            <i class="fas fa-users"></i>
                                        </div>
                                    </div>
                                </div>
                                <div class=" col-lg-12 col-md-12 col-sm-12 col-xs-12 float-left p-l-0 p-r-0 m-b-15">
                                    <div class="main-boxes bg-white com-box-shodow rounded">
                                        <div class="order-box-content p-10 ">
                                            <h2>1000</h2>
                                            <p>Items soon Out of Stock</p>
                                        </div>
                                        <div class="icon-new">
                                            <i class="fas fa-users"></i>
                                        </div>
                                    </div>
                                </div>
                                <div class=" col-lg-12 col-md-12 col-sm-12 col-xs-12 float-left p-l-0 p-r-0 m-b-15">
                                    <div class="main-boxes bg-white com-box-shodow rounded">
                                        <div class="order-box-content p-10 ">
                                            <h2>1000</h2>
                                            <p>User Feedback</p>
                                        </div>
                                        <div class="icon-new">
                                            <i class="fas fa-users"></i>
                                        </div>
                                    </div>
                                </div>
                                <div class=" col-lg-12 col-md-12 col-sm-12 col-xs-12 float-left p-l-0 p-r-0 m-b-30">
                                    <div class="main-boxes bg-white com-box-shodow rounded">
                                        <div class="order-box-content p-10 ">
                                            <h2>1000</h2>
                                            <p>Repeat Customer rate</p>
                                        </div>
                                        <div class="icon-new">
                                            <i class="fas fa-users"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                         <div class="chart-section">
                            <div class="col-lg-8 col-md-8 col-sm-6 col-xs-12 float-left">
                                <div class="map-ifream">
                                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d248849.88653870064!2d77.49085525490416!3d12.953959988510581!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bae1670c9b44e6d%3A0xf8dfc3e8517e4fe0!2sBengaluru%2C+Karnataka!5e0!3m2!1sen!2sin!4v1532030646850" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 float-left right-side-order">
                                 
                                <div class="order-income-cart bg-white com-box-shodow m-b-15">
                                    <div class="order-cchar-box">
                                        <h5>Comparison</h5>
                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 char-box-new float-left">
                                            <div class="al">
                                                <div class="alm">
                                                    <div class="wall-div">
                                                        <div class="graph-round">
                                                            <svg viewBox="0 0 100 100" style="display: inline-block; width: 150px">
                                                                <path d="M 50,50 m 0,-47 a 47,47 0 1 1 0,94 a 47,47 0 1 1 0,-94" stroke="#eee" stroke-width="6" fill-opacity="0"></path>
                                                                <path d="M 50,50 m 0,-47 a 47,47 0 1 1 0,94 a 47,47 0 1 1 0,-94" stroke="rgb(81,181,63)" stroke-width="6" fill-opacity="0" style="stroke-dasharray: 295.416, 295.416; stroke-dashoffset: 200;"></path>
                                                            </svg>
                                                        </div>
                                                        <div class="graph-detail">
                                                            <span>Order</span>
                                                            <h3>₹ 65000</h3>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 char-box-new float-left">
                                            <div class="al">
                                                <div class="alm">
                                                    <div class="wall-div">
                                                        <div class="graph-round">
                                                            <svg viewBox="0 0 100 100" style="display: inline-block; width: 150px">
                                                                <path d="M 50,50 m 0,-47 a 47,47 0 1 1 0,94 a 47,47 0 1 1 0,-94" stroke="#eee" stroke-width="6" fill-opacity="0"></path>
                                                                <path d="M 50,50 m 0,-47 a 47,47 0 1 1 0,94 a 47,47 0 1 1 0,-94" stroke="rgb(255,58,129)" stroke-width="6" fill-opacity="0" style="stroke-dasharray: 295.416, 295.416; stroke-dashoffset: 150;"></path>
                                                            </svg>
                                                        </div>
                                                        <div class="graph-detail">
                                                            <span>Income</span>
                                                            <h3>₹ 80000</h3>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class=" col-lg-12 col-md-12 col-sm-12 col-xs-12 float-left p-l-0 p-r-0 m-b-15">
                                    <div class="main-boxes bg-white com-box-shodow rounded">
                                        <div class="order-box-content p-10 ">
                                            <h2>78</h2>
                                            <p>Top Sales from City</p>
                                        </div>
                                        <div class="icon-new">
                                            <i class="fas fa-users"></i>
                                        </div>
                                    </div>
                                </div>
                                <div class=" col-lg-12 col-md-12 col-sm-12 col-xs-12 float-left p-l-0 p-r-0 m-b-15">
                                    <div class="main-boxes bg-white com-box-shodow rounded">
                                        <div class="order-box-content p-10 ">
                                            <h2>1000</h2>
                                            <p>Top Order receiced form the Location </p>
                                        </div>
                                        <div class="icon-new">
                                            <i class="fas fa-users"></i>
                                        </div>
                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>
                </div>
            </div>
@endsection
 
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="js/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <!-- <script src="js/popper.js"></script> -->
    <script src="js/bootstrap.min.js"></script>

    <script src="js/material.min.js"></script>
    
    <script defer src="js/solid.js"></script>
    <script defer src="js/fontawesome.js"></script>
    <script src="js/canvasjs.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#sidebarCollapse').on('click', function () {
                $('#sidebar').toggleClass('active');
                $('.main-container').toggleClass('open-menu');
            });
        });
    </script>
   <script type="text/javascript">
window.onload = function () {

var chart = new CanvasJS.Chart("chartContainer", {
    animationEnabled: true,
    title:{
        text: "Total Sales"
    },
    axisY: {
        title: "    "
    },
    legend: {
        cursor:"pointer",
        itemclick : toggleDataSeries
    },
    toolTip: {
        shared: true,
        content: toolTipFormatter
    },
    data: [{
        type: "column",
        showInLegend: true,
        name: "New Order receives",
        color: "#ff8c9d",
        dataPoints: [
            { y: 243, label: "Monday" },
            { y: 236, label: "Tuesday" },
            { y: 243, label: "Wednesday" },
            { y: 273, label: "Thursday" },
            { y: 269, label: "Friday" },
            { y: 196, label: "Saturday" },
            { y: 1118, label: "Sunday" }
        ]
    },
    {
        type: "column",
        showInLegend: true,
        name: "Item to be Delivered",
        color: "#c0ff8c",
        dataPoints: [
            { y: 212, label: "Monday" },
            { y: 186, label: "Tuesday" },
            { y: 272, label: "Wednesday" },
            { y: 299, label: "Thursday" },
            { y: 270, label: "Friday" },
            { y: 165, label: "Saturday" },
            { y: 896, label: "Sunday" }
        ]
    },
    {
        type: "column",
        showInLegend: true,
        name: "Item to be Packed",
        color: "#fff78c",
        dataPoints: [
            { y: 236, label: "Monday" },
            { y: 172, label: "Tuesday" },
            { y: 309, label: "Wednesday" },
            { y: 302, label: "Thursday" },
            { y: 285, label: "Friday" },
            { y: 188, label: "Saturday" },
            { y: 788, label: "Sunday" }
        ]
    }]
});
chart.render();

function toolTipFormatter(e) {
    var str = "";
    var total = 0 ;
    var str3;
    var str2 ;
    for (var i = 0; i < e.entries.length; i++){
        var str1 = "<span style= 'color:"+e.entries[i].dataSeries.color + "'>" + e.entries[i].dataSeries.name + "</span>: <strong>"+  e.entries[i].dataPoint.y + "</strong> <br/>" ;
        total = e.entries[i].dataPoint.y + total;
        str = str.concat(str1);
    }
    str2 = "<strong>" + e.entries[0].dataPoint.label + "</strong> <br/>";
    str3 = "<span style = 'color:Tomato'>Total: </span><strong>" + total + "</strong><br/>";
    return (str2.concat(str)).concat(str3);
}

function toggleDataSeries(e) {
    if (typeof (e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
        e.dataSeries.visible = false;
    }
    else {
        e.dataSeries.visible = true;
    }
    chart.render();
}

}

</script>




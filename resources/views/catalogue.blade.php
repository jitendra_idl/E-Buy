@extends('layouts.app')

@section('content')  
            
            <div class="main-order-section">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="bread-crum">
                                <nav class="breadcrumb">
                                  <a class="breadcrumb-item" href="index.html">Home</a>
                                  <span class="breadcrumb-item active">Catalouges</span>
                                </nav>
                            </div>
                            <div class="catalouge-list">  
                                 <div class="page-headeing">
                                    <h5>Catalouges</h5>
                                </div>  
                                <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                                  <li class="nav-item">
                                    <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true">In stock</a>
                                  </li>
                                  <li class="nav-item">
                                    <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false">Out of Stock</a>
                                  </li>
                                </ul>
                                <div class="placeorder-btn">
                                    <div class="order-btn">
                                        <a href="#add-catelouge" data-toggle="modal">Add Catalogue</a>
                                    </div>
                                </div>
                                <div class="tab-content" id="pills-tabContent">
                                  <div class="tab-pane fade show active catelouge-page" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                                     
                                      @foreach($catalogues as $catalogue)
                                      <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 float-left m-b-30">
                                        <div class="main-content-box bg-white com-box-shodow rounded float-left">
                                            <div class="main-pro-img">
                                                <img src="{{ asset('storage/'.$catalogue->media->path) }}">
                                                <div class="pro-code">
                                                    <h6>#00162</h6>
                                                </div>
                                                <div class="pro-cat-view">
                                                    <a href="{{ route('product.show', $catalogue->id) }}">View Product</a>
                                                </div>
                                            </div>
                                            <div class="pro-detail">
                                                <h5>{{ $catalogue->name }}</h5>
                                                <p>{{ $catalogue->description }}</p>
                                                <!-- <a href="#">View More</a> -->
                                            </div>
                                        </div>
                                      </div>
                                      @endforeach
                                  
                
                                  </div>
                                  <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
                                      
                                  
                                  @foreach($catalogues as $catalogue)
                                      <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 float-left m-b-30">
                                        <div class="main-content-box bg-white com-box-shodow rounded float-left">
                                            <div class="main-pro-img">
                                                <img src="{{ asset('storage/'.$catalogue->media->path) }}">
                                                <div class="pro-code">
                                                    <h6>#00162</h6>
                                                </div>
                                                <div class="pro-cat-view">
                                                    <a href="{{ route('productOutOfStock.show', $catalogue->id) }}">View Product</a>
                                                </div>
                                            </div>
                                            <div class="pro-detail">
                                                <h5>{{ $catalogue->name }}</h5>
                                                <p>{{ $catalogue->description }}</p>
                                                <!-- <a href="#">View More</a> -->
                                            </div>
                                        </div>
                                      </div>
                                      @endforeach

                                  </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

 <form method="POST" action="{{ route('catalogues.store')}}" enctype="multipart/form-data">
 {{ csrf_field() }}
      <div class="modal fade bd-example-modal-lg" id="add-catelouge" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Add Catalouge</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <div class="invoice-info add-cat">
                    <div class="add-cat-img-main">
                        <div class="add-cat-img">
                            <input type="file" name="file">
                            <i class="fas fa-plus-circle"></i>
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 float-left p-0">
                        <div class="frm-group">
                            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                <input class="mdl-textfield__input" type="text" id="sample3" name='name'>
                                <label class="mdl-textfield__label" for="sample3">Catalouge Name</label>
                              </div>
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 float-left p-0">
                        <div class="frm-group">
                              <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                <input class="mdl-textfield__input" type="text" id="sample3" name='description'>
                                <label class="mdl-textfield__label" for="sample3">Discription</label>
                              </div>
                        </div>
                    </div>
                </div>
              </div>
              <div class="modal-footer">
              
                    <button type="button" class="model-btn">Submit</button>
                
                <a href="add-product.html">
                    <button type="button" class="model-btn">Add Product</button>
                </a>
              </div>
            </div>
          </div>
        </div>
       </form>
 @endsection
  
  
  
  
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="js/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <!-- <script src="js/popper.js"></script> -->
    <script src="js/bootstrap.min.js"></script>
    <script src="js/material.min.js"></script>
    <script defer src="js/solid.js"></script>
    <script defer src="js/fontawesome.js"></script>
    <script src="js/canvasjs.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#sidebarCollapse').on('click', function () {
                $('#sidebar').toggleClass('active');
                $('.main-container').toggleClass('open-menu');
            });
        });
    </script>
   <script type="text/javascript">
window.onload = function () {

var chart = new CanvasJS.Chart("chartContainer", {
    animationEnabled: true,
    title:{
        text: "Total Sales"
    },
    axisY: {
        title: "    "
    },
    legend: {
        cursor:"pointer",
        itemclick : toggleDataSeries
    },
    toolTip: {
        shared: true,
        content: toolTipFormatter
    },
    data: [{
        type: "bar",
        showInLegend: true,
        name: "New Order receives",
        color: "gold",
        dataPoints: [
            { y: 243, label: "Monday" },
            { y: 236, label: "Tuesday" },
            { y: 243, label: "Wednesday" },
            { y: 273, label: "Thursday" },
            { y: 269, label: "Friday" },
            { y: 196, label: "Saturday" },
            { y: 1118, label: "Sunday" }
        ]
    },
    {
        type: "bar",
        showInLegend: true,
        name: "Item to be Delivered",
        color: "silver",
        dataPoints: [
            { y: 212, label: "Monday" },
            { y: 186, label: "Tuesday" },
            { y: 272, label: "Wednesday" },
            { y: 299, label: "Thursday" },
            { y: 270, label: "Friday" },
            { y: 165, label: "Saturday" },
            { y: 896, label: "Sunday" }
        ]
    },
    {
        type: "bar",
        showInLegend: true,
        name: "Item to be Packed",
        color: "#A57164",
        dataPoints: [
            { y: 236, label: "Monday" },
            { y: 172, label: "Tuesday" },
            { y: 309, label: "Wednesday" },
            { y: 302, label: "Thursday" },
            { y: 285, label: "Friday" },
            { y: 188, label: "Saturday" },
            { y: 788, label: "Sunday" }
        ]
    }]
});
chart.render();

function toolTipFormatter(e) {
    var str = "";
    var total = 0 ;
    var str3;
    var str2 ;
    for (var i = 0; i < e.entries.length; i++){
        var str1 = "<span style= 'color:"+e.entries[i].dataSeries.color + "'>" + e.entries[i].dataSeries.name + "</span>: <strong>"+  e.entries[i].dataPoint.y + "</strong> <br/>" ;
        total = e.entries[i].dataPoint.y + total;
        str = str.concat(str1);
    }
    str2 = "<strong>" + e.entries[0].dataPoint.label + "</strong> <br/>";
    str3 = "<span style = 'color:Tomato'>Total: </span><strong>" + total + "</strong><br/>";
    return (str2.concat(str)).concat(str3);
}

function toggleDataSeries(e) {
    if (typeof (e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
        e.dataSeries.visible = false;
    }
    else {
        e.dataSeries.visible = true;
    }
    chart.render();
}

}
</script>
  </body>
</html>

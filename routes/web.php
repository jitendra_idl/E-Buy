<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::view('/', 'auth.login');

Auth::routes();

Route::group(['middleware' => 'auth'], function () {


Route::get('/home', 'Dashboard\DashboardController@dashboardData')->name('home');

Route::get('order', 'HomeController@order')->name('order');

Route::get('product/in-stock/{id}', 'Products\ProductsController@getCatelogRealtedInStockProducts')->name('product.show');
Route::get('product/out-of-stock/{id}', 'Products\ProductsController@getCatelogRealtedOutOfStockProducts')->name('productOutOfStock.show');


Route::get('settings', 'Catalogues\CataloguesController@index')->name('settings');
Route::get('catalogue', 'Catalogues\CataloguesController@index')->name('catalogue');
Route::resource('catalogues', 'Catalogues\CataloguesController');



Route::get('venderList', 'HomeController@venderList')->name('venderList');
Route::get('venderList2', 'HomeController@venderList2')->name('venderList2');



Route::get('invoiceList', 'HomeController@invoiceList')->name('invoiceList');

});





<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => 'cors'], function () {
    
        Route::post('user/register', 'APIRegisterController@register');
        //Admin Login
        Route::post('admin/login', 'APILoginController@login')->name('adminLogin');
        //User Login 
        Route::post('user/login', 'APILoginController@login')->name('userLogin');
    
    
    Route::group(['middleware' => ['jwt.auth', 'admin']], function () {
        //Admin Route

        //Products
        Route::resource('products', 'Products\ProductsController');

        //Organizations
        Route::resource('organizations', 'Organizations\OrganizationsController');

        //Catalogues
        Route::resource('catalogues', 'Catalogues\CataloguesController');

        //Orders
        Route::get('orders/current-date', 'Orders\OrdersController@currentDateOrders');
        Route::get('orders/tommorrow', 'Orders\OrdersController@tommorowsDateOrders');
        Route::get('orders/yesterday', 'Orders\OrdersController@yesterdaysDateOrders');
        Route::get('orders/lastSevenDays', 'Orders\OrdersController@lastSevenDaysOrders');
        Route::get('orders/lastFifteenDays', 'Orders\OrdersController@lastFifteenDaysOrders');
        Route::get('orders/lastThirteenDays', 'Orders\OrdersController@lastThirteenDaysOrders');
        Route::post('orders/status', 'Orders\OrdersController@getOrders');
        Route::resource('orders', 'Orders\OrdersController');

        //Dashboard
        Route::get('dashboard/cancel-orders', 'Dashboard\DashboardController@getCancelOrder');   
        Route::get('dashboard/sold-orders', 'Dashboard\DashboardController@getSoldOrder');                                     
        Route::get('dashboard/new-order-received', 'Dashboard\DashboardController@newOrderReceived');        
        Route::get('dashboard', 'Dashboard\DashboardController@dashboardData');
        
        //Customers
        Route::resource('customers', 'Customers\CustomersController');

        //Customer Addresses        
        Route::resource('customer-addreses', 'CustomerAddresses\CustomerAddressesController');

        //Pricings
        Route::resource('pricings', 'Pricings\PricingsController');

        //Lookups
        Route::resource('types', 'Lookups\TypesController');
        Route::get('lookups/get-lookups', 'Lookups\LookupsController@getLookups');
        Route::resource('lookups', 'Lookups\LookupsController');
        Route::post('statuses/change-status', 'OrderStatus\OrderStatusController@store');
        Route::get('statuses/get-status', 'OrderStatus\OrderStatusController@getAllStatuses');

        //Cart
        Route::get('carts/allProduct', 'Carts\CartsController@getAllProducts');
        Route::resource('carts', 'Carts\CartsController');

        //DeliveryBioys
        Route::get('deliveryboys/all-delivery-boys', 'DeliveryBoys\DeliveryBoysController@getAllDeliveryBoys');
        Route::resource('delivery-boys', 'DeliveryBoys\DeliveryBoysController');

        //Brand
        Route::resource('brands', 'Brands\BrandsController');        

        //categories
        Route::resource('categories', 'Categories\CategoriesController');

        //Invoices
        Route::resource('invoice', 'Invoices\InvoicesController');

        //Vender
        Route::resource('vender', 'Venders\VendersController');
        


    });

});


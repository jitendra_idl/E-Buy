<?php

namespace App\CustomerAddresses;

use Illuminate\Database\Eloquent\Model;
use App\BaseModel;

class CustomerAddress extends BaseModel
{
    protected $fillable = [
        'uuid',
        'customer_id', 
        'street_1',
        'street_2', 
        'country', 
        'state', 
        'city',
        'pincode', 
        'mobile', 
    ];

    public function customer()
    {
        $this->belongsTo(self::class, 'customer_id');
    }

}

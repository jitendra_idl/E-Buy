<?php

namespace App\Lookups;

use App\BaseModel;

class Type extends BaseModel
{
    protected $fillable = [
        'name', 'alias',
    ];

    public function lookups()
    {
        return $this->hasMany(Lookup::class);
    }
}

<?php

namespace App\Lookups;

use App\BaseModel;

class Lookup extends BaseModel
{
    protected $fillable = [
        'type_id', 'name',
 ];

    public function type()
    {
        return $this->belongsTo(Type::class, 'type_id');
    }
}

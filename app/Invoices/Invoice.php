<?php

namespace App\Invoices;

use Illuminate\Database\Eloquent\Model;
use App\BaseModel;

class Invoice extends BaseModel
{
    protected $table = 'invoice';
    
    protected $fillable = [
        'uuid',
        'customer_id',
        'invoice_no',
        'invoice_date',
        'payment_due',
        'sub_total',
        'total',
        'notes',
    ];
}

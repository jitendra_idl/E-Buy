<?php

namespace App\Orders;

use Illuminate\Database\Eloquent\Model;
use App\BaseModel;
use App\OrderStatus\OrderStatus;

class Order extends BaseModel
{
    protected $fillable = [
        'uuid',
        'customer_id',
        'order_no',
        'order_delivery_id',
        'order_date',
        'delivery_date',
        'payment_date',
    ];

    public function orderStatus()
    {
        return $this->hasMany(OrderStatus::class);
    }

}

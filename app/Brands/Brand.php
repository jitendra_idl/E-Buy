<?php

namespace App\Brands;

use Illuminate\Database\Eloquent\Model;
use App\BaseModel;
use App\Media\Media;

class Brand extends BaseModel
{
    protected $fillable =[
        'name',
        'descriptions',
        'media_id',
        'uuid'
    ];

    public function media()
    {
        return $this->belongsTo(Media::class, 'media_id');
    }
}

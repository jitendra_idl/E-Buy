<?php

namespace App\DeliveryBoys;

use Illuminate\Database\Eloquent\Model;
use App\BaseModel;

class DeliveryBoy extends BaseModel
{
    protected $fillable = [
        'uuid',
        'name', 
        'mobile_no', 
        'email', 
        'identity_no',  
        'address'
    ];

}

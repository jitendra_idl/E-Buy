<?php

namespace App\Media;

use Illuminate\Database\Eloquent\Model;
use App\BaseModel;


class Media extends BaseModel
{
    protected $table = 'media';

    protected $fillable =
     [
         'media_name', 
         'mime_type', 
         'path', 
         'original_url', 
         'thumbnail_url', 
         'media_title'
    ];

}

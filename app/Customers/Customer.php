<?php

namespace App\Customers;

use Illuminate\Database\Eloquent\Model;
use App\BaseModel;
use App\User;

class Customer extends BaseModel
{
    protected $fillable = [
        'uuid',
        'user_id',
        'device_token',
    ];

    public function user()
    {
        $this->belongsTo(User::class, 'user_id');
    }

    public function addresses()
    {
        $this->hasMany(self::class);
    }
}

<?php

namespace App;

use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\QueryException;

class BaseModel extends Model
{
    /**
     * The attributes that are guarded.
     * @var array
     */
    protected $guarded = [];

    protected $dateFormat = 'Y-m-d';

    /**
     * The attributes that should be hidden for arrays.
     * @var array
     */
    protected $hidden = ['creator_id', 'updater_id', 'created_at', 'updated_at', 'deleted_at'];

    /**
     * Method to perform update of any record.
     * @param  $data      Input data
     * @param  $attribute Column name
     * @param  $value     Column value
     * @return array
     */
    public function doUpdate($data, $attribute, $value)
    {
        $response = [];
        $modelName = class_basename($this);
        $dataCollection = $this->where($attribute, $value)->first();
        if ($dataCollection) {
            $dataCollection->fill($data);

            try {
                $dataCollection->save();
            } catch (QueryException $e) {
                $response['error'] = "Problem in updating details of {$modelName}.";

                return $response;
            }
        } else {
            $response['error'] = "{$modelName} Details not found.";

            return $response;
        }
        $response['success'] = "{$modelName} Details updated successfully.";

        return $response;
    }

    /**
     * Method to delete any record.
     * @param  $id Record id
     * @return array
     */
    public function doDelete($id)
    {
        $response = [];
        $modelName = class_basename($this);

        $dataCollection = $this->where('id', $id)->first();

        if ($dataCollection) {
            $dataCollection->delete();
        } else {
            $response['error'] = "{$modelName} Details not found.";

            return $response;
        }

        $response['success'] = "{$modelName} Details deleted successfully.";

        return $response;
    }

    /**
     * Method to generate paginated data.
     * @param  $perPage
     * @param  $column
     * @param  $pageNo
     * @return mixed
     */
    public function doPaginate($perPage, $column, $pageNo)
    {
        $status = strstr($column, '-');
        $perPage = ($perPage) ? $perPage : 10;
        $column = ($column) ? $column : 'id';

        if ($status) {
            $sort = 'desc';
            $column = substr($status, 1);
            $dataCollection = $this->orderBy($column, $sort);
        } else {
            $sort = 'asc';
            $dataCollection = $this->orderBy($column, $sort);
        }

        $data = $dataCollection->paginate($perPage);

        return $data;
    }

    /**
     * Create and Update Default Creator_id and updater_id.
     */
    public static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            $user = Auth::user();
            if (!$user) {
                $model->creator_id = 1;
                $model->updater_id = 1;
            } else {
                $model->creator_id = $user->id;
                $model->updater_id = $user->id;
            }
        });

        static::updating(function ($model) {
            $user = Auth::user();
            if (!$user) {
                $model->updater_id = 1;
            } else {
                $model->updater_id = $user->id;
            }
        });
    }
}

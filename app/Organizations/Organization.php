<?php

namespace App\Organizations;

use Illuminate\Database\Eloquent\Model;
use App\BaseModel;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Route;



class Organization extends BaseModel
{
    protected $table = 'organizations';

    protected $fillble = [
        'uuid',
        'user_id',
        'name',
        'address',
        'city',
        'state',
        'country',
        'postal_code',
        'tin_no',
        'gstin_n0',
        'cin_no',
        'contact_name',
        'phone_no',
        'website',
    ];

    public function scopeSaveData($query, $data)
    {

        if(Route::getCurrentRoute()->uri == 'api/vender'){

            $data = json_decode($data,true);
        }
        
       return  $query->create([
            'uuid' => (string) Str::uuid(),
            'user_id' => Auth::user()->id,
            'name' => $data['name'],
            'city' => $data['city'],
            'state' => $data['state'],
            'country' => $data['country'],
            'postal_code' => $data['postal_code'],
            'tin_no' => $data['tin_no'],
            'gstin_n0' => $data['gstin_n0'],
            'cin_no' => $data['cin_no'],
            'phone_no' => $data['phone_no'],
            'website' => $data['website'],
        ]);
    }
    
}

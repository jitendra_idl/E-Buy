<?php

/**
 * Get Sorted data for index pages.
 *
 * @param  mixed  $column
 * @param  mixed  $model
 * @return $model
 */
function sortData($column, $model)
{
    $status = strstr($column, '-');

    if ($status) {
        $sort = 'desc';
        $column = substr($status, 1);
        $model->orderBy($column, $sort);
    } else {
        $sort = 'asc';
        $model->orderBy($column, $sort);
    }

    return $model;
}

function paginateAndSort($model, $perPage = 10, $column = 'id', $page = 1)
{
    $perPage = ($perPage) ? $perPage : 10;
    $column = ($column) ? $column : 'id';
    $sortedData = sortData($column, $model);
    $data = $sortedData->paginate($perPage);
    if (empty($data->toArray()['data'])) {
        return ['data' => []];
    } else {
        return $data;
    }
}

function uploadFile($path, $file, $storageName)
{
    $result = [];
    $extension = $file->getClientOriginalExtension();
    $fileName = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
    $fileName = cleanString($fileName);
    $finalFileName = $fileName . '.' . $extension;
    Storage::disk($storageName)->put($finalFileName, File::get($file));
    $exists = Storage::disk($storageName)->exists($finalFileName);
    if ($exists) {
        $result['success'] = 'File has been uploaded successfully.';
    } else {
        $result['error'] = 'File upload failed.';
    }
    $result['fileName'] = $fileName . '.' . $extension;
    $result['mime'] = $file->getClientMimeType();
    $result['fileExtension'] = $extension;
    $result['slug'] = str_slug(str_random(10));
    $result['fileLocation'] = $path;

    return $result;
}

/**
 * Format Date.
 *
 * @param  mixed         $date
 * @param  mixed         $dateFormat
 * @return $current_date
 */
function formatDate($date, $dateFormat = 'd/m/Y')
{
    $date = \Carbon\Carbon::parse($date);
    $date = $date->format($dateFormat);

    return $date;
}

// get current date
function getCurrentDate()
{
    $todayDate = \Carbon\Carbon::parse(\Carbon\Carbon::now());

    return $todayDate;
}

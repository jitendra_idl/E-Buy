<?php

function jsonMessage($message, $status_code = 200)
{
    return Response::json(['description' => $message], $status_code);
}

function jsonData($data, $status_code = 200)
{
    return Response::json($data, $status_code);
}

function cleanString($string)
{
    $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
    return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
}

function formatDateToW3cString($date)
{
    if ($date !== null) {
        $date = \Carbon\Carbon::parse($date);

        return $date->toW3cString();
    } else {
        return;
    }
}

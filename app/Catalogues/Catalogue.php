<?php

namespace App\Catalogues;

use Illuminate\Database\Eloquent\Model;
use App\BaseModel;
use Illuminate\Support\Facades\Storage;
use App\Media\Media;

class Catalogue extends BaseModel
{
    protected $fillable = [
        'uuid',
        'name',
        'description',
        'file_name',
        'media_id'
    ];

    public function scopeSaveData($query, $data)
    {
        
        // $files = $data->file('file');

        // $folder = public_path('../public/storage/' . 'catelogues'. '/');
    
        // if (!Storage::exists($folder)) {
        //     Storage::makeDirectory($folder, 0775, true, true);
        // }
    
        // if (!empty($files)) {
        //     foreach($files as $file) {
        //         Storage::disk(['drivers' => 'local', 'root' => $folder])->put($file->getClientOriginalName(), file_get_contents($file));
        //     }
        // }

        $customer = \Auth::user()->customer()->first();
        $result = [];
        $path = 'temp/' . $customer->id . '/';
        $file = $data['file'];
        $result = uploadFile($path, $file, 'catelogues');

        // $query->create([
        //     'uuid' => (string) Str::Uuid(),
        //     'product_details_id' => $product->id,
        //     'file_name' => $result['fileName'],
        //     'slug' => $result['slug'],
        //     'file_location' => $result['fileLocation']
        // ]);
    }

    public function media()
    {
        return $this->belongsTo(Media::class, 'media_id');
    }
}

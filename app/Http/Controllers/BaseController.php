<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use App\Http\Controllers\Media\MediaPhoto;
use Illuminate\Routing\Controller;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use App\Media\Media as UploadMedia;
use Carbon\Carbon;

class BaseController extends Controller
{
    protected $user;

    protected $customer;

    public function __construct()
    {
        $this->middleware(function ($request, $next) {

            $this->user = Auth::user();
            if ($this->user) {
                $this->customer = Auth::user()->customer()->first();
            }
            return $next($request);
        });
    }

    public function getUuid()
    {
        return (string) Str::uuid();
    }

    public function saveMediaFile(
        UploadedFile $file,
        $id,
        $rootFolder,
        $sub_folder = null,
        $tb_width = 200,
        $tb_height = 200,
        $max_size = null,
        $media_alt
    ) {
        
        $media_attributes = (new MediaPhoto())->save($file, $id, $rootFolder, $sub_folder, $tb_width, $tb_height, $max_size);
        $media_attributes['media_alt'] = $media_alt;
        return UploadMedia::create($media_attributes);
    }
}

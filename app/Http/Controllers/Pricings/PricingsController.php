<?php

namespace App\Http\Controllers\Pricings;

use Illuminate\Http\Request;
use App\Http\Controllers\BaseController;
use App\Pricings\Pricing;

class PricingsController extends BaseController
{
    protected $model;
     
    /**
     * pricing model.
     */
    public function __construct(Pricing $pricing)
    {
        $this->model = $pricing;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $pricings = $this->model->all();

       return jsonData($pricings);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       request()->request->add(['uuid' => $this->getUuid()]);
       $pricing = $this->model->create(request()->all());

        if (!$pricing) {
            return jsonMessage('Pricing Details are not found', 422);
        } else {
            return jsonMessage('Pricing details are stored successfully.', 201);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $uuid
     * @return \Illuminate\Http\Response
     */
    public function show($uuid)
    {
        $pricing = $this->model->whereUuid($uuid)->first();

        if(!$pricing) {
            return jsonMessage('Pricing Details are not found', 422);            
        }
        return jsonData($pricing);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $uuid
     * @return \Illuminate\Http\Response
     */
    public function edit($uuid)
    {
        $pricing = $this->model->whereUuid($uuid)->first();

        if(!$pricing) {
            return jsonMessage('Pricing Details are not found', 422);            
        }
        return jsonData($pricing);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $uuid
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $uuid)
    {
        $pricing = $this->model->whereUuid($uuid)->first();

        if(!$pricing) {
            return jsonMessage('Pricing Details are not found', 422);            
        }

        $pricing->fill(request()->all())->save();

        return jsonMessage('Pricing Details updated successfully.', 201);            
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $uuid
     * @return \Illuminate\Http\Response
     */
    public function destroy($uuid)
    {
        $pricing = $this->model->whereUuid($uuid)->first();

        if(!$pricing) {
            return jsonMessage('Pricing Details are not found', 422);            
        }

        $pricing->delete();

        return jsonMessage('Pricing Details deleted successfully.', 200);            
    }
}

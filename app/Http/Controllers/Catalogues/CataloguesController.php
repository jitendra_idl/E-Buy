<?php

namespace App\Http\Controllers\Catalogues;

use Illuminate\Http\Request;
use App\Http\Controllers\BaseController;
use App\Catalogues\Catalogue;
use Illuminate\Support\Facades\Route;
use App\Brands\Brand;

class CataloguesController extends BaseController
{
    protected $model;
     
    /**
     * catalogue model.
     */
    public function __construct(Catalogue $catalogue)
    {
        $this->model = $catalogue;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

       $catalogues = $this->model->with('media')->get();

       if(!$request->ajax() &&  Route::getCurrentRoute()->uri <> 'settings')
       {
          return view('catalogue', compact('catalogues'));  
       } 

       if(!$request->ajax() && Route::getCurrentRoute()->uri === 'settings')
       { 
          $brands = Brand::with('media')->get();

          return view('settings', compact('catalogues','brands'));
       } 

       return jsonData($catalogues);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (request()->has('file')) {
            $media = $this->saveMediaFile(
            request()->file,
            request()->id,
            config('vendor.catelogues.directory'),
            request()->name,
            config('vendor.catelogues.thumbnails.width', 200),
            config('vendor.catelogues.thumbnails.height', 200),
            config('vendor.catelogues.max_size', null),
            request()->media_alt
        );
        }

        request()->request->add(['uuid' => $this->getUuid(), 'media_id' => $media->id]);

        $catalogue = $this->model->create(request()->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $uuid
     * @return \Illuminate\Http\Response
     */
    public function show($uuid)
    {
        $catalogue = $this->model->whereUuid($uuid)->first();

        if(!$catalogue) {
            return jsonMessage('Catalogue Details are not found', 422);            
        }
        return jsonData($catalogue);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $uuid
     * @return \Illuminate\Http\Response
     */
    public function edit($uuid)
    {
        $catalogue = $this->model->whereUuid($uuid)->first();

        if(!$catalogue) {
            return jsonMessage('Catalogue Details are not found', 422);            
        }
        return jsonData($catalogue);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $uuid
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $uuid)
    {
        $catalogue = $this->model->whereUuid($uuid)->first();

        if(!$catalogue) {
            return jsonMessage('Catalogue Details are not found', 422);            
        }

        $catalogue->fill(request()->all())->save();

        return jsonMessage('Catalogue Details updated successfully.', 201);            
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $uuid
     * @return \Illuminate\Http\Response
     */
    public function destroy($uuid)
    {
        $catalogue = $this->model->whereUuid($uuid)->first();

        if(!$catalogue) {
            return jsonMessage('Catalogue Details are not found', 422);            
        }

        $catalogue->delete();

        return jsonMessage('Catalogue Details deleted successfully.', 200);            
    }
}

<?php

namespace App\Http\Controllers\Organizations;

use Illuminate\Http\Request;
use App\Http\Controllers\BaseController;
use App\Organizations\Organization;

class OrganizationsController extends BaseController
{

    protected $model;
      /**
     * Organization model.
     */
    public function __construct(Organization $organization)
    {
        $this->model = $organization;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $organizations = $this->model->all();

       return jsonData($organizations);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $organization = $this->model->saveData(request()->all());

        if (!$organization) {
            return jsonMessage('Organization Details are not found', 422);
        } else {
            return jsonMessage('Organization details are stored successfully.', 201);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $uuid
     * @return \Illuminate\Http\Response
     */
    public function show($uuid)
    {
        $organization = $this->model->whereUuid($uuid)->first();

        if(!$organization) {
            return jsonMessage('Organization Details are not found', 422);            
        }
        return jsonData($organization);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $uuid
     * @return \Illuminate\Http\Response
     */
    public function edit($uuid)
    {
        $organization = $this->model->whereUuid($uuid)->first();

        if(!$organization) {
            return jsonMessage('Organization Details are not found', 422);            
        }
        return jsonData($organization);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $uuid
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $uuid)
    {
        $organization = $this->model->whereUuid($uuid)->first();

        if(!$organization) {
            return jsonMessage('Organization Details are not found', 422);            
        }

        $organization->fill(request()->all())->save();

        return jsonMessage('Organization Details updated successfully.', 201);            
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $uuid
     * @return \Illuminate\Http\Response
     */
    public function destroy($uuid)
    {
        $organization = $this->model->whereUuid($uuid)->first();

        if(!$organization) {
            return jsonMessage('Organization Details are not found', 422);            
        }

        $organization->delete();

        return jsonMessage('Organization Details deleted successfully.', 200);            
    }
}

<?php

namespace App\Http\Controllers\Media;

use Illuminate\Http\UploadedFile;

/**
 * Interface for dealing with Media files.
 */
interface MediaInterface
{
    public function save(
        UploadedFile $file,
        $id,
        $rootFolder,
        $config_folder = null
    );
}

<?php

namespace App\Http\Controllers\Media;

//use App\Http\Controllers\Media;
use Exception;
use File;
use Image;
use Storage;
use Illuminate\Http\UploadedFile;
use App\Modules\Media\Media;

class MediaPhoto extends MediaHandler
{
    /**
     * Instance of Media Model.
     *
     * @var Media
     */
    private $media;

    /**
     * ID of the related node.
     *
     * @var string
     */
    private $id;

    /**
     * Default folder of the image according to the configuration file.
     *
     * @var string
     */
    private $photoFolder = 'images';

    /**
     * Default folder of the document according to the configuration file.
     *
     * @var string
     */
    private $documentFolder = 'documents';

    /**
     * Unique Image Name with extension.
     *
     * @var string
     */
    private $path;

    /**
     * @var string
     */
    private $originalPath;

    /**
     * return folder name where file store without file name.
     *
     * @var string
     */
    private $absolute_path;

    /**
     * return final file name.
     */

    /**
     * Thumbnail Path.
     *
     * @var string
     */
    private $thumbnailPath;

    /**
     * Uploaded File.
     *
     * @var UploadedFile
     */
    private $file;

    /**
     * @param UploadedFile $file
     * @param $id
     * @param null $config_folder
     * @param int  $thumb_width
     * @param int  $thumb_height
     * @param null $max_size
     *
     * @return array
     *
     * @throws Exception
     */
    public function save(
        UploadedFile $file,
        $id,
        $rootFolder,
        $sub_folder = null,
        $thumb_width = 200,
        $thumb_height = 200,
        $max_size = null
    ) {
        $this->file = $file;
        $this->id = $id;
        $this->photoFolder = (empty($sub_folder)) ? '' : 'images'.DIRECTORY_SEPARATOR.$rootFolder.DIRECTORY_SEPARATOR.$sub_folder;
        $this->setPaths();

        $this->moveOriginal();

        $this->moveFile($max_size);
        $this->thumbnail($thumb_width, $thumb_height);
        $properties = [
            'media_name' => preg_replace('/\..+$/', '', $this->file->getClientOriginalName()),
            'mime_type' => $file->getMimeType(),
            'absolute_path' => $this->absolute_path,
            'original_path' => $this->originalPath,
            'original_url' => str_replace(DIRECTORY_SEPARATOR, '/', $this->originalPath),
            'path' => $this->path,
            'url' => str_replace(DIRECTORY_SEPARATOR, '/', $this->path),
            'storage' => 'filesystem',
            'thumbnail_path' => $this->thumbnailPath,
            'thumbnail_url' => str_replace(DIRECTORY_SEPARATOR, '/', $this->thumbnailPath),
        ];
        return $properties;
    }

    /**
     * @param UploadedFile $file
     * @param Media        $media
     * @param $id
     * @param null $config_folder
     * @param int  $thumb_width
     * @param int  $thumb_height
     * @param null $max_size
     *
     * @return array
     *
     * @throws Exception
     */
    public function update(
        UploadedFile $file,
        $media,
        $id,
        $rootFolder,
        $sub_folder = null,
        $thumb_width = 200,
        $thumb_height = 200,
        $max_size = null
    ) {
        // if (isset($media->media)) {
        //     $this->delete($media);
        // }
        
        return $this->save($file, $id, $rootFolder, $sub_folder, $thumb_width, $thumb_height, $max_size);
    }

    /**
     * Delete Existing image and its thumbnail.
     *
     * @param Media $media
     *
     * @return bool
     */
    public function delete($media)
    {
        $original_path = $media->media->original_url;
        $thumbnail_url = $media->media->thumbnail_url;
        $path = $media->media->path;

        if (Storage::disk('public')->exists($original_path)) {
            Storage::disk('public')->delete($original_path);
            Storage::delete($original_path);
        }

        if (Storage::disk('public')->exists($thumbnail_url)) {
            Storage::disk('public')->delete($thumbnail_url);
            Storage::delete($thumbnail_url);
        }
        if (Storage::disk('public')->exists($path)) {
            Storage::disk('public')->delete($path);
            Storage::delete($path);
        }

        return true;
    }

    /**
     * @param Media $media
     * @param $value
     *
     * @return Media
     *
     * @throws Exception
     */
    public function rotate($media, $value)
    {
        try {
            $value = $this->rotationValue($value);

            $image = (string) Image::make(public_path($media->path))->rotate($value)->encode();
            $thumbnail = (string) Image::make(public_path($media->thumbnail_path))->rotate($value)->encode();
            $this->delete($media);

            $media = $this->newMediaNames($media);

            if (!Storage::disk('public')->put($media->path, $image) || !Storage::disk('public')->put($media->thumbnail_path, $thumbnail)) {
                throw new Exception('Cannot Upload the Image', 500);
            }
        } catch (\Exception $e) {
        }

        return $media;
    }

    /**
     * @param $media
     *
     * @return Media
     */
    public function newMediaNames($media)
    {
        $pathInfo = pathinfo($media->path);
        $tn_pathInfo = pathinfo($media->thumbnail_path);

        $media->path = $pathInfo['dirname'].DIRECTORY_SEPARATOR.
            $this->generateUniqueName($pathInfo['dirname'].DIRECTORY_SEPARATOR, $pathInfo['extension']).
            '.'.$pathInfo['extension'];
        $media->thumbnail_path = $tn_pathInfo['dirname'].DIRECTORY_SEPARATOR.
            $this->generateUniqueName($tn_pathInfo['dirname'].DIRECTORY_SEPARATOR, $tn_pathInfo['extension']).
            '.'.$tn_pathInfo['extension'];
        $media->url = str_replace(DIRECTORY_SEPARATOR, '/', $media->path);
        $media->thumbnail_url = str_replace(DIRECTORY_SEPARATOR, '/', $media->thumbnail_path);

        $media->save();

        return $media;
    }

    /**
     * @param $path
     *
     * @return string
     *
     * @throws Exception
     */
    public function getExtension($path)
    {
        preg_match('/.*\.([a-z]{3,5})$/', $path, $matches);

        if (isset($matches[1])) {
            return $matches[1];
        }

        throw new \Exception('The path does not have extension');
    }

    /**
     * @param $path
     * @param $value
     *
     * @return bool
     *
     * @throws Exception
     */
    public function rotatePath($path, $value)
    {
        if (Storage::disk('public')->exists($path)) {
            try {
                $image = (string) Image::make($path)->rotate($this->rotationValue($value))->encode();
            } catch (\Exception $e) {
                return false;
            }
            Storage::disk('public')->delete($path);
            if (!Storage::disk('public')->put($path, $image)) {
                throw new Exception('Cannot Upload the Image', 500);
            }

            return $path;
        }

        return false;
    }

    /**
     * @param Media $media
     * @param int   $width
     * @param int   $height
     * @param int   $tb_width
     * @param int   $tb_height
     *
     * @return bool
     *
     * @throws Exception
     */
    public function resize(Media $media, $width = 200, $height = 200, $tb_width = 100, $tb_height = 100)
    {
        $pathInfo = pathinfo($media->path);
        $resizeFileName = $pathInfo['dirname'].DIRECTORY_SEPARATOR.'_'.$width.'x'.$height.'_'.$pathInfo['basename'];
        $tbResizeFileName = $pathInfo['dirname'].DIRECTORY_SEPARATOR.'tb_'.$tb_width.'x'.$tb_height.'_'.$pathInfo['basename'];

        if (\Storage::exists($resizeFileName) && !\Storage::exists($media->path)) {
            return false;
        }
        try {
            $imageStream = (string) Image::make(public_path($media->path))
                ->resize($width, $height, function ($constraint) {
                    $constraint->aspectRatio();
                })->encode();
            $imageStream->resizeCanvas(200, 200, 'center', false, array(255, 255, 255, 0));
        } catch (\Exception $e) {
            return false;
        }

        if (!Storage::disk('public')->put($resizeFileName, $imageStream)) {
            throw new Exception('Cannot Create the resized image', 500);
        }

        $this->thumbnail($tb_width, $tb_height, $tbResizeFileName, $media->original_path);

        if (\Storage::exists($media->path)) {
            \Storage::delete($media->path);
        }
        $media->path = $resizeFileName;
        //$media->url = str_replace(DIRECTORY_SEPARATOR, '/', $resizeFileName);
        $media->thumbnail_url = $tbResizeFileName;
        // $media->thumbnail_url = str_replace(DIRECTORY_SEPARATOR, '/', $tbResizeFileName);
        $media->save();

        return true;
    }

    /**
     * @param $value
     *
     * @return int
     */
    protected function rotationValue($value)
    {
        if (!is_numeric($value) && is_string($value)) {
            switch ($value) {
                case 'right':
                    $value = 270;
                    break;
                case 'left':
                    $value = 90;
                    break;
                case 'turn':
                case 'roll':
                    $value = 180;
                    break;
            }
        }

        return $value;
    }

    /**
     * Set the path and thumbnailPath of the image.
     */
    private function setPaths()
    {
        $absolute_path = ($this->id == null) ? $this->photoFolder : $this->photoFolder.DIRECTORY_SEPARATOR.$this->id;
        $file_name = $this->generateUniqueName($absolute_path.DIRECTORY_SEPARATOR, $this->file->extension());
        $file_extension = $this->file->extension();
        $this->path = $absolute_path.DIRECTORY_SEPARATOR.$file_name.'.'.$file_extension;
        $this->thumbnailPath = $absolute_path.DIRECTORY_SEPARATOR.'tn-'.$file_name.'.'.$file_extension;
        $this->originalPath = $absolute_path.DIRECTORY_SEPARATOR.'original-'.$file_name.'.'.$file_extension;

        return true;
    }

    /**
     * @param $max_size
     *
     * @return bool
     *
     * @throws Exception
     */
    private function moveFile($max_size)
    {
        try {
            $stream = (is_integer($max_size)) ? $this->resizeFile($max_size) : File::get($this->file);
        } catch (\Exception $e) {
            $stream = File::get($this->file);
        }

        if (!Storage::disk('public')->put($this->path, $stream)) {
            throw new Exception('Cannot Upload the Image', 500);
        }

        return true;
    }

    /**
     * @return bool
     *
     * @throws Exception
     */
    private function moveOriginal()
    {
        if (!Storage::disk('public')->put($this->originalPath, File::get($this->file))) {
            throw new Exception('Cannot Upload the Image', 500);
        }

        return true;
    }

    /**
     * @param $max_size
     *
     * @return string
     */
    protected function resizeFile($max_size)
    {
        try {
            return (string) Image::make($this->file)
                ->resize($max_size, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->encode();
        } catch (\Exception $e) {
            return File::get($this->file);
        }
    }

    /**
     * @param $width
     * @param $height
     * @param null $thumbnail_path
     * @param null $file
     *
     * @return bool
     *
     * @throws Exception
     */
    private function thumbnail($width, $height, $thumbnail_path = null, $file = null)
    {
        try {
            $thumbnail = (string) Image::make($file ?: $this->file)
                ->resize((int) $width, (int) $height, function ($constraint) {
                    $constraint->aspectRatio();
                })->encode();
        } catch (\Exception $e) {
            $thumbnail = File::get($file ?: $this->file);
        }

        if (!Storage::disk('public')->put($thumbnail_path ?: $this->thumbnailPath, $thumbnail)) {
            throw new Exception('Cannot Create the Thumbnail', 500);
        }

        return true;
    }
}

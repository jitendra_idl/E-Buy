<?php

namespace App\Http\Controllers\Media;

use Illuminate\Http\UploadedFile;
use Illuminate\Support\Str;

class MediaHandler implements MediaInterface
{
    /**
     * Main storage folder.
     *
     * @var string
     */
    protected $storageFolder = 'public';

    /**
     * @param $folderPath
     * @param $extension
     *
     * @return string
     */
    public function generateUniqueName($folderPath, $extension)
    {
        $uniqueName = Str::random(50);
        $path = $folderPath.$uniqueName.'.'.$extension;

        if (!\Storage::exists($path)) {
            return $uniqueName;
        }

        return $this->generateUniqueName($folderPath, $extension);
    }

    public function save(UploadedFile $file, $id, $rootFolder, $config_folder = null)
    {
    }
}

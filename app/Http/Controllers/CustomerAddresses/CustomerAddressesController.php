<?php

namespace App\Http\Controllers\CustomerAddresses;

use Illuminate\Http\Request;
use App\Http\Controllers\BaseController;
use App\CustomerAddresses\CustomerAddress;

class CustomerAddressesController extends BaseController
{
    protected $model;
     
    /**
     * customerAddress model.
     */
    public function __construct(CustomerAddress $customerAddress)
    {
        parent::__construct();
        $this->model = $customerAddress;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $customerAddresss = $this->model->all();

       return jsonData($customerAddresss);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        dd(\Auth::user()->customer());
        
       request()->request->add(['uuid' => $this->getUuid()]);
       $customerAddress = $this->model->create(request()->all());

        if (!$customerAddress) {
            return jsonMessage('Customer Address Details are not found', 422);
        } else {
            return jsonMessage('Customer Address details are stored successfully.', 201);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $uuid
     * @return \Illuminate\Http\Response
     */
    public function show($uuid)
    {
        $customerAddress = $this->model->whereUuid($uuid)->first();

        if(!$customerAddress) {
            return jsonMessage('Customer Address Details are not found', 422);            
        }
        return jsonData($customerAddress);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $uuid
     * @return \Illuminate\Http\Response
     */
    public function edit($uuid)
    {
        $customerAddress = $this->model->whereUuid($uuid)->first();

        if(!$customerAddress) {
            return jsonMessage('Customer Address Details are not found', 422);            
        }
        return jsonData($customerAddress);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $uuid
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $uuid)
    {
        $customerAddress = $this->model->whereUuid($uuid)->first();

        if(!$customerAddress) {
            return jsonMessage('Customer Address Details are not found', 422);            
        }

        $customerAddress->fill(request()->all())->save();

        return jsonMessage('Customer Address Details updated successfully.', 201);            
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $uuid
     * @return \Illuminate\Http\Response
     */
    public function destroy($uuid)
    {
        $customerAddress = $this->model->whereUuid($uuid)->first();

        if(!$customerAddress) {
            return jsonMessage('Customer Address Details are not found', 422);            
        }

        $customerAddress->delete();

        return jsonMessage('Customer Address Details deleted successfully.', 200);            
    }
}

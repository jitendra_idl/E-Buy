<?php

namespace App\Http\Controllers\DeliveryBoys;

use Illuminate\Http\Request;
use App\Http\Controllers\BaseController;
use App\DeliveryBoys\DeliveryBoy;

class DeliveryBoysController extends BaseController
{
     /**
     * DeliveryBoy Model Instance.
     * @var
     */
    protected $deliveryboy;

    /**
     * @param DeliveryBoy $deliveryboy
     */
    public function __construct(DeliveryBoy $deliveryboy)
    {
        $this->model = $deliveryboy;
    }

    public function getAllDeliveryBoys()
    {
        $deliveryboys = $this->model->all();

        return jsonData($deliveryboys, 200);
    }

    /**
     * Store a newly created DeliveryBoy.
     *
     * @param  StoreDeliveryBoyRequest $request
     * @return json
     */
    public function store(Request $request)
    {
        request()->request->Add(['uuid' => $this->getUuid()]);
        $deliveryboys = $this->model->create($request->all());
        if (!$deliveryboys) {
            return jsonMessage('Delivery boys details are not found', 422);
        } else {
            return jsonMessage('Delivery boys details stored successfully.', 201);
        }
    }

    /**
     * DeliveryBoy Index
     * Display a listing of the DeliveryBoy.
     *
     * @param  Request $request
     * @return mixed
     */
    public function index(Request $request)
    {
        $deliveryboys = $this->model->paginate($request->per_page);

        return jsonData($deliveryboys, 200);
    }

    /**
     * DeliveryBoy View
     * Display the specified DeliveryBoy details.
     *
     * @param  $id
     * @return json
     */
    public function show($uuid)
    {
        $deliveryboy = $this->model->whereUuid($uuid)->first();
        if ($deliveryboy) {
            return jsonData($deliveryboy);
        } else {
            return jsonMessage('DeliveryBoy details not found.', 400);
        }
    }

    /**
     * DeliveryBoy Edit View
     * Show the form for editing the specified deliveryboy.
     *
     * @param  $id
     * @return json
     */
    public function edit($uuid)
    {
        $deliveryboy = $this->model->whereUuid($uuid)->first();
        if ($deliveryboy) {
            return jsonData($deliveryboy);
        } else {
            return jsonMessage('DeliveryBoy details not found.', 400);
        }
    }

    /**
     * DeliveryBoy Update
     * Update the specified deliveryboy.
     *
     * @param Request $request
     * @param  $id
     * @return json
     */
    public function update(Request $request, $uuid)
    {
        $deliveryboy = $this->model->whereUuid($uuid)->first();

        if (!$deliveryboy) {
            return jsonMessage('DeliveryBoy details not found.', 400);
        }
        $deliveryboy->update($request->all());

        return jsonMessage(' Details Updated successfully');
    }

    /**
     * DeliveryBoy Delete
     * Delete the specified deliveryboy.
     * @param  $id
     * @return json
     */
    public function destroy($uuid)
    {
        $deliveryboy = $this->model->whereUuid($uuid)->first();

        if (!$deliveryboy) {
            return jsonMessage('DeliveryBoy details not found.', 400);
        }
        $deliveryboy->delete();

        return jsonMessage('DeliveryBoy details Details Deleted successfully');
    }
}

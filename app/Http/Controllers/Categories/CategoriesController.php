<?php

namespace App\Http\Controllers\Categories;

use Illuminate\Http\Request;
use App\Http\Controllers\BaseController;
use App\Categories\Category;

class CategoriesController extends BaseController
{
    protected $model;
     
    /**
     * category model.
     */
    public function __construct(Category $category)
    {
        parent::__construct();

        $this->model = $category;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
       $categorys = $this->model->paginate($request->per_page);

       return jsonData($categorys);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       request()->request->add(['uuid' => $this->getUuid()]);
       
       $category = $this->model->create(request()->all());

        if (!$category) {
            return jsonMessage('category Details are not found', 422);
        } else {
            return jsonMessage('category details are stored successfully.', 201);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $uuid
     * @return \Illuminate\Http\Response
     */
    public function show($uuid)
    {
        $category = $this->model->whereUuid($uuid)->first();

        if(!$category) {
            return jsonMessage('category Details are not found', 422);            
        }
        return jsonData($category);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $uuid
     * @return \Illuminate\Http\Response
     */
    public function edit($uuid)
    {
        $category = $this->model->whereUuid($uuid)->first();

        if(!$category) {
            return jsonMessage('category Details are not found', 422);            
        }
        return jsonData($category);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $uuid
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $uuid)
    {
        $category = $this->model->whereUuid($uuid)->first();

        if(!$category) {
            return jsonMessage('category Details are not found', 422);            
        }

        $category->fill(request()->all())->save();

        return jsonMessage('category Details updated successfully.', 201);            
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $uuid
     * @return \Illuminate\Http\Response
     */
    public function destroy($uuid)
    {
        $category = $this->model->whereUuid($uuid)->first();

        if(!$category) {
            return jsonMessage('category Details are not found', 422);            
        }

        $category->delete();

        return jsonMessage('category Details deleted successfully.', 200);            
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      //  return view('index');

 
        return view('index');
    }

    // public function lending()
    // {
    //     return view('index');
        
    // }

    public function order()
    {
        return view('order');
    }


    // public function settings()
    // {
    //     return view('settings');
        
    // }

    public function venderList()
    {
        return view('vender-list');
        
    }

    public function venderList2()
    {
        return view('vender-list-2');
        
    }

    public function invoiceList()
    {
        return view('invoice-listing');
        
    }
}

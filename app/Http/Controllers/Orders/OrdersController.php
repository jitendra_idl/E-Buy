<?php

namespace App\Http\Controllers\Orders;

use Illuminate\Http\Request;
use App\Http\Controllers\BaseController;
use App\Orders\Order;
use Carbon\Carbon;
use App\OrderStatus\OrderStatus;
use App\ProductImages\ProductImage;


class OrdersController extends BaseController
{
    protected $model;
     
    /**
     * order model.
     */
    public function __construct(Order $order)
    {
        parent::__construct();
        $this->model = $order;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
       $orders = $this->model->paginate($request->per_page);

       return jsonData($orders);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $currentDate = Carbon::now()->format('Y-m-d');
        
       request()->request->add(['uuid' => $this->getUuid(), 
                                'order_no' => $this->getNewOrderNo(),
                                'order_date' => $currentDate]);

        $order = $this->model->create(request()->all());

        // Save Order Status.    
        OrderStatus::saveData($order);

        if (!$order) {
            return jsonMessage('Order Details are not found', 422);
        } else {
            return jsonMessage('Order details are stored successfully.', 201);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $uuid
     * @return \Illuminate\Http\Response
     */
    public function show($uuid)
    {
        $order = $this->model->whereUuid($uuid)->first();

        if(!$order) {
            return jsonMessage('Order Details are not found', 422);            
        }
        return jsonData($order);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $uuid
     * @return \Illuminate\Http\Response
     */
    public function edit($uuid)
    {
        $order = $this->model->whereUuid($uuid)->first();

        if(!$order) {
            return jsonMessage('Order Details are not found', 422);            
        }
        return jsonData($order);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $uuid
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $uuid)
    {
        $order = $this->model->whereUuid($uuid)->first();

        if(!$order) {
            return jsonMessage('Order Details are not found', 422);            
        }

        $order->fill(request()->all())->save();

        return jsonMessage('Order Details updated successfully.', 201);            
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $uuid
     * @return \Illuminate\Http\Response
     */
    public function destroy($uuid)
    {
        $order = $this->model->whereUuid($uuid)->first();

        if(!$order) {
            return jsonMessage('Order Details are not found', 422);            
        }

        $order->delete();

        return jsonMessage('Order Details deleted successfully.', 200);            
    }

    /**
     * Get new order number.
     */
    public function getNewOrderNo()
    {
        $order = $this->model->orderBy('order_no', 'desc')->first();
        
        if ($order) {
            $newOrderNo = $order->order_no + 1;
            return $newOrderNo;
        } else {
            return 1;
        }
    }

    /**
     * Get Todays date Orders List
     */
    public function currentDateOrders(Request $request)
    {
        $currentDate = Carbon::now()->format('Y-m-d');

        $orders = $this->model->where('order_date' , $currentDate)->paginate($request->per_page);

       return jsonData($orders);
    }

    /**
     * Get Tommorows date Orders List
     */
    public function tommorowsDateOrders(Request $request)
    {
        $tommorrowDayDate = Carbon::now()->addDay(1)->format('Y-m-d');
      
        $orders = $this->model->where('order_date' , $tommorrowDayDate)->paginate($request->per_page);

       return jsonData($orders);
    }

    /**
     * Get Yesterdays date Orders List
     */
    public function yesterdaysDateOrders(Request $request)
    {
        $yestreDayDate = Carbon::now()->subDay(1)->format('Y-m-d');
      
        $orders = $this->model->where('order_date' , $yestreDayDate)->paginate($request->per_page);

       return jsonData($orders);
    }

    public function lastSevenDaysOrders(Request $request)
    {
        $yestreDayDate = Carbon::now()->subDay(7)->format('Y-m-d');
        $currentDate = Carbon::now()->format('Y-m-d');
        
      
        $orders = $this->model->where('order_date' ,'>=', $yestreDayDate)
                                ->where('order_date' ,'<=', $currentDate)
                                ->paginate($request->per_page);

       return jsonData($orders);
    }

    public function lastFifteenDaysOrders(Request $request)
    {
        $lastFifteenDays = Carbon::now()->subDay(15)->format('Y-m-d');
        $currentDate = Carbon::now()->format('Y-m-d');
        
        $orders = $this->model->where('order_date' ,'>=', $lastFifteenDays)
                                ->where('order_date' ,'<=', $currentDate)
                                ->paginate($request->per_page);

       return jsonData($orders);
    }

    public function lastThirteenDaysOrders(Request $request)
    {
        $lastMonthData = Carbon::now()->subDay(30)->format('Y-m-d');
        $currentDate = Carbon::now()->format('Y-m-d');
        
        $orders = $this->model->where('order_date' ,'>=', $lastMonthData)
                                ->where('order_date' ,'<=', $currentDate)
                                ->paginate($request->per_page);

       return jsonData($orders);
    }

    public function getOrders(Request $request)
    {
       $orderIds = OrderStatus::where('status', request('status'))->pluck('order_id');
       
       $orders = $this->model->whereIn('id', $orderIds)->paginate($request->per_page);

       return jsonData($orders);
       
    }
}

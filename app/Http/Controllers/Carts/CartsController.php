<?php

namespace App\Http\Controllers\Carts;

use Illuminate\Http\Request;
use App\Carts\Cart;
use App\Http\Controllers\BaseController;

class CartsController extends BaseController
{
    protected $cart;

    public function __construct(Cart $cart)
    {
        parent::__construct();
        $this->model = $cart;
    }

    public function getAllProducts()
    {
        $carts = $this->model->with('products')->where('customer_id', $this->customer->id)->get();

        return jsonData($carts);
    }

    /**
     * Display a listing of the resource.
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $carts = $this->model->paginate($request->per_page);

        return jsonData($carts, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $medicine = Cart::where('customer_id', $this->customer->id)->where('product_details_id', request('product_details_id'))->first();
        if ($medicine) {
            $totalMedicine = $medicine->quantity + request('quantity');
            $medicine->update(['quantity' => $totalMedicine]);

            return jsonMessage('Cart details updated successfully.', 201);
        }
        request()->request->add(['customer_id' => $this->customer->id, 'uuid' => $this->getUuid()]);

        try {
            $this->model->create(request()->all());
        } catch (\Illuminate\Database\QueryException $e) {
            return jsonMessage('Cart details already exists.', 201);
        }

        return jsonMessage('Cart details saved successfully.', 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int                       $id
     * @return \Illuminate\Http\Response
     */
    public function show($uuid)
    {
        $cart = $this->model->whereUuid($uuid)->first();
        if ($cart) {
            return jsonData($cart);
        } else {
            return jsonMessage('Cart details does not found', 400);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int                       $id
     * @return \Illuminate\Http\Response
     */
    public function edit($uuid)
    {
        $cart = $this->model->whereUuid($uuid)->first();
        if ($cart) {
            return jsonData($cart);
        } else {
            return jsonMessage('Cart details does not found', 400);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int                       $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $uuid)
    {
        $cartsData = $this->model->whereUuid($uuid)->first();
        if (!$cartsData) {
            return jsonMessage('Cart Details not found', 400);
        }
        $quantity = $cartsData->quantity + request('quantity');
        
        request()->request->add(['quantity' => $quantity]);
        $cartsData->update(request()->all());

        return jsonMessage('Cart Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int                       $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($uuid)
    {
        $cartsData = $this->model->whereUuid($uuid)->first();
        if (!$cartsData) {
            return jsonMessage('Cart Details not found', 400);
        }
        $cartsData->delete();

        return jsonMessage('Cart Details Deleted Successfully');
    }
}

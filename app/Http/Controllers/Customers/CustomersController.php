<?php

namespace App\Http\Controllers\Customers;

use Illuminate\Http\Request;
use App\Http\Controllers\BaseController;
use App\Customers\Customer;

class CustomersController extends BaseController
{
    protected $model;
     
    /**
     * customer model.
     */
    public function __construct(Customer $customer)
    {
        $this->model = $customer;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $customers = $this->model->all();

       return jsonData($customers);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       request()->request->add(['uuid' => $this->getUuid()]);
       $customer = $this->model->create(request()->all());

        if (!$customer) {
            return jsonMessage('Customer Details are not found', 422);
        } else {
            return jsonMessage('Customer details are stored successfully.', 201);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $uuid
     * @return \Illuminate\Http\Response
     */
    public function show($uuid)
    {
        $customer = $this->model->whereUuid($uuid)->first();

        if(!$customer) {
            return jsonMessage('Customer Details are not found', 422);            
        }
        return jsonData($customer);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $uuid
     * @return \Illuminate\Http\Response
     */
    public function edit($uuid)
    {
        $customer = $this->model->whereUuid($uuid)->first();

        if(!$customer) {
            return jsonMessage('Customer Details are not found', 422);            
        }
        return jsonData($customer);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $uuid
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $uuid)
    {
        $customer = $this->model->whereUuid($uuid)->first();

        if(!$customer) {
            return jsonMessage('Customer Details are not found', 422);            
        }

        $customer->fill(request()->all())->save();

        return jsonMessage('Customer Details updated successfully.', 201);            
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $uuid
     * @return \Illuminate\Http\Response
     */
    public function destroy($uuid)
    {
        $customer = $this->model->whereUuid($uuid)->first();

        if(!$customer) {
            return jsonMessage('Customer Details are not found', 422);            
        }

        $customer->delete();

        return jsonMessage('Customer Details deleted successfully.', 200);            
    }
}

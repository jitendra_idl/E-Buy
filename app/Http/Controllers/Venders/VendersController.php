<?php

namespace App\Http\Controllers\Venders;

use Illuminate\Http\Request;
use App\Http\Controllers\BaseController;
use App\Venders\Vender;
use App\Organizations\Organization;

class VendersController extends BaseController
{
    protected $model;
     
    /**
     * vender model.
     */
    public function __construct(Vender $vender)
    {
        $this->model = $vender;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

       $venders = $this->model->with(['media', 'organization'])->paginate($request->per_page);

       return jsonData($venders);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

       $organization = Organization::saveData(request('organization'));
       
        if (request()->has('file')) {
            $media = $this->saveMediaFile(
            request()->file,
            request()->id,
            config('vendor.vender.directory'),
            request()->name,
            config('vendor.vender.thumbnails.width', 200),
            config('vendor.vender.thumbnails.height', 200),
            config('vendor.vender.max_size', null),
            request()->media_alt
        );
        }
        request()->request->add([
            'uuid' => $this->getUuid(), 
            'media_id' => $media->id, 
            'organization_id' => $organization->id
            ]);

        $this->model->create(request()->all());

        return jsonMessage('Vendore Details saved successfully.', 200);            
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $uuid
     * @return \Illuminate\Http\Response
     */
    public function show($uuid)
    {
        $vender = $this->model->with(['media', 'organization'])->whereUuid($uuid)->first();

        if(!$vender) {
            return jsonMessage('Vender Details are not found', 422);            
        }
        return jsonData($vender);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $uuid
     * @return \Illuminate\Http\Response
     */
    public function edit($uuid)
    {
        $vender = $this->model->with(['media', 'organization'])->whereUuid($uuid)->first();

        if(!$vender) {
            return jsonMessage('Vender Details are not found', 422);            
        }
        return jsonData($vender);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $uuid
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $uuid)
    {
        $vender = $this->model->whereUuid($uuid)->first();

        if(!$vender) {
            return jsonMessage('Vender Details are not found', 422);            
        }
        $data = request()->except(['_method', 'organization']);
  
        $vender->fill($data)->save();

        return jsonMessage('Vender Details updated successfully.', 201);            
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $uuid
     * @return \Illuminate\Http\Response
     */
    public function destroy($uuid)
    {
        $vender = $this->model->whereUuid($uuid)->first();

        if(!$vender) {
            return jsonMessage('Vender Details are not found', 422);            
        }

        $vender->delete();

        return jsonMessage('Vender Details deleted successfully.', 200);            
    }
}

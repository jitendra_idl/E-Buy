<?php

namespace App\Http\Controllers\OrderStatus;

use Illuminate\Http\Request;
use App\Http\Controllers\BaseController;
use App\OrderStatus\OrderStatus;

class OrderStatusController extends BaseController
{
      /**
     * Status Model Instance.
     * @var
     */
    protected $status;

    /**
     * @param StatusRepository $status
     */
    public function __construct(OrderStatus $status)
    {
        parent::__construct();
        $this->model = $status;
    }

    /**
     * Store
     * Store a newly created status in database.
     *
     * @param  Request $request
     * @return json
     */
    public function store(Request $request)
    {
        $existOrderStatus = $this->model->where('order_id', request('order_id'))->where('status', request('status'))->first();

        if ($existOrderStatus) {
            return jsonMessage('Order status already updated.');
        }


       request()->request->add(['uuid' => $this->getUuid()]);
        $status = $this->model->create($request->all());
        if (!$status) {
            return jsonMessage('Status not found', 422);
        } else {
            return jsonMessage('Status updated Successfully.', 201);
        }
    }

    public function getAllStatuses()
    {
        return jsonData([
            'Order Placed',
            'Confirmed',
            'Delivery boy assigned',
            'Delivery boy confirmed',
            'Customer confirmed',
            'Delivered',
            'Canceled',
            'Despatch',
        ], 200);
    }
}

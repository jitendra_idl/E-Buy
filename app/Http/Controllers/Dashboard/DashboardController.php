<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\BaseController;
use App\Orders\Order;
use Carbon\Carbon;
use App\OrderStatus\OrderStatus;

class DashboardController extends BaseController
{
    public function __construct()
    {
        parent::__construct();
    }

    public function dashboardData(Request $request)
    {
        $currentDate = Carbon::now()->format('Y-m-d');

        $ordersCount = Order::where('order_date', $currentDate)->count();
        $cancelOrderCount = OrderStatus::where('status', 'Cancel')->count();
        $soltOrderCount = OrderStatus::where('status', 'Order Delivered')->count();
        
        $data = [
            'New Order Received' => $ordersCount,
            'Order Cancel' => $cancelOrderCount,
            'No Of Items Solds' => $soltOrderCount
        ];
   

        if(!$request->ajax())
        {
            return view('index', compact('data'));
        } 

        return jsonData($data);
    }

    public function newOrderReceived()
    {
        $currentDate = Carbon::now()->format('Y-m-d');

        $orders = Order::where('order_date', $currentDate)->get();

        return jsonData($orders);
        
    }

    public function getCancelOrder(Request $request)
    {
        $cancelOrderIds = OrderStatus::where('status', 'Cancel')->pluck('order_id');

        $orders = Order::whereIn('id', $cancelOrderIds)->with(['orderStatus' => function($query) {
            $query->where('status', 'cancel');
        } 
        ])->paginate($request->per_page);

        return jsonData($orders);        
        
    }

    public function getSoldOrder(Request $request)
    {
        $cancelOrderIds = OrderStatus::where('status', 'Order Delivered')->pluck('order_id');

        $orders = Order::whereIn('id', $cancelOrderIds)->with(['orderStatus' => function($query) {
            $query->where('status', 'Order Delivered');
        } 
        ])->paginate($request->per_page);

        return jsonData($orders);        
        
    }
}

<?php

namespace App\Http\Controllers\Brands;

use Illuminate\Http\Request;
use App\Http\Controllers\BaseController;
use App\Brands\Brand;

class BrandsController extends BaseController
{
    protected $model;
     
    /**
     * brand model.
     */
    public function __construct(Brand $brand)
    {
        parent::__construct();

        $this->model = $brand;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
       $brands = $this->model->paginate($request->per_page);

       return jsonData($brands);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (request()->has('file')) {
            $media = $this->saveMediaFile(
            request()->file,
            request()->id,
            config('vendor.brand.directory'),
            request()->name,
            config('vendor.brand.thumbnails.width', 200),
            config('vendor.brand.thumbnails.height', 200),
            config('vendor.brand.max_size', null),
            request()->media_alt
        );
        }

        request()->request->add(['uuid' => $this->getUuid(), 'media_id' => $media->id]);
       
       $brand = $this->model->create(request()->all());

        if (!$brand) {
            return jsonMessage('brand Details are not found', 422);
        } else {
            return jsonMessage('brand details are stored successfully.', 201);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $uuid
     * @return \Illuminate\Http\Response
     */
    public function show($uuid)
    {
        $brand = $this->model->whereUuid($uuid)->first();

        if(!$brand) {
            return jsonMessage('brand Details are not found', 422);            
        }
        return jsonData($brand);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $uuid
     * @return \Illuminate\Http\Response
     */
    public function edit($uuid)
    {
        $brand = $this->model->whereUuid($uuid)->first();

        if(!$brand) {
            return jsonMessage('brand Details are not found', 422);            
        }
        return jsonData($brand);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $uuid
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $uuid)
    {
        $brand = $this->model->whereUuid($uuid)->first();

        if(!$brand) {
            return jsonMessage('brand Details are not found', 422);            
        }

        $brand->fill(request()->all())->save();

        return jsonMessage('brand Details updated successfully.', 201);            
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $uuid
     * @return \Illuminate\Http\Response
     */
    public function destroy($uuid)
    {
        $brand = $this->model->whereUuid($uuid)->first();

        if(!$brand) {
            return jsonMessage('brand Details are not found', 422);            
        }

        $brand->delete();

        return jsonMessage('brand Details deleted successfully.', 200);            
    }
}

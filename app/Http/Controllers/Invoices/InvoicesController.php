<?php

namespace App\Http\Controllers\Invoices;

use Illuminate\Http\Request;
use App\Http\Controllers\BaseController;
use App\Invoices\Invoice;

class InvoicesController extends BaseController
{
    protected $model;
     
    /**
     * invoice model.
     */
    public function __construct(Invoice $invoice)
    {
        parent::__construct();

        $this->model = $invoice;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
       $invoices = $this->model->paginate($request->per_page);;

       return jsonData($invoices);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       request()->request->add(['uuid' => $this->getUuid(), 
                                'customer_id' => $this->customer->id]);

       $invoice = $this->model->create(request()->all());

        if (!$invoice) {
            return jsonMessage('invoice Details are not found', 422);
        } else {
            return jsonMessage('invoice details are stored successfully.', 201);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $uuid
     * @return \Illuminate\Http\Response
     */
    public function show($uuid)
    {
        $invoice = $this->model->whereUuid($uuid)->first();

        if(!$invoice) {
            return jsonMessage('invoice Details are not found', 422);            
        }
        return jsonData($invoice);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $uuid
     * @return \Illuminate\Http\Response
     */
    public function edit($uuid)
    {
        $invoice = $this->model->whereUuid($uuid)->first();

        if(!$invoice) {
            return jsonMessage('invoice Details are not found', 422);            
        }
        return jsonData($invoice);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $uuid
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $uuid)
    {
        $invoice = $this->model->whereUuid($uuid)->first();

        if(!$invoice) {
            return jsonMessage('invoice Details are not found', 422);            
        }
  
        try{
            $invoice->fill(request()->all())->save();
        } catch(\Exception $e){
            dd($e->getMessage());
        }

        return jsonMessage('invoice Details updated successfully.', 201);            
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $uuid
     * @return \Illuminate\Http\Response
     */
    public function destroy($uuid)
    {
        $invoice = $this->model->whereUuid($uuid)->first();

        if(!$invoice) {
            return jsonMessage('invoice Details are not found', 422);            
        }

        $invoice->delete();

        return jsonMessage('invoice Details deleted successfully.', 200);            
    }
}

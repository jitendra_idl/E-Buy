<?php

namespace App\Http\Controllers\Products;

use Illuminate\Http\Request;
use App\Http\Controllers\BaseController;
use App\Products\Product;
use Illuminate\Support\Facades\Auth;
use App\ProductImages\ProductImage;
use App\Pricings\Pricing;

class ProductsController extends BaseController
{

    protected $model;
     
    /**
     * Product model.
     */
    public function __construct(Product $product)
    {
        parent::__construct();

        $this->model = $product;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $products = $this->model->paginate($request->per_page);

       return jsonData($products);
       
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $price = Pricing::saveData(request('pricing'));

        if (request()->has('file')) {
            $media = $this->saveMediaFile(
            request()->file,
            request()->id,
            config('vendor.product.directory'),
            request()->name,
            config('vendor.product.thumbnails.width', 200),
            config('vendor.product.thumbnails.height', 200),
            config('vendor.product.max_size', null),
            request()->media_alt
        );
        }

        request()->request->add([
            'uuid' => $this->getUuid(), 
            'media_id' => $media->id,
            'pricing_id' => $price->id
            ]);

       $product = $this->model->create(request()->all());

        if (!$product) {
            return jsonMessage('Product Details are not found', 422);
        } else {
            return jsonMessage('product details are stored successfully.', 201);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $uuid
     * @return \Illuminate\Http\Response
     */
    public function show($uuid)
    {
        $product = $this->model->whereUuid($uuid)->first();

        if(!$product) {
            return jsonMessage('Product Details are not found', 422);            
        }
        return jsonData($product);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $uuid
     * @return \Illuminate\Http\Response
     */
    public function edit($uuid)
    {
        $product = $this->model->whereUuid($uuid)->first();

        if(!$product) {
            return jsonMessage('Product Details are not found', 422);            
        }
        return jsonData($product);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $uuid
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $uuid)
    {
        $product = $this->model->whereUuid($uuid)->first();

        if(!$product) {
            return jsonMessage('Product Details are not found', 422);            
        }

        $product->fill(request()->all())->save();

        return jsonMessage('Product Details updated successfully.', 201);            
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $uuid
     * @return \Illuminate\Http\Response
     */
    public function destroy($uuid)
    {
        $product = $this->model->whereUuid($uuid)->first();

        if(!$product) {
            return jsonMessage('Product Details are not found', 422);            
        }

        $product->delete();

        return jsonMessage('Product Details deleted successfully.', 200);            
    }

    public function getCatelogRealtedInStockProducts($catelogId)
    {
       $products = $this->model->with(['media', 'price'])
                                ->whereIn('catalogue_id', [$catelogId])
                                 ->where('status', 'In Stock')->get();
        
        return view('product-list', compact('products'));
    }

    public function getCatelogRealtedOutOfStockProducts($catelogId)
    {
       $products = $this->model->with(['media', 'price'])
                                ->whereIn('catalogue_id', [$catelogId])
                                 ->where('status', 'out-of-stock')->get();
        
        return view('product-list', compact('products'));
    }
}

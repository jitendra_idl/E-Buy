<?php

namespace App\Http\Controllers\Lookups;

use App\Lookups\Type;

use Illuminate\Http\Request;
use App\Http\Requests\StoreTypeRequest;
use App\Http\Controllers\BaseController;

class TypesController extends BaseController
{
    /**
     * Type Model Instance.
     * @var
     */
    protected $type;

    /**
     * @param Type $type
     */
    public function __construct(Type $type)
    {
        $this->model = $type;
    }

    /**
     * Store a newly created type.
     *
     * @param  StoreOrderRequest $request
     * @return json
     */
    public function store(Request $request)
    {
        $type = $this->model->create($request->all());
        if (!$type) {
            return jsonMessage('Data not found');
        }

        return jsonMessage('Data Stored Successfully', 201);
    }

    /**
     * Type Index
     * Display a listing of the Type.
     *
     * @param  Request $request
     * @return mixed
     */
    public function index(Request $request)
    {
        $types = $this->model->get();

        return jsonData($types);
    }

    /**
     * Type View
     * Display the specified Type details.
     *
     * @param  $id
     * @return json
     */
    public function show($id)
    {
        $type = $this->model->whereId($id)->first();

        if(!$type) {
            return jsonMessage('Yype Details are not found', 422);            
        }
        return jsonData($type);
    }

    /**
     * Type Edit View
     * Show the form for editing the specified type.
     *
     * @param  $id
     * @return json
     */
    public function edit($id)
    {
        $type = $this->model->whereId($id)->first();

        if(!$type) {
            return jsonMessage('Type Details are not found', 422);            
        }
        return jsonData($type);
    }

    /**
     * Type Update
     * Update the specified type.
     *
     * @param Request $request
     * @param  $id
     * @return json
     */
    public function update(Request $request, $id)
    {
        $typeData = $this->model->whereId($id)->first();

        if(!$typeData) {
            return jsonMessage('TypeData Details are not found', 422);            
        }

        $typeData->fill(request()->all())->save();

        return jsonMessage('TypeData Details updated successfully.', 201);
    }

    public function destroy($id)
    {
        $type = $this->model->whereId($id)->first();

        if(!$type) {
            return jsonMessage('Type Details are not found', 422);            
        }

        $type->delete();

        return jsonMessage('Type Details deleted successfully.', 200); 
    }

    public function getTypes()
    {
        $types = $this->model->all();

        return jsonData($types, 200);
    }
}

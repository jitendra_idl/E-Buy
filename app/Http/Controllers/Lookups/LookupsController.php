<?php

namespace App\Http\Controllers\Lookups;

use App\Lookups\Type;

use App\Lookups\Lookup;
use Illuminate\Http\Request;
use App\Http\Controllers\BaseController;

/**
 * @resource Lookup
 *
 * Lookup API
 */
class LookupsController extends BaseController
{
    /**
     * Lookup Model Instance.
     * @var
     */
    protected $lookup;

    /**
     * @param Lookup $lookup
     */
    public function __construct(Lookup $lookup)
    {
        $this->model = $lookup;
    }

    /**
     * Get Lookups
     * Get list of Lookpus.
     *
     * @return json
     */
    public function getLookups(Request $request)
    {
        $type = Type::where('alias', $request->get('type'))->first();
    
        if ($type) {
            $lookups = $this->model->where('type_id', $type->id)->get();
            $response['lookups'] = $lookups;
        } else {
            return jsonMessage('Type Detail not found', 400);
        }

        return jsonData($response);
    }

    /**
     * Store
     * Store a newly created lookup in storage.
     *
     * @param  StorelookupRequest $lookup
     * @return json
     */
    public function store(Request $request)
    {
        $lookupData = $this->model->create($request->all());

        if ($lookupData) {
            return jsonMessage('Lookups Details save successfully.', 200);            
        } else {
            return jsonMessage('Lookups Details are not found', 422);            
        }
    }

    /**
     * Lookup Index
     * Display a listing of the lookup.
     *
     * @param  Request $request
     * @return mixed
     */
    public function index(Request $request)
    {
        $lookups = $this->model->all();

        return jsonData($lookups);
    }

    /**
     * Show
     * Display the specified lookup.
     *
     * @param  $id
     * @return json
     */
    public function show($id)
    {
        $lookup = $this->model->whereId($id)->first();
        
        if (!$lookup) {
            return jsonMessage('Lookup details not found.', 400);
        }
        return jsonData($lookup);
    }

    /**
     * Edit
     * Show the form for editing the specified lookup.
     *
     * @param  $id
     * @return json
     */
    public function edit($id)
    {
        $lookup = $this->model->whereId($id)->first();
        
        if (!$lookup) {
            return jsonMessage('Lookup details not found.', 400);
        }
        return jsonData($lookup);
    }

    /**
     * Update
     * Update the specified lookup in storage.
     *
     * @param Request $request
     * @param  $id
     * @return json
     */
    public function update(Request $request, $id)
    {
        $lookupData = $this->model->whereId($id)->first();

        if(!$lookupData) {
            return jsonMessage('Lookups Details are not found', 422);            
        }

        $lookupData->fill(request()->all())->save();

        return jsonMessage('Lookup Details updated successfully.', 201);    
    }

    /**
     * Delete
     * Remove the specified lookup from storage.
     *
     * @param  $id
     * @return json
     */
    public function destroy($id)
    {
        $lookup = $this->model->whereId($id)->first();

        if(!$lookup) {
            return jsonMessage('Lookup Details are not found', 422);            
        }
        $lookup->delete();

        return jsonMessage('Lookup Details deleted successfully.', 200);    
    }
}

<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use JWTFactory;
use JWTAuth;
use App\User;
use Illuminate\Support\Facades\Auth;
class APILoginController extends Controller
{

    /**
     * Login Functionality for user and admin
     * 
     * @return Token and current login user Details.
     */

    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|string|email|max:255',
            'password'=> 'required'
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors());
        }
        $credentials = $request->only('email', 'password');

            if ($token = JWTAuth::attempt($credentials)) {
                if(Auth()->user()->role_id == 1 && \Route::currentRouteName() == 'userLogin') {
                    return response()->json(['error' => 'Invalid credential, Plese try again.!', 400]);
                } elseif(Auth()->user()->role_id == 2 && \Route::currentRouteName() == 'adminLogin') {
                    return response()->json(['error' => 'Invalid credential, Please try again', 400]);
                } else {            
                    $userDetails = $this->sendLoginResponse(Auth()->user());

                    return jsonData([
                        'token'    => $token,
                        'customer' => $userDetails,
                    ]);
                }
            }
    }

    public function sendLoginResponse($loginUser) {
      return  \DB::table('users')->where('id', $loginUser->id)
                                    ->select('first_name', 'last_name', 'email', 'role_id')->first();
    }
}
<?php

namespace App\Products;

use Illuminate\Database\Eloquent\Model;
use App\BaseModel;
use App\Media\Media;
use App\Pricings\Pricing;

class Product extends BaseModel
{
    protected $table = 'product_details';
    
    protected $fillable = [
        'catalogue_id',
        'brand_id',
        'order_id',
        'pricing_id',
        'media_id',
        'name',
        'description',
        'disscount',
        'quantities',        
        'minimum_quantities',
        'product_code',
        'product_weight',
        'unit',
        'status',
        'uuid'
    ];


    public function media()
    {
        return $this->belongsTo(Media::class, 'media_id');
    }
    
    public function price()
    {
        return $this->belongsTo(Pricing::class, 'pricing_id');
    }
}

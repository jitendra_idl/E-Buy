<?php

namespace App\Pricings;

use Illuminate\Database\Eloquent\Model;
use App\BaseModel;
use Illuminate\Support\Str;

class Pricing extends BaseModel
{
    protected $table = 'pricing';

    protected $fillble = [
        'uuid',
        'price_tax_inc',
        'price_tax_excl',
        'price_per_unit',
        'tax_rule'
    ];

    public function scopeSaveData($query,$data)
    {
        $data = json_decode($data,true);
        $data['uuid'] = (string) Str::uuid();
       
        return $query->create($data);
    }
}

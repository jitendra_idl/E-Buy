<?php

namespace App\Venders;

use Illuminate\Database\Eloquent\Model;
use App\BaseModel;
use App\Media\Media;
use App\Organizations\Organization;

class Vender extends BaseModel
{
    protected $fillable = [
        'uuid',
        'media_id',
        'organization_id',        
        'first_name',
        'last_name',
        'gender',
        'email',
        'password',
        'city',
        'state',
        'country',
    ];

    public function media()
    {
        return $this->belongsTo(Media::class, 'media_id');
    }

    public function organization()
    {
        return $this->belongsTo(Organization::class, 'organization_id');
    }
}

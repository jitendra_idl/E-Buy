<?php

namespace App\Categories;

use Illuminate\Database\Eloquent\Model;
use App\BaseModel;

class Category extends BaseModel
{
    protected $fillable =[
        'name',
        'descriptions',
        'uuid'
    ];
}

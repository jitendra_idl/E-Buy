<?php

namespace App\OrderStatus;

use Illuminate\Database\Eloquent\Model;
use App\Lookups\Lookup;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use App\BaseModel;

class OrderStatus extends BaseModel
{
    protected $table = 'order_status';
    protected $fillable = ['order_id', 'status', 'uuid'];

    public function status()
    {
        return $this->belongsTo(Lookup::class, 'status_id');
    }

    public function scopeSaveData($query, $data)
    {
        $query->create([
            'order_id' => $data->id,
            'uuid' => (string) Str::uuid(),
            'status' => 'pending',
        ]);
    }

    public function order()
    {
        return $this->belongsTo(Order::class,'order_id');
    }
}

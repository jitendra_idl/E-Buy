<?php

namespace App\ProductImages;

use Illuminate\Database\Eloquent\Model;
use App\BaseModel;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class ProductImage extends BaseModel
{
    protected $fillable = [
        'uuid',
        'product_details_id',
        'file_name',
        'slug',
        'file_location',
        'url'
    ];
}

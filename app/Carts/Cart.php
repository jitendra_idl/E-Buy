<?php

namespace App\Carts;

use Illuminate\Database\Eloquent\Model;
use App\Customers\Customer;
use App\Products\Product;
use App\BaseModel;

class Cart extends BaseModel
{
    protected $table = 'customer_carts';

    protected $fillable = ['customer_id', 'product_details_id', 'quantity', 'uuid'];

    public function customer()
    {
        return $this->belongsTo(Customer::class, 'customer_id');
    }

    public function products()
    {
        return $this->belongsTo(Product::class, 'product_details_id')->join('customer_carts as customer_cart', 'customer_cart.product_details_id', '=', 'product_details.id')
        ->select('product_details.id', 'product_details.brand', 'product_details.disscount', 'product_details.categories');
    }

}
